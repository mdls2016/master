<?php

use App\Models\sections_coordinates;
use App\Repositories\sections_coordinatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class sections_coordinatesRepositoryTest extends TestCase
{
    use Makesections_coordinatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var sections_coordinatesRepository
     */
    protected $sectionsCoordinatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sectionsCoordinatesRepo = App::make(sections_coordinatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatesections_coordinates()
    {
        $sectionsCoordinates = $this->fakesections_coordinatesData();
        $createdsections_coordinates = $this->sectionsCoordinatesRepo->create($sectionsCoordinates);
        $createdsections_coordinates = $createdsections_coordinates->toArray();
        $this->assertArrayHasKey('id', $createdsections_coordinates);
        $this->assertNotNull($createdsections_coordinates['id'], 'Created sections_coordinates must have id specified');
        $this->assertNotNull(sections_coordinates::find($createdsections_coordinates['id']), 'sections_coordinates with given id must be in DB');
        $this->assertModelData($sectionsCoordinates, $createdsections_coordinates);
    }

    /**
     * @test read
     */
    public function testReadsections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $dbsections_coordinates = $this->sectionsCoordinatesRepo->find($sectionsCoordinates->id);
        $dbsections_coordinates = $dbsections_coordinates->toArray();
        $this->assertModelData($sectionsCoordinates->toArray(), $dbsections_coordinates);
    }

    /**
     * @test update
     */
    public function testUpdatesections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $fakesections_coordinates = $this->fakesections_coordinatesData();
        $updatedsections_coordinates = $this->sectionsCoordinatesRepo->update($fakesections_coordinates, $sectionsCoordinates->id);
        $this->assertModelData($fakesections_coordinates, $updatedsections_coordinates->toArray());
        $dbsections_coordinates = $this->sectionsCoordinatesRepo->find($sectionsCoordinates->id);
        $this->assertModelData($fakesections_coordinates, $dbsections_coordinates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletesections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $resp = $this->sectionsCoordinatesRepo->delete($sectionsCoordinates->id);
        $this->assertTrue($resp);
        $this->assertNull(sections_coordinates::find($sectionsCoordinates->id), 'sections_coordinates should not exist in DB');
    }
}
