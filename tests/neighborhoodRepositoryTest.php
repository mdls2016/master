<?php

use App\Models\neighborhoods;
use App\Repositories\neighborhoodRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class neighborhoodRepositoryTest extends TestCase
{
    use MakeneighborhoodTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var neighborhoodRepository
     */
    protected $neighborhoodRepo;

    public function setUp()
    {
        parent::setUp();
        $this->neighborhoodRepo = App::make(neighborhoodRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateneighborhood()
    {
        $neighborhood = $this->fakeneighborhoodData();
        $createdneighborhood = $this->neighborhoodRepo->create($neighborhood);
        $createdneighborhood = $createdneighborhood->toArray();
        $this->assertArrayHasKey('id', $createdneighborhood);
        $this->assertNotNull($createdneighborhood['id'], 'Created neighborhoods must have id specified');
        $this->assertNotNull(neighborhoods::find($createdneighborhood['id']), 'neighborhoods with given id must be in DB');
        $this->assertModelData($neighborhood, $createdneighborhood);
    }

    /**
     * @test read
     */
    public function testReadneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $dbneighborhood = $this->neighborhoodRepo->find($neighborhood->id);
        $dbneighborhood = $dbneighborhood->toArray();
        $this->assertModelData($neighborhood->toArray(), $dbneighborhood);
    }

    /**
     * @test update
     */
    public function testUpdateneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $fakeneighborhood = $this->fakeneighborhoodData();
        $updatedneighborhood = $this->neighborhoodRepo->update($fakeneighborhood, $neighborhood->id);
        $this->assertModelData($fakeneighborhood, $updatedneighborhood->toArray());
        $dbneighborhood = $this->neighborhoodRepo->find($neighborhood->id);
        $this->assertModelData($fakeneighborhood, $dbneighborhood->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $resp = $this->neighborhoodRepo->delete($neighborhood->id);
        $this->assertTrue($resp);
        $this->assertNull(neighborhoods::find($neighborhood->id), 'neighborhoods should not exist in DB');
    }
}
