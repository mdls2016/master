<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class entity_coordinatesApiTest extends TestCase
{
    use Makeentity_coordinatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateentity_coordinates()
    {
        $entityCoordinates = $this->fakeentity_coordinatesData();
        $this->json('POST', '/api/v1/entityCoordinates', $entityCoordinates);

        $this->assertApiResponse($entityCoordinates);
    }

    /**
     * @test
     */
    public function testReadentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $this->json('GET', '/api/v1/entityCoordinates/'.$entityCoordinates->id);

        $this->assertApiResponse($entityCoordinates->toArray());
    }

    /**
     * @test
     */
    public function testUpdateentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $editedentity_coordinates = $this->fakeentity_coordinatesData();

        $this->json('PUT', '/api/v1/entityCoordinates/'.$entityCoordinates->id, $editedentity_coordinates);

        $this->assertApiResponse($editedentity_coordinates);
    }

    /**
     * @test
     */
    public function testDeleteentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $this->json('DELETE', '/api/v1/entityCoordinates/'.$entityCoordinates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/entityCoordinates/'.$entityCoordinates->id);

        $this->assertResponseStatus(404);
    }
}
