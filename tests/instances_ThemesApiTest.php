<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instances_ThemesApiTest extends TestCase
{
    use Makeinstances_ThemesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateinstances_Themes()
    {
        $instancesThemes = $this->fakeinstances_ThemesData();
        $this->json('POST', '/api/v1/instancesThemes', $instancesThemes);

        $this->assertApiResponse($instancesThemes);
    }

    /**
     * @test
     */
    public function testReadinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $this->json('GET', '/api/v1/instancesThemes/'.$instancesThemes->id);

        $this->assertApiResponse($instancesThemes->toArray());
    }

    /**
     * @test
     */
    public function testUpdateinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $editedinstances_Themes = $this->fakeinstances_ThemesData();

        $this->json('PUT', '/api/v1/instancesThemes/'.$instancesThemes->id, $editedinstances_Themes);

        $this->assertApiResponse($editedinstances_Themes);
    }

    /**
     * @test
     */
    public function testDeleteinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $this->json('DELETE', '/api/v1/instancesThemes/'.$instancesThemes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/instancesThemes/'.$instancesThemes->id);

        $this->assertResponseStatus(404);
    }
}
