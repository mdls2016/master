<?php

use App\Models\districts_coordinates;
use App\Repositories\districts_coordinatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class districts_coordinatesRepositoryTest extends TestCase
{
    use Makedistricts_coordinatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var districts_coordinatesRepository
     */
    protected $districtsCoordinatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->districtsCoordinatesRepo = App::make(districts_coordinatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatedistricts_coordinates()
    {
        $districtsCoordinates = $this->fakedistricts_coordinatesData();
        $createddistricts_coordinates = $this->districtsCoordinatesRepo->create($districtsCoordinates);
        $createddistricts_coordinates = $createddistricts_coordinates->toArray();
        $this->assertArrayHasKey('id', $createddistricts_coordinates);
        $this->assertNotNull($createddistricts_coordinates['id'], 'Created districts_coordinates must have id specified');
        $this->assertNotNull(districts_coordinates::find($createddistricts_coordinates['id']), 'districts_coordinates with given id must be in DB');
        $this->assertModelData($districtsCoordinates, $createddistricts_coordinates);
    }

    /**
     * @test read
     */
    public function testReaddistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $dbdistricts_coordinates = $this->districtsCoordinatesRepo->find($districtsCoordinates->id);
        $dbdistricts_coordinates = $dbdistricts_coordinates->toArray();
        $this->assertModelData($districtsCoordinates->toArray(), $dbdistricts_coordinates);
    }

    /**
     * @test update
     */
    public function testUpdatedistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $fakedistricts_coordinates = $this->fakedistricts_coordinatesData();
        $updateddistricts_coordinates = $this->districtsCoordinatesRepo->update($fakedistricts_coordinates, $districtsCoordinates->id);
        $this->assertModelData($fakedistricts_coordinates, $updateddistricts_coordinates->toArray());
        $dbdistricts_coordinates = $this->districtsCoordinatesRepo->find($districtsCoordinates->id);
        $this->assertModelData($fakedistricts_coordinates, $dbdistricts_coordinates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletedistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $resp = $this->districtsCoordinatesRepo->delete($districtsCoordinates->id);
        $this->assertTrue($resp);
        $this->assertNull(districts_coordinates::find($districtsCoordinates->id), 'districts_coordinates should not exist in DB');
    }
}
