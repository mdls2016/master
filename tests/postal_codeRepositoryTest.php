<?php

use App\Models\postal_codes;
use App\Repositories\postal_codeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class postal_codeRepositoryTest extends TestCase
{
    use Makepostal_codeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var postal_codeRepository
     */
    protected $postalCodeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->postalCodeRepo = App::make(postal_codeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatepostal_code()
    {
        $postalCode = $this->fakepostal_codeData();
        $createdpostal_code = $this->postalCodeRepo->create($postalCode);
        $createdpostal_code = $createdpostal_code->toArray();
        $this->assertArrayHasKey('id', $createdpostal_code);
        $this->assertNotNull($createdpostal_code['id'], 'Created postal_codes must have id specified');
        $this->assertNotNull(postal_codes::find($createdpostal_code['id']), 'postal_codes with given id must be in DB');
        $this->assertModelData($postalCode, $createdpostal_code);
    }

    /**
     * @test read
     */
    public function testReadpostal_code()
    {
        $postalCode = $this->makepostal_code();
        $dbpostal_code = $this->postalCodeRepo->find($postalCode->id);
        $dbpostal_code = $dbpostal_code->toArray();
        $this->assertModelData($postalCode->toArray(), $dbpostal_code);
    }

    /**
     * @test update
     */
    public function testUpdatepostal_code()
    {
        $postalCode = $this->makepostal_code();
        $fakepostal_code = $this->fakepostal_codeData();
        $updatedpostal_code = $this->postalCodeRepo->update($fakepostal_code, $postalCode->id);
        $this->assertModelData($fakepostal_code, $updatedpostal_code->toArray());
        $dbpostal_code = $this->postalCodeRepo->find($postalCode->id);
        $this->assertModelData($fakepostal_code, $dbpostal_code->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletepostal_code()
    {
        $postalCode = $this->makepostal_code();
        $resp = $this->postalCodeRepo->delete($postalCode->id);
        $this->assertTrue($resp);
        $this->assertNull(postal_codes::find($postalCode->id), 'postal_codes should not exist in DB');
    }
}
