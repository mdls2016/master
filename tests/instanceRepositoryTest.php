<?php

use App\Models\instance;
use App\Repositories\instanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instanceRepositoryTest extends TestCase
{
    use MakeinstanceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var instanceRepository
     */
    protected $instanceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->instanceRepo = App::make(instanceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateinstance()
    {
        $instance = $this->fakeinstanceData();
        $createdinstance = $this->instanceRepo->create($instance);
        $createdinstance = $createdinstance->toArray();
        $this->assertArrayHasKey('id', $createdinstance);
        $this->assertNotNull($createdinstance['id'], 'Created instance must have id specified');
        $this->assertNotNull(instance::find($createdinstance['id']), 'instance with given id must be in DB');
        $this->assertModelData($instance, $createdinstance);
    }

    /**
     * @test read
     */
    public function testReadinstance()
    {
        $instance = $this->makeinstance();
        $dbinstance = $this->instanceRepo->find($instance->id);
        $dbinstance = $dbinstance->toArray();
        $this->assertModelData($instance->toArray(), $dbinstance);
    }

    /**
     * @test update
     */
    public function testUpdateinstance()
    {
        $instance = $this->makeinstance();
        $fakeinstance = $this->fakeinstanceData();
        $updatedinstance = $this->instanceRepo->update($fakeinstance, $instance->id);
        $this->assertModelData($fakeinstance, $updatedinstance->toArray());
        $dbinstance = $this->instanceRepo->find($instance->id);
        $this->assertModelData($fakeinstance, $dbinstance->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteinstance()
    {
        $instance = $this->makeinstance();
        $resp = $this->instanceRepo->delete($instance->id);
        $this->assertTrue($resp);
        $this->assertNull(instance::find($instance->id), 'instance should not exist in DB');
    }
}
