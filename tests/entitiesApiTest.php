<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class entitiesApiTest extends TestCase
{
    use MakeentitiesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateentities()
    {
        $entities = $this->fakeentitiesData();
        $this->json('POST', '/api/v1/entities', $entities);

        $this->assertApiResponse($entities);
    }

    /**
     * @test
     */
    public function testReadentities()
    {
        $entities = $this->makeentities();
        $this->json('GET', '/api/v1/entities/'.$entities->id);

        $this->assertApiResponse($entities->toArray());
    }

    /**
     * @test
     */
    public function testUpdateentities()
    {
        $entities = $this->makeentities();
        $editedentities = $this->fakeentitiesData();

        $this->json('PUT', '/api/v1/entities/'.$entities->id, $editedentities);

        $this->assertApiResponse($editedentities);
    }

    /**
     * @test
     */
    public function testDeleteentities()
    {
        $entities = $this->makeentities();
        $this->json('DELETE', '/api/v1/entities/'.$entities->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/entities/'.$entities->id);

        $this->assertResponseStatus(404);
    }
}
