<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class municipalitiesApiTest extends TestCase
{
    use MakemunicipalitiesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatemunicipalities()
    {
        $municipalities = $this->fakemunicipalitiesData();
        $this->json('POST', '/api/v1/municipalities', $municipalities);

        $this->assertApiResponse($municipalities);
    }

    /**
     * @test
     */
    public function testReadmunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $this->json('GET', '/api/v1/municipalities/'.$municipalities->id);

        $this->assertApiResponse($municipalities->toArray());
    }

    /**
     * @test
     */
    public function testUpdatemunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $editedmunicipalities = $this->fakemunicipalitiesData();

        $this->json('PUT', '/api/v1/municipalities/'.$municipalities->id, $editedmunicipalities);

        $this->assertApiResponse($editedmunicipalities);
    }

    /**
     * @test
     */
    public function testDeletemunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $this->json('DELETE', '/api/v1/municipalities/'.$municipalities->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/municipalities/'.$municipalities->id);

        $this->assertResponseStatus(404);
    }
}
