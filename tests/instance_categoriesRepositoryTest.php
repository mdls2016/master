<?php

use App\Models\instance_categories;
use App\Repositories\instance_categoriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instance_categoriesRepositoryTest extends TestCase
{
    use Makeinstance_categoriesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var instance_categoriesRepository
     */
    protected $instanceCategoriesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->instanceCategoriesRepo = App::make(instance_categoriesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateinstance_categories()
    {
        $instanceCategories = $this->fakeinstance_categoriesData();
        $createdinstance_categories = $this->instanceCategoriesRepo->create($instanceCategories);
        $createdinstance_categories = $createdinstance_categories->toArray();
        $this->assertArrayHasKey('id', $createdinstance_categories);
        $this->assertNotNull($createdinstance_categories['id'], 'Created instance_categories must have id specified');
        $this->assertNotNull(instance_categories::find($createdinstance_categories['id']), 'instance_categories with given id must be in DB');
        $this->assertModelData($instanceCategories, $createdinstance_categories);
    }

    /**
     * @test read
     */
    public function testReadinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $dbinstance_categories = $this->instanceCategoriesRepo->find($instanceCategories->id);
        $dbinstance_categories = $dbinstance_categories->toArray();
        $this->assertModelData($instanceCategories->toArray(), $dbinstance_categories);
    }

    /**
     * @test update
     */
    public function testUpdateinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $fakeinstance_categories = $this->fakeinstance_categoriesData();
        $updatedinstance_categories = $this->instanceCategoriesRepo->update($fakeinstance_categories, $instanceCategories->id);
        $this->assertModelData($fakeinstance_categories, $updatedinstance_categories->toArray());
        $dbinstance_categories = $this->instanceCategoriesRepo->find($instanceCategories->id);
        $this->assertModelData($fakeinstance_categories, $dbinstance_categories->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $resp = $this->instanceCategoriesRepo->delete($instanceCategories->id);
        $this->assertTrue($resp);
        $this->assertNull(instance_categories::find($instanceCategories->id), 'instance_categories should not exist in DB');
    }
}
