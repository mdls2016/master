<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class local_districtApiTest extends TestCase
{
    use Makelocal_districtTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatelocal_district()
    {
        $localDistrict = $this->fakelocal_districtData();
        $this->json('POST', '/api/v1/localDistricts', $localDistrict);

        $this->assertApiResponse($localDistrict);
    }

    /**
     * @test
     */
    public function testReadlocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $this->json('GET', '/api/v1/localDistricts/'.$localDistrict->id);

        $this->assertApiResponse($localDistrict->toArray());
    }

    /**
     * @test
     */
    public function testUpdatelocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $editedlocal_district = $this->fakelocal_districtData();

        $this->json('PUT', '/api/v1/localDistricts/'.$localDistrict->id, $editedlocal_district);

        $this->assertApiResponse($editedlocal_district);
    }

    /**
     * @test
     */
    public function testDeletelocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $this->json('DELETE', '/api/v1/localDistricts/'.$localDistrict->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/localDistricts/'.$localDistrict->id);

        $this->assertResponseStatus(404);
    }
}
