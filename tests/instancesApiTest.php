<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instancesApiTest extends TestCase
{
    use MakeinstancesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateinstances()
    {
        $instances = $this->fakeinstancesData();
        $this->json('POST', '/api/v1/instances', $instances);

        $this->assertApiResponse($instances);
    }

    /**
     * @test
     */
    public function testReadinstances()
    {
        $instances = $this->makeinstances();
        $this->json('GET', '/api/v1/instances/'.$instances->id);

        $this->assertApiResponse($instances->toArray());
    }

    /**
     * @test
     */
    public function testUpdateinstances()
    {
        $instances = $this->makeinstances();
        $editedinstances = $this->fakeinstancesData();

        $this->json('PUT', '/api/v1/instances/'.$instances->id, $editedinstances);

        $this->assertApiResponse($editedinstances);
    }

    /**
     * @test
     */
    public function testDeleteinstances()
    {
        $instances = $this->makeinstances();
        $this->json('DELETE', '/api/v1/instances/'.$instances->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/instances/'.$instances->id);

        $this->assertResponseStatus(404);
    }
}
