<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instanceApiTest extends TestCase
{
    use MakeinstanceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateinstance()
    {
        $instance = $this->fakeinstanceData();
        $this->json('POST', '/api/v1/instances', $instance);

        $this->assertApiResponse($instance);
    }

    /**
     * @test
     */
    public function testReadinstance()
    {
        $instance = $this->makeinstance();
        $this->json('GET', '/api/v1/instances/'.$instance->id);

        $this->assertApiResponse($instance->toArray());
    }

    /**
     * @test
     */
    public function testUpdateinstance()
    {
        $instance = $this->makeinstance();
        $editedinstance = $this->fakeinstanceData();

        $this->json('PUT', '/api/v1/instances/'.$instance->id, $editedinstance);

        $this->assertApiResponse($editedinstance);
    }

    /**
     * @test
     */
    public function testDeleteinstance()
    {
        $instance = $this->makeinstance();
        $this->json('DELETE', '/api/v1/instances/'.$instance->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/instances/'.$instance->id);

        $this->assertResponseStatus(404);
    }
}
