<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class sections_coordinatesApiTest extends TestCase
{
    use Makesections_coordinatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatesections_coordinates()
    {
        $sectionsCoordinates = $this->fakesections_coordinatesData();
        $this->json('POST', '/api/v1/sectionsCoordinates', $sectionsCoordinates);

        $this->assertApiResponse($sectionsCoordinates);
    }

    /**
     * @test
     */
    public function testReadsections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $this->json('GET', '/api/v1/sectionsCoordinates/'.$sectionsCoordinates->id);

        $this->assertApiResponse($sectionsCoordinates->toArray());
    }

    /**
     * @test
     */
    public function testUpdatesections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $editedsections_coordinates = $this->fakesections_coordinatesData();

        $this->json('PUT', '/api/v1/sectionsCoordinates/'.$sectionsCoordinates->id, $editedsections_coordinates);

        $this->assertApiResponse($editedsections_coordinates);
    }

    /**
     * @test
     */
    public function testDeletesections_coordinates()
    {
        $sectionsCoordinates = $this->makesections_coordinates();
        $this->json('DELETE', '/api/v1/sectionsCoordinates/'.$sectionsCoordinates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sectionsCoordinates/'.$sectionsCoordinates->id);

        $this->assertResponseStatus(404);
    }
}
