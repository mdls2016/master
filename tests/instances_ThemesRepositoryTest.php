<?php

use App\Models\instances_Themes;
use App\Repositories\instances_ThemesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instances_ThemesRepositoryTest extends TestCase
{
    use Makeinstances_ThemesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var instances_ThemesRepository
     */
    protected $instancesThemesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->instancesThemesRepo = App::make(instances_ThemesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateinstances_Themes()
    {
        $instancesThemes = $this->fakeinstances_ThemesData();
        $createdinstances_Themes = $this->instancesThemesRepo->create($instancesThemes);
        $createdinstances_Themes = $createdinstances_Themes->toArray();
        $this->assertArrayHasKey('id', $createdinstances_Themes);
        $this->assertNotNull($createdinstances_Themes['id'], 'Created instances_Themes must have id specified');
        $this->assertNotNull(instances_Themes::find($createdinstances_Themes['id']), 'instances_Themes with given id must be in DB');
        $this->assertModelData($instancesThemes, $createdinstances_Themes);
    }

    /**
     * @test read
     */
    public function testReadinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $dbinstances_Themes = $this->instancesThemesRepo->find($instancesThemes->id);
        $dbinstances_Themes = $dbinstances_Themes->toArray();
        $this->assertModelData($instancesThemes->toArray(), $dbinstances_Themes);
    }

    /**
     * @test update
     */
    public function testUpdateinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $fakeinstances_Themes = $this->fakeinstances_ThemesData();
        $updatedinstances_Themes = $this->instancesThemesRepo->update($fakeinstances_Themes, $instancesThemes->id);
        $this->assertModelData($fakeinstances_Themes, $updatedinstances_Themes->toArray());
        $dbinstances_Themes = $this->instancesThemesRepo->find($instancesThemes->id);
        $this->assertModelData($fakeinstances_Themes, $dbinstances_Themes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteinstances_Themes()
    {
        $instancesThemes = $this->makeinstances_Themes();
        $resp = $this->instancesThemesRepo->delete($instancesThemes->id);
        $this->assertTrue($resp);
        $this->assertNull(instances_Themes::find($instancesThemes->id), 'instances_Themes should not exist in DB');
    }
}
