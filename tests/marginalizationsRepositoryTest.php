<?php

use App\Models\marginalizations;
use App\Repositories\marginalizationsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class marginalizationsRepositoryTest extends TestCase
{
    use MakemarginalizationsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var marginalizationsRepository
     */
    protected $marginalizationsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->marginalizationsRepo = App::make(marginalizationsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatemarginalizations()
    {
        $marginalizations = $this->fakemarginalizationsData();
        $createdmarginalizations = $this->marginalizationsRepo->create($marginalizations);
        $createdmarginalizations = $createdmarginalizations->toArray();
        $this->assertArrayHasKey('id', $createdmarginalizations);
        $this->assertNotNull($createdmarginalizations['id'], 'Created marginalizations must have id specified');
        $this->assertNotNull(marginalizations::find($createdmarginalizations['id']), 'marginalizations with given id must be in DB');
        $this->assertModelData($marginalizations, $createdmarginalizations);
    }

    /**
     * @test read
     */
    public function testReadmarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $dbmarginalizations = $this->marginalizationsRepo->find($marginalizations->id);
        $dbmarginalizations = $dbmarginalizations->toArray();
        $this->assertModelData($marginalizations->toArray(), $dbmarginalizations);
    }

    /**
     * @test update
     */
    public function testUpdatemarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $fakemarginalizations = $this->fakemarginalizationsData();
        $updatedmarginalizations = $this->marginalizationsRepo->update($fakemarginalizations, $marginalizations->id);
        $this->assertModelData($fakemarginalizations, $updatedmarginalizations->toArray());
        $dbmarginalizations = $this->marginalizationsRepo->find($marginalizations->id);
        $this->assertModelData($fakemarginalizations, $dbmarginalizations->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletemarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $resp = $this->marginalizationsRepo->delete($marginalizations->id);
        $this->assertTrue($resp);
        $this->assertNull(marginalizations::find($marginalizations->id), 'marginalizations should not exist in DB');
    }
}
