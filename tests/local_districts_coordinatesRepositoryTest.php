<?php

use App\Models\local_districts_coordinates;
use App\Repositories\local_districts_coordinatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class local_districts_coordinatesRepositoryTest extends TestCase
{
    use Makelocal_districts_coordinatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var local_districts_coordinatesRepository
     */
    protected $localDistrictsCoordinatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->localDistrictsCoordinatesRepo = App::make(local_districts_coordinatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->fakelocal_districts_coordinatesData();
        $createdlocal_districts_coordinates = $this->localDistrictsCoordinatesRepo->create($localDistrictsCoordinates);
        $createdlocal_districts_coordinates = $createdlocal_districts_coordinates->toArray();
        $this->assertArrayHasKey('id', $createdlocal_districts_coordinates);
        $this->assertNotNull($createdlocal_districts_coordinates['id'], 'Created local_districts_coordinates must have id specified');
        $this->assertNotNull(local_districts_coordinates::find($createdlocal_districts_coordinates['id']), 'local_districts_coordinates with given id must be in DB');
        $this->assertModelData($localDistrictsCoordinates, $createdlocal_districts_coordinates);
    }

    /**
     * @test read
     */
    public function testReadlocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $dblocal_districts_coordinates = $this->localDistrictsCoordinatesRepo->find($localDistrictsCoordinates->id);
        $dblocal_districts_coordinates = $dblocal_districts_coordinates->toArray();
        $this->assertModelData($localDistrictsCoordinates->toArray(), $dblocal_districts_coordinates);
    }

    /**
     * @test update
     */
    public function testUpdatelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $fakelocal_districts_coordinates = $this->fakelocal_districts_coordinatesData();
        $updatedlocal_districts_coordinates = $this->localDistrictsCoordinatesRepo->update($fakelocal_districts_coordinates, $localDistrictsCoordinates->id);
        $this->assertModelData($fakelocal_districts_coordinates, $updatedlocal_districts_coordinates->toArray());
        $dblocal_districts_coordinates = $this->localDistrictsCoordinatesRepo->find($localDistrictsCoordinates->id);
        $this->assertModelData($fakelocal_districts_coordinates, $dblocal_districts_coordinates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $resp = $this->localDistrictsCoordinatesRepo->delete($localDistrictsCoordinates->id);
        $this->assertTrue($resp);
        $this->assertNull(local_districts_coordinates::find($localDistrictsCoordinates->id), 'local_districts_coordinates should not exist in DB');
    }
}
