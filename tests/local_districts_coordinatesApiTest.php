<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class local_districts_coordinatesApiTest extends TestCase
{
    use Makelocal_districts_coordinatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->fakelocal_districts_coordinatesData();
        $this->json('POST', '/api/v1/localDistrictsCoordinates', $localDistrictsCoordinates);

        $this->assertApiResponse($localDistrictsCoordinates);
    }

    /**
     * @test
     */
    public function testReadlocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $this->json('GET', '/api/v1/localDistrictsCoordinates/'.$localDistrictsCoordinates->id);

        $this->assertApiResponse($localDistrictsCoordinates->toArray());
    }

    /**
     * @test
     */
    public function testUpdatelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $editedlocal_districts_coordinates = $this->fakelocal_districts_coordinatesData();

        $this->json('PUT', '/api/v1/localDistrictsCoordinates/'.$localDistrictsCoordinates->id, $editedlocal_districts_coordinates);

        $this->assertApiResponse($editedlocal_districts_coordinates);
    }

    /**
     * @test
     */
    public function testDeletelocal_districts_coordinates()
    {
        $localDistrictsCoordinates = $this->makelocal_districts_coordinates();
        $this->json('DELETE', '/api/v1/localDistrictsCoordinates/'.$localDistrictsCoordinates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/localDistrictsCoordinates/'.$localDistrictsCoordinates->id);

        $this->assertResponseStatus(404);
    }
}
