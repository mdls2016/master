<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class subthemesApiTest extends TestCase
{
    use MakesubthemesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatesubthemes()
    {
        $subthemes = $this->fakesubthemesData();
        $this->json('POST', '/api/v1/subthemes', $subthemes);

        $this->assertApiResponse($subthemes);
    }

    /**
     * @test
     */
    public function testReadsubthemes()
    {
        $subthemes = $this->makesubthemes();
        $this->json('GET', '/api/v1/subthemes/'.$subthemes->id);

        $this->assertApiResponse($subthemes->toArray());
    }

    /**
     * @test
     */
    public function testUpdatesubthemes()
    {
        $subthemes = $this->makesubthemes();
        $editedsubthemes = $this->fakesubthemesData();

        $this->json('PUT', '/api/v1/subthemes/'.$subthemes->id, $editedsubthemes);

        $this->assertApiResponse($editedsubthemes);
    }

    /**
     * @test
     */
    public function testDeletesubthemes()
    {
        $subthemes = $this->makesubthemes();
        $this->json('DELETE', '/api/v1/subthemes/'.$subthemes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/subthemes/'.$subthemes->id);

        $this->assertResponseStatus(404);
    }
}
