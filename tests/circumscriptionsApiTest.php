<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class circumscriptionsApiTest extends TestCase
{
    use MakecircumscriptionsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatecircumscriptions()
    {
        $circumscriptions = $this->fakecircumscriptionsData();
        $this->json('POST', '/api/v1/circumscriptions', $circumscriptions);

        $this->assertApiResponse($circumscriptions);
    }

    /**
     * @test
     */
    public function testReadcircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $this->json('GET', '/api/v1/circumscriptions/'.$circumscriptions->id);

        $this->assertApiResponse($circumscriptions->toArray());
    }

    /**
     * @test
     */
    public function testUpdatecircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $editedcircumscriptions = $this->fakecircumscriptionsData();

        $this->json('PUT', '/api/v1/circumscriptions/'.$circumscriptions->id, $editedcircumscriptions);

        $this->assertApiResponse($editedcircumscriptions);
    }

    /**
     * @test
     */
    public function testDeletecircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $this->json('DELETE', '/api/v1/circumscriptions/'.$circumscriptions->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/circumscriptions/'.$circumscriptions->id);

        $this->assertResponseStatus(404);
    }
}
