<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class settlementApiTest extends TestCase
{
    use MakesettlementTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatesettlement()
    {
        $settlement = $this->fakesettlementData();
        $this->json('POST', '/api/v1/settlements', $settlement);

        $this->assertApiResponse($settlement);
    }

    /**
     * @test
     */
    public function testReadsettlement()
    {
        $settlement = $this->makesettlement();
        $this->json('GET', '/api/v1/settlements/'.$settlement->id);

        $this->assertApiResponse($settlement->toArray());
    }

    /**
     * @test
     */
    public function testUpdatesettlement()
    {
        $settlement = $this->makesettlement();
        $editedsettlement = $this->fakesettlementData();

        $this->json('PUT', '/api/v1/settlements/'.$settlement->id, $editedsettlement);

        $this->assertApiResponse($editedsettlement);
    }

    /**
     * @test
     */
    public function testDeletesettlement()
    {
        $settlement = $this->makesettlement();
        $this->json('DELETE', '/api/v1/settlements/'.$settlement->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/settlements/'.$settlement->id);

        $this->assertResponseStatus(404);
    }
}
