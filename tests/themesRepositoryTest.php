<?php

use App\Models\themes;
use App\Repositories\themesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class themesRepositoryTest extends TestCase
{
    use MakethemesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var themesRepository
     */
    protected $themesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->themesRepo = App::make(themesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatethemes()
    {
        $themes = $this->fakethemesData();
        $createdthemes = $this->themesRepo->create($themes);
        $createdthemes = $createdthemes->toArray();
        $this->assertArrayHasKey('id', $createdthemes);
        $this->assertNotNull($createdthemes['id'], 'Created themes must have id specified');
        $this->assertNotNull(themes::find($createdthemes['id']), 'themes with given id must be in DB');
        $this->assertModelData($themes, $createdthemes);
    }

    /**
     * @test read
     */
    public function testReadthemes()
    {
        $themes = $this->makethemes();
        $dbthemes = $this->themesRepo->find($themes->id);
        $dbthemes = $dbthemes->toArray();
        $this->assertModelData($themes->toArray(), $dbthemes);
    }

    /**
     * @test update
     */
    public function testUpdatethemes()
    {
        $themes = $this->makethemes();
        $fakethemes = $this->fakethemesData();
        $updatedthemes = $this->themesRepo->update($fakethemes, $themes->id);
        $this->assertModelData($fakethemes, $updatedthemes->toArray());
        $dbthemes = $this->themesRepo->find($themes->id);
        $this->assertModelData($fakethemes, $dbthemes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletethemes()
    {
        $themes = $this->makethemes();
        $resp = $this->themesRepo->delete($themes->id);
        $this->assertTrue($resp);
        $this->assertNull(themes::find($themes->id), 'themes should not exist in DB');
    }
}
