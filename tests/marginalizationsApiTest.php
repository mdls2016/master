<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class marginalizationsApiTest extends TestCase
{
    use MakemarginalizationsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatemarginalizations()
    {
        $marginalizations = $this->fakemarginalizationsData();
        $this->json('POST', '/api/v1/marginalizations', $marginalizations);

        $this->assertApiResponse($marginalizations);
    }

    /**
     * @test
     */
    public function testReadmarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $this->json('GET', '/api/v1/marginalizations/'.$marginalizations->id);

        $this->assertApiResponse($marginalizations->toArray());
    }

    /**
     * @test
     */
    public function testUpdatemarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $editedmarginalizations = $this->fakemarginalizationsData();

        $this->json('PUT', '/api/v1/marginalizations/'.$marginalizations->id, $editedmarginalizations);

        $this->assertApiResponse($editedmarginalizations);
    }

    /**
     * @test
     */
    public function testDeletemarginalizations()
    {
        $marginalizations = $this->makemarginalizations();
        $this->json('DELETE', '/api/v1/marginalizations/'.$marginalizations->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/marginalizations/'.$marginalizations->id);

        $this->assertResponseStatus(404);
    }
}
