<?php

use Faker\Factory as Faker;
use App\Models\local_districts_coordinates;
use App\Repositories\local_districts_coordinatesRepository;

trait Makelocal_districts_coordinatesTrait
{
    /**
     * Create fake instance of local_districts_coordinates and save it in database
     *
     * @param array $localDistrictsCoordinatesFields
     * @return local_districts_coordinates
     */
    public function makelocal_districts_coordinates($localDistrictsCoordinatesFields = [])
    {
        /** @var local_districts_coordinatesRepository $localDistrictsCoordinatesRepo */
        $localDistrictsCoordinatesRepo = App::make(local_districts_coordinatesRepository::class);
        $theme = $this->fakelocal_districts_coordinatesData($localDistrictsCoordinatesFields);
        return $localDistrictsCoordinatesRepo->create($theme);
    }

    /**
     * Get fake instance of local_districts_coordinates
     *
     * @param array $localDistrictsCoordinatesFields
     * @return local_districts_coordinates
     */
    public function fakelocal_districts_coordinates($localDistrictsCoordinatesFields = [])
    {
        return new local_districts_coordinates($this->fakelocal_districts_coordinatesData($localDistrictsCoordinatesFields));
    }

    /**
     * Get fake data of local_districts_coordinates
     *
     * @param array $postFields
     * @return array
     */
    public function fakelocal_districts_coordinatesData($localDistrictsCoordinatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'entity_id' => $fake->randomDigitNotNull,
            'coordinates' => $fake->word,
            'identifier' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $localDistrictsCoordinatesFields);
    }
}
