<?php

use Faker\Factory as Faker;
use App\Models\instance_categories;
use App\Repositories\instance_categoriesRepository;

trait Makeinstance_categoriesTrait
{
    /**
     * Create fake instance of instance_categories and save it in database
     *
     * @param array $instanceCategoriesFields
     * @return instance_categories
     */
    public function makeinstance_categories($instanceCategoriesFields = [])
    {
        /** @var instance_categoriesRepository $instanceCategoriesRepo */
        $instanceCategoriesRepo = App::make(instance_categoriesRepository::class);
        $theme = $this->fakeinstance_categoriesData($instanceCategoriesFields);
        return $instanceCategoriesRepo->create($theme);
    }

    /**
     * Get fake instance of instance_categories
     *
     * @param array $instanceCategoriesFields
     * @return instance_categories
     */
    public function fakeinstance_categories($instanceCategoriesFields = [])
    {
        return new instance_categories($this->fakeinstance_categoriesData($instanceCategoriesFields));
    }

    /**
     * Get fake data of instance_categories
     *
     * @param array $postFields
     * @return array
     */
    public function fakeinstance_categoriesData($instanceCategoriesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'instance_id' => $fake->randomDigitNotNull,
            'category_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $instanceCategoriesFields);
    }
}
