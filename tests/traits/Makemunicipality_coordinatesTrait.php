<?php

use Faker\Factory as Faker;
use App\Models\municipality_coordinates;
use App\Repositories\municipality_coordinatesRepository;

trait Makemunicipality_coordinatesTrait
{
    /**
     * Create fake instance of municipality_coordinates and save it in database
     *
     * @param array $municipalityCoordinatesFields
     * @return municipality_coordinates
     */
    public function makemunicipality_coordinates($municipalityCoordinatesFields = [])
    {
        /** @var municipality_coordinatesRepository $municipalityCoordinatesRepo */
        $municipalityCoordinatesRepo = App::make(municipality_coordinatesRepository::class);
        $theme = $this->fakemunicipality_coordinatesData($municipalityCoordinatesFields);
        return $municipalityCoordinatesRepo->create($theme);
    }

    /**
     * Get fake instance of municipality_coordinates
     *
     * @param array $municipalityCoordinatesFields
     * @return municipality_coordinates
     */
    public function fakemunicipality_coordinates($municipalityCoordinatesFields = [])
    {
        return new municipality_coordinates($this->fakemunicipality_coordinatesData($municipalityCoordinatesFields));
    }

    /**
     * Get fake data of municipality_coordinates
     *
     * @param array $postFields
     * @return array
     */
    public function fakemunicipality_coordinatesData($municipalityCoordinatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'municipality_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'coordinates' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $municipalityCoordinatesFields);
    }
}
