<?php

use Faker\Factory as Faker;
use App\Models\neighborhoods;
use App\Repositories\neighborhoodRepository;

trait MakeneighborhoodTrait
{
    /**
     * Create fake instance of neighborhoods and save it in database
     *
     * @param array $neighborhoodFields
     * @return neighborhoods
     */
    public function makeneighborhood($neighborhoodFields = [])
    {
        /** @var neighborhoodRepository $neighborhoodRepo */
        $neighborhoodRepo = App::make(neighborhoodRepository::class);
        $theme = $this->fakeneighborhoodData($neighborhoodFields);
        return $neighborhoodRepo->create($theme);
    }

    /**
     * Get fake instance of neighborhoods
     *
     * @param array $neighborhoodFields
     * @return neighborhoods
     */
    public function fakeneighborhood($neighborhoodFields = [])
    {
        return new neighborhoods($this->fakeneighborhoodData($neighborhoodFields));
    }

    /**
     * Get fake data of neighborhoods
     *
     * @param array $postFields
     * @return array
     */
    public function fakeneighborhoodData($neighborhoodFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_municipality' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $neighborhoodFields);
    }
}
