<?php

use Faker\Factory as Faker;
use App\Models\local_district;
use App\Repositories\local_districtRepository;

trait Makelocal_districtTrait
{
    /**
     * Create fake instance of local_district and save it in database
     *
     * @param array $localDistrictFields
     * @return local_district
     */
    public function makelocal_district($localDistrictFields = [])
    {
        /** @var local_districtRepository $localDistrictRepo */
        $localDistrictRepo = App::make(local_districtRepository::class);
        $theme = $this->fakelocal_districtData($localDistrictFields);
        return $localDistrictRepo->create($theme);
    }

    /**
     * Get fake instance of local_district
     *
     * @param array $localDistrictFields
     * @return local_district
     */
    public function fakelocal_district($localDistrictFields = [])
    {
        return new local_district($this->fakelocal_districtData($localDistrictFields));
    }

    /**
     * Get fake data of local_district
     *
     * @param array $postFields
     * @return array
     */
    public function fakelocal_districtData($localDistrictFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'entity_id' => $fake->randomDigitNotNull,
            'identifier' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $localDistrictFields);
    }
}
