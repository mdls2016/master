<?php

use Faker\Factory as Faker;
use App\Models\sections;
use App\Repositories\sectionRepository;

trait MakesectionTrait
{
    /**
     * Create fake instance of sections and save it in database
     *
     * @param array $sectionFields
     * @return sections
     */
    public function makesection($sectionFields = [])
    {
        /** @var sectionRepository $sectionRepo */
        $sectionRepo = App::make(sectionRepository::class);
        $theme = $this->fakesectionData($sectionFields);
        return $sectionRepo->create($theme);
    }

    /**
     * Get fake instance of sections
     *
     * @param array $sectionFields
     * @return sections
     */
    public function fakesection($sectionFields = [])
    {
        return new sections($this->fakesectionData($sectionFields));
    }

    /**
     * Get fake data of sections
     *
     * @param array $postFields
     * @return array
     */
    public function fakesectionData($sectionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_municipality' => $fake->randomDigitNotNull,
            'identifier' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $sectionFields);
    }
}
