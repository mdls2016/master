<?php

use Faker\Factory as Faker;
use App\Models\instance;
use App\Repositories\instanceRepository;

trait MakeinstanceTrait
{
    /**
     * Create fake instance of instance and save it in database
     *
     * @param array $instanceFields
     * @return instance
     */
    public function makeinstance($instanceFields = [])
    {
        /** @var instanceRepository $instanceRepo */
        $instanceRepo = App::make(instanceRepository::class);
        $theme = $this->fakeinstanceData($instanceFields);
        return $instanceRepo->create($theme);
    }

    /**
     * Get fake instance of instance
     *
     * @param array $instanceFields
     * @return instance
     */
    public function fakeinstance($instanceFields = [])
    {
        return new instance($this->fakeinstanceData($instanceFields));
    }

    /**
     * Get fake data of instance
     *
     * @param array $postFields
     * @return array
     */
    public function fakeinstanceData($instanceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'file' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $instanceFields);
    }
}
