<?php

use Faker\Factory as Faker;
use App\Models\circumscriptions;
use App\Repositories\circumscriptionsRepository;

trait MakecircumscriptionsTrait
{
    /**
     * Create fake instance of circumscriptions and save it in database
     *
     * @param array $circumscriptionsFields
     * @return circumscriptions
     */
    public function makecircumscriptions($circumscriptionsFields = [])
    {
        /** @var circumscriptionsRepository $circumscriptionsRepo */
        $circumscriptionsRepo = App::make(circumscriptionsRepository::class);
        $theme = $this->fakecircumscriptionsData($circumscriptionsFields);
        return $circumscriptionsRepo->create($theme);
    }

    /**
     * Get fake instance of circumscriptions
     *
     * @param array $circumscriptionsFields
     * @return circumscriptions
     */
    public function fakecircumscriptions($circumscriptionsFields = [])
    {
        return new circumscriptions($this->fakecircumscriptionsData($circumscriptionsFields));
    }

    /**
     * Get fake data of circumscriptions
     *
     * @param array $postFields
     * @return array
     */
    public function fakecircumscriptionsData($circumscriptionsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $circumscriptionsFields);
    }
}
