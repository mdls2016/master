<?php

use Faker\Factory as Faker;
use App\Models\themes;
use App\Repositories\themesRepository;

trait MakethemesTrait
{
    /**
     * Create fake instance of themes and save it in database
     *
     * @param array $themesFields
     * @return themes
     */
    public function makethemes($themesFields = [])
    {
        /** @var themesRepository $themesRepo */
        $themesRepo = App::make(themesRepository::class);
        $theme = $this->fakethemesData($themesFields);
        return $themesRepo->create($theme);
    }

    /**
     * Get fake instance of themes
     *
     * @param array $themesFields
     * @return themes
     */
    public function fakethemes($themesFields = [])
    {
        return new themes($this->fakethemesData($themesFields));
    }

    /**
     * Get fake data of themes
     *
     * @param array $postFields
     * @return array
     */
    public function fakethemesData($themesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'icon' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $themesFields);
    }
}
