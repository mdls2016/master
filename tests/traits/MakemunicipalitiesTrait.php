<?php

use Faker\Factory as Faker;
use App\Models\municipalities;
use App\Repositories\municipalitiesRepository;

trait MakemunicipalitiesTrait
{
    /**
     * Create fake instance of municipalities and save it in database
     *
     * @param array $municipalitiesFields
     * @return municipalities
     */
    public function makemunicipalities($municipalitiesFields = [])
    {
        /** @var municipalitiesRepository $municipalitiesRepo */
        $municipalitiesRepo = App::make(municipalitiesRepository::class);
        $theme = $this->fakemunicipalitiesData($municipalitiesFields);
        return $municipalitiesRepo->create($theme);
    }

    /**
     * Get fake instance of municipalities
     *
     * @param array $municipalitiesFields
     * @return municipalities
     */
    public function fakemunicipalities($municipalitiesFields = [])
    {
        return new municipalities($this->fakemunicipalitiesData($municipalitiesFields));
    }

    /**
     * Get fake data of municipalities
     *
     * @param array $postFields
     * @return array
     */
    public function fakemunicipalitiesData($municipalitiesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'entity_id' => $fake->randomDigitNotNull,
            'identifier' => $fake->word,
            'name' => $fake->word,
            'population' => $fake->randomDigitNotNull,
            'indigenous' => $fake->word,
            'cnch' => $fake->word,
            'marginalization_id' => $fake->randomDigitNotNull,
            'district_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $municipalitiesFields);
    }
}
