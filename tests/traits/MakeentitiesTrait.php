<?php

use Faker\Factory as Faker;
use App\Models\entities;
use App\Repositories\entitiesRepository;

trait MakeentitiesTrait
{
    /**
     * Create fake instance of entities and save it in database
     *
     * @param array $entitiesFields
     * @return entities
     */
    public function makeentities($entitiesFields = [])
    {
        /** @var entitiesRepository $entitiesRepo */
        $entitiesRepo = App::make(entitiesRepository::class);
        $theme = $this->fakeentitiesData($entitiesFields);
        return $entitiesRepo->create($theme);
    }

    /**
     * Get fake instance of entities
     *
     * @param array $entitiesFields
     * @return entities
     */
    public function fakeentities($entitiesFields = [])
    {
        return new entities($this->fakeentitiesData($entitiesFields));
    }

    /**
     * Get fake data of entities
     *
     * @param array $postFields
     * @return array
     */
    public function fakeentitiesData($entitiesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'circumscription_id' => $fake->randomDigitNotNull,
            'identifier' => $fake->word,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $entitiesFields);
    }
}
