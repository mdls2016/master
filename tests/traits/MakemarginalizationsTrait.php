<?php

use Faker\Factory as Faker;
use App\Models\marginalizations;
use App\Repositories\marginalizationsRepository;

trait MakemarginalizationsTrait
{
    /**
     * Create fake instance of marginalizations and save it in database
     *
     * @param array $marginalizationsFields
     * @return marginalizations
     */
    public function makemarginalizations($marginalizationsFields = [])
    {
        /** @var marginalizationsRepository $marginalizationsRepo */
        $marginalizationsRepo = App::make(marginalizationsRepository::class);
        $theme = $this->fakemarginalizationsData($marginalizationsFields);
        return $marginalizationsRepo->create($theme);
    }

    /**
     * Get fake instance of marginalizations
     *
     * @param array $marginalizationsFields
     * @return marginalizations
     */
    public function fakemarginalizations($marginalizationsFields = [])
    {
        return new marginalizations($this->fakemarginalizationsData($marginalizationsFields));
    }

    /**
     * Get fake data of marginalizations
     *
     * @param array $postFields
     * @return array
     */
    public function fakemarginalizationsData($marginalizationsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $marginalizationsFields);
    }
}
