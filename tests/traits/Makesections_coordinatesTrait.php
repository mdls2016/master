<?php

use Faker\Factory as Faker;
use App\Models\sections_coordinates;
use App\Repositories\sections_coordinatesRepository;

trait Makesections_coordinatesTrait
{
    /**
     * Create fake instance of sections_coordinates and save it in database
     *
     * @param array $sectionsCoordinatesFields
     * @return sections_coordinates
     */
    public function makesections_coordinates($sectionsCoordinatesFields = [])
    {
        /** @var sections_coordinatesRepository $sectionsCoordinatesRepo */
        $sectionsCoordinatesRepo = App::make(sections_coordinatesRepository::class);
        $theme = $this->fakesections_coordinatesData($sectionsCoordinatesFields);
        return $sectionsCoordinatesRepo->create($theme);
    }

    /**
     * Get fake instance of sections_coordinates
     *
     * @param array $sectionsCoordinatesFields
     * @return sections_coordinates
     */
    public function fakesections_coordinates($sectionsCoordinatesFields = [])
    {
        return new sections_coordinates($this->fakesections_coordinatesData($sectionsCoordinatesFields));
    }

    /**
     * Get fake data of sections_coordinates
     *
     * @param array $postFields
     * @return array
     */
    public function fakesections_coordinatesData($sectionsCoordinatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'coordinates' => $fake->word,
            'district_id' => $fake->randomDigitNotNull,
            'section_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $sectionsCoordinatesFields);
    }
}
