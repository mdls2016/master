<?php

use Faker\Factory as Faker;
use App\Models\instances_Themes;
use App\Repositories\instances_ThemesRepository;

trait Makeinstances_ThemesTrait
{
    /**
     * Create fake instance of instances_Themes and save it in database
     *
     * @param array $instancesThemesFields
     * @return instances_Themes
     */
    public function makeinstances_Themes($instancesThemesFields = [])
    {
        /** @var instances_ThemesRepository $instancesThemesRepo */
        $instancesThemesRepo = App::make(instances_ThemesRepository::class);
        $theme = $this->fakeinstances_ThemesData($instancesThemesFields);
        return $instancesThemesRepo->create($theme);
    }

    /**
     * Get fake instance of instances_Themes
     *
     * @param array $instancesThemesFields
     * @return instances_Themes
     */
    public function fakeinstances_Themes($instancesThemesFields = [])
    {
        return new instances_Themes($this->fakeinstances_ThemesData($instancesThemesFields));
    }

    /**
     * Get fake data of instances_Themes
     *
     * @param array $postFields
     * @return array
     */
    public function fakeinstances_ThemesData($instancesThemesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'instance_id' => $fake->randomDigitNotNull,
            'theme_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $instancesThemesFields);
    }
}
