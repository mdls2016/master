<?php

use Faker\Factory as Faker;
use App\Models\instances;
use App\Repositories\instancesRepository;

trait MakeinstancesTrait
{
    /**
     * Create fake instance of instances and save it in database
     *
     * @param array $instancesFields
     * @return instances
     */
    public function makeinstances($instancesFields = [])
    {
        /** @var instancesRepository $instancesRepo */
        $instancesRepo = App::make(instancesRepository::class);
        $theme = $this->fakeinstancesData($instancesFields);
        return $instancesRepo->create($theme);
    }

    /**
     * Get fake instance of instances
     *
     * @param array $instancesFields
     * @return instances
     */
    public function fakeinstances($instancesFields = [])
    {
        return new instances($this->fakeinstancesData($instancesFields));
    }

    /**
     * Get fake data of instances
     *
     * @param array $postFields
     * @return array
     */
    public function fakeinstancesData($instancesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'participant_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'shortname' => $fake->word,
            'entity_id' => $fake->randomDigitNotNull,
            'municipality_id' => $fake->randomDigitNotNull,
            'description' => $fake->randomDigitNotNull,
            'secretary_state' => $fake->word,
            'secretary_municipality' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $instancesFields);
    }
}
