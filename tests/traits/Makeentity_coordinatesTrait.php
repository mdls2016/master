<?php

use Faker\Factory as Faker;
use App\Models\entity_coordinates;
use App\Repositories\entity_coordinatesRepository;

trait Makeentity_coordinatesTrait
{
    /**
     * Create fake instance of entity_coordinates and save it in database
     *
     * @param array $entityCoordinatesFields
     * @return entity_coordinates
     */
    public function makeentity_coordinates($entityCoordinatesFields = [])
    {
        /** @var entity_coordinatesRepository $entityCoordinatesRepo */
        $entityCoordinatesRepo = App::make(entity_coordinatesRepository::class);
        $theme = $this->fakeentity_coordinatesData($entityCoordinatesFields);
        return $entityCoordinatesRepo->create($theme);
    }

    /**
     * Get fake instance of entity_coordinates
     *
     * @param array $entityCoordinatesFields
     * @return entity_coordinates
     */
    public function fakeentity_coordinates($entityCoordinatesFields = [])
    {
        return new entity_coordinates($this->fakeentity_coordinatesData($entityCoordinatesFields));
    }

    /**
     * Get fake data of entity_coordinates
     *
     * @param array $postFields
     * @return array
     */
    public function fakeentity_coordinatesData($entityCoordinatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'entity_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'coordinates' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $entityCoordinatesFields);
    }
}
