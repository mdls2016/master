<?php

use Faker\Factory as Faker;
use App\Models\settlements;
use App\Repositories\settlementRepository;

trait MakesettlementTrait
{
    /**
     * Create fake instance of settlements and save it in database
     *
     * @param array $settlementFields
     * @return settlements
     */
    public function makesettlement($settlementFields = [])
    {
        /** @var settlementRepository $settlementRepo */
        $settlementRepo = App::make(settlementRepository::class);
        $theme = $this->fakesettlementData($settlementFields);
        return $settlementRepo->create($theme);
    }

    /**
     * Get fake instance of settlements
     *
     * @param array $settlementFields
     * @return settlements
     */
    public function fakesettlement($settlementFields = [])
    {
        return new settlements($this->fakesettlementData($settlementFields));
    }

    /**
     * Get fake data of settlements
     *
     * @param array $postFields
     * @return array
     */
    public function fakesettlementData($settlementFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $settlementFields);
    }
}
