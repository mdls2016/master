<?php

use Faker\Factory as Faker;
use App\Models\districts_coordinates;
use App\Repositories\districts_coordinatesRepository;

trait Makedistricts_coordinatesTrait
{
    /**
     * Create fake instance of districts_coordinates and save it in database
     *
     * @param array $districtsCoordinatesFields
     * @return districts_coordinates
     */
    public function makedistricts_coordinates($districtsCoordinatesFields = [])
    {
        /** @var districts_coordinatesRepository $districtsCoordinatesRepo */
        $districtsCoordinatesRepo = App::make(districts_coordinatesRepository::class);
        $theme = $this->fakedistricts_coordinatesData($districtsCoordinatesFields);
        return $districtsCoordinatesRepo->create($theme);
    }

    /**
     * Get fake instance of districts_coordinates
     *
     * @param array $districtsCoordinatesFields
     * @return districts_coordinates
     */
    public function fakedistricts_coordinates($districtsCoordinatesFields = [])
    {
        return new districts_coordinates($this->fakedistricts_coordinatesData($districtsCoordinatesFields));
    }

    /**
     * Get fake data of districts_coordinates
     *
     * @param array $postFields
     * @return array
     */
    public function fakedistricts_coordinatesData($districtsCoordinatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'entity_id' => $fake->randomDigitNotNull,
            'identifier' => $fake->word,
            'coordinates' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $districtsCoordinatesFields);
    }
}
