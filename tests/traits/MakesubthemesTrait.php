<?php

use Faker\Factory as Faker;
use App\Models\subthemes;
use App\Repositories\subthemesRepository;

trait MakesubthemesTrait
{
    /**
     * Create fake instance of subthemes and save it in database
     *
     * @param array $subthemesFields
     * @return subthemes
     */
    public function makesubthemes($subthemesFields = [])
    {
        /** @var subthemesRepository $subthemesRepo */
        $subthemesRepo = App::make(subthemesRepository::class);
        $theme = $this->fakesubthemesData($subthemesFields);
        return $subthemesRepo->create($theme);
    }

    /**
     * Get fake instance of subthemes
     *
     * @param array $subthemesFields
     * @return subthemes
     */
    public function fakesubthemes($subthemesFields = [])
    {
        return new subthemes($this->fakesubthemesData($subthemesFields));
    }

    /**
     * Get fake data of subthemes
     *
     * @param array $postFields
     * @return array
     */
    public function fakesubthemesData($subthemesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'theme_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $subthemesFields);
    }
}
