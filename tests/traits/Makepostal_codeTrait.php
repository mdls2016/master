<?php

use Faker\Factory as Faker;
use App\Models\postal_codes;
use App\Repositories\postal_codeRepository;

trait Makepostal_codeTrait
{
    /**
     * Create fake instance of postal_codes and save it in database
     *
     * @param array $postalCodeFields
     * @return postal_codes
     */
    public function makepostal_code($postalCodeFields = [])
    {
        /** @var postal_codeRepository $postalCodeRepo */
        $postalCodeRepo = App::make(postal_codeRepository::class);
        $theme = $this->fakepostal_codeData($postalCodeFields);
        return $postalCodeRepo->create($theme);
    }

    /**
     * Get fake instance of postal_codes
     *
     * @param array $postalCodeFields
     * @return postal_codes
     */
    public function fakepostal_code($postalCodeFields = [])
    {
        return new postal_codes($this->fakepostal_codeData($postalCodeFields));
    }

    /**
     * Get fake data of postal_codes
     *
     * @param array $postFields
     * @return array
     */
    public function fakepostal_codeData($postalCodeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_neighborhood' => $fake->randomDigitNotNull,
            'identifier' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $postalCodeFields);
    }
}
