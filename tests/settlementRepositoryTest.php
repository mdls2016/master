<?php

use App\Models\settlements;
use App\Repositories\settlementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class settlementRepositoryTest extends TestCase
{
    use MakesettlementTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var settlementRepository
     */
    protected $settlementRepo;

    public function setUp()
    {
        parent::setUp();
        $this->settlementRepo = App::make(settlementRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatesettlement()
    {
        $settlement = $this->fakesettlementData();
        $createdsettlement = $this->settlementRepo->create($settlement);
        $createdsettlement = $createdsettlement->toArray();
        $this->assertArrayHasKey('id', $createdsettlement);
        $this->assertNotNull($createdsettlement['id'], 'Created settlements must have id specified');
        $this->assertNotNull(settlements::find($createdsettlement['id']), 'settlements with given id must be in DB');
        $this->assertModelData($settlement, $createdsettlement);
    }

    /**
     * @test read
     */
    public function testReadsettlement()
    {
        $settlement = $this->makesettlement();
        $dbsettlement = $this->settlementRepo->find($settlement->id);
        $dbsettlement = $dbsettlement->toArray();
        $this->assertModelData($settlement->toArray(), $dbsettlement);
    }

    /**
     * @test update
     */
    public function testUpdatesettlement()
    {
        $settlement = $this->makesettlement();
        $fakesettlement = $this->fakesettlementData();
        $updatedsettlement = $this->settlementRepo->update($fakesettlement, $settlement->id);
        $this->assertModelData($fakesettlement, $updatedsettlement->toArray());
        $dbsettlement = $this->settlementRepo->find($settlement->id);
        $this->assertModelData($fakesettlement, $dbsettlement->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletesettlement()
    {
        $settlement = $this->makesettlement();
        $resp = $this->settlementRepo->delete($settlement->id);
        $this->assertTrue($resp);
        $this->assertNull(settlements::find($settlement->id), 'settlements should not exist in DB');
    }
}
