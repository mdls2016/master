<?php

use App\Models\municipalities;
use App\Repositories\municipalitiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class municipalitiesRepositoryTest extends TestCase
{
    use MakemunicipalitiesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var municipalitiesRepository
     */
    protected $municipalitiesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->municipalitiesRepo = App::make(municipalitiesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatemunicipalities()
    {
        $municipalities = $this->fakemunicipalitiesData();
        $createdmunicipalities = $this->municipalitiesRepo->create($municipalities);
        $createdmunicipalities = $createdmunicipalities->toArray();
        $this->assertArrayHasKey('id', $createdmunicipalities);
        $this->assertNotNull($createdmunicipalities['id'], 'Created municipalities must have id specified');
        $this->assertNotNull(municipalities::find($createdmunicipalities['id']), 'municipalities with given id must be in DB');
        $this->assertModelData($municipalities, $createdmunicipalities);
    }

    /**
     * @test read
     */
    public function testReadmunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $dbmunicipalities = $this->municipalitiesRepo->find($municipalities->id);
        $dbmunicipalities = $dbmunicipalities->toArray();
        $this->assertModelData($municipalities->toArray(), $dbmunicipalities);
    }

    /**
     * @test update
     */
    public function testUpdatemunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $fakemunicipalities = $this->fakemunicipalitiesData();
        $updatedmunicipalities = $this->municipalitiesRepo->update($fakemunicipalities, $municipalities->id);
        $this->assertModelData($fakemunicipalities, $updatedmunicipalities->toArray());
        $dbmunicipalities = $this->municipalitiesRepo->find($municipalities->id);
        $this->assertModelData($fakemunicipalities, $dbmunicipalities->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletemunicipalities()
    {
        $municipalities = $this->makemunicipalities();
        $resp = $this->municipalitiesRepo->delete($municipalities->id);
        $this->assertTrue($resp);
        $this->assertNull(municipalities::find($municipalities->id), 'municipalities should not exist in DB');
    }
}
