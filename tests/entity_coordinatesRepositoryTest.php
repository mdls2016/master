<?php

use App\Models\entity_coordinates;
use App\Repositories\entity_coordinatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class entity_coordinatesRepositoryTest extends TestCase
{
    use Makeentity_coordinatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var entity_coordinatesRepository
     */
    protected $entityCoordinatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->entityCoordinatesRepo = App::make(entity_coordinatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateentity_coordinates()
    {
        $entityCoordinates = $this->fakeentity_coordinatesData();
        $createdentity_coordinates = $this->entityCoordinatesRepo->create($entityCoordinates);
        $createdentity_coordinates = $createdentity_coordinates->toArray();
        $this->assertArrayHasKey('id', $createdentity_coordinates);
        $this->assertNotNull($createdentity_coordinates['id'], 'Created entity_coordinates must have id specified');
        $this->assertNotNull(entity_coordinates::find($createdentity_coordinates['id']), 'entity_coordinates with given id must be in DB');
        $this->assertModelData($entityCoordinates, $createdentity_coordinates);
    }

    /**
     * @test read
     */
    public function testReadentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $dbentity_coordinates = $this->entityCoordinatesRepo->find($entityCoordinates->id);
        $dbentity_coordinates = $dbentity_coordinates->toArray();
        $this->assertModelData($entityCoordinates->toArray(), $dbentity_coordinates);
    }

    /**
     * @test update
     */
    public function testUpdateentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $fakeentity_coordinates = $this->fakeentity_coordinatesData();
        $updatedentity_coordinates = $this->entityCoordinatesRepo->update($fakeentity_coordinates, $entityCoordinates->id);
        $this->assertModelData($fakeentity_coordinates, $updatedentity_coordinates->toArray());
        $dbentity_coordinates = $this->entityCoordinatesRepo->find($entityCoordinates->id);
        $this->assertModelData($fakeentity_coordinates, $dbentity_coordinates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteentity_coordinates()
    {
        $entityCoordinates = $this->makeentity_coordinates();
        $resp = $this->entityCoordinatesRepo->delete($entityCoordinates->id);
        $this->assertTrue($resp);
        $this->assertNull(entity_coordinates::find($entityCoordinates->id), 'entity_coordinates should not exist in DB');
    }
}
