<?php

use App\Models\local_district;
use App\Repositories\local_districtRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class local_districtRepositoryTest extends TestCase
{
    use Makelocal_districtTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var local_districtRepository
     */
    protected $localDistrictRepo;

    public function setUp()
    {
        parent::setUp();
        $this->localDistrictRepo = App::make(local_districtRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatelocal_district()
    {
        $localDistrict = $this->fakelocal_districtData();
        $createdlocal_district = $this->localDistrictRepo->create($localDistrict);
        $createdlocal_district = $createdlocal_district->toArray();
        $this->assertArrayHasKey('id', $createdlocal_district);
        $this->assertNotNull($createdlocal_district['id'], 'Created local_district must have id specified');
        $this->assertNotNull(local_district::find($createdlocal_district['id']), 'local_district with given id must be in DB');
        $this->assertModelData($localDistrict, $createdlocal_district);
    }

    /**
     * @test read
     */
    public function testReadlocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $dblocal_district = $this->localDistrictRepo->find($localDistrict->id);
        $dblocal_district = $dblocal_district->toArray();
        $this->assertModelData($localDistrict->toArray(), $dblocal_district);
    }

    /**
     * @test update
     */
    public function testUpdatelocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $fakelocal_district = $this->fakelocal_districtData();
        $updatedlocal_district = $this->localDistrictRepo->update($fakelocal_district, $localDistrict->id);
        $this->assertModelData($fakelocal_district, $updatedlocal_district->toArray());
        $dblocal_district = $this->localDistrictRepo->find($localDistrict->id);
        $this->assertModelData($fakelocal_district, $dblocal_district->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletelocal_district()
    {
        $localDistrict = $this->makelocal_district();
        $resp = $this->localDistrictRepo->delete($localDistrict->id);
        $this->assertTrue($resp);
        $this->assertNull(local_district::find($localDistrict->id), 'local_district should not exist in DB');
    }
}
