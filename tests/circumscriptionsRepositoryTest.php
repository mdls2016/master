<?php

use App\Models\circumscriptions;
use App\Repositories\circumscriptionsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class circumscriptionsRepositoryTest extends TestCase
{
    use MakecircumscriptionsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var circumscriptionsRepository
     */
    protected $circumscriptionsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->circumscriptionsRepo = App::make(circumscriptionsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatecircumscriptions()
    {
        $circumscriptions = $this->fakecircumscriptionsData();
        $createdcircumscriptions = $this->circumscriptionsRepo->create($circumscriptions);
        $createdcircumscriptions = $createdcircumscriptions->toArray();
        $this->assertArrayHasKey('id', $createdcircumscriptions);
        $this->assertNotNull($createdcircumscriptions['id'], 'Created circumscriptions must have id specified');
        $this->assertNotNull(circumscriptions::find($createdcircumscriptions['id']), 'circumscriptions with given id must be in DB');
        $this->assertModelData($circumscriptions, $createdcircumscriptions);
    }

    /**
     * @test read
     */
    public function testReadcircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $dbcircumscriptions = $this->circumscriptionsRepo->find($circumscriptions->id);
        $dbcircumscriptions = $dbcircumscriptions->toArray();
        $this->assertModelData($circumscriptions->toArray(), $dbcircumscriptions);
    }

    /**
     * @test update
     */
    public function testUpdatecircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $fakecircumscriptions = $this->fakecircumscriptionsData();
        $updatedcircumscriptions = $this->circumscriptionsRepo->update($fakecircumscriptions, $circumscriptions->id);
        $this->assertModelData($fakecircumscriptions, $updatedcircumscriptions->toArray());
        $dbcircumscriptions = $this->circumscriptionsRepo->find($circumscriptions->id);
        $this->assertModelData($fakecircumscriptions, $dbcircumscriptions->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletecircumscriptions()
    {
        $circumscriptions = $this->makecircumscriptions();
        $resp = $this->circumscriptionsRepo->delete($circumscriptions->id);
        $this->assertTrue($resp);
        $this->assertNull(circumscriptions::find($circumscriptions->id), 'circumscriptions should not exist in DB');
    }
}
