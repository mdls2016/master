<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class neighborhoodApiTest extends TestCase
{
    use MakeneighborhoodTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateneighborhood()
    {
        $neighborhood = $this->fakeneighborhoodData();
        $this->json('POST', '/api/v1/neighborhoods', $neighborhood);

        $this->assertApiResponse($neighborhood);
    }

    /**
     * @test
     */
    public function testReadneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $this->json('GET', '/api/v1/neighborhoods/'.$neighborhood->id);

        $this->assertApiResponse($neighborhood->toArray());
    }

    /**
     * @test
     */
    public function testUpdateneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $editedneighborhood = $this->fakeneighborhoodData();

        $this->json('PUT', '/api/v1/neighborhoods/'.$neighborhood->id, $editedneighborhood);

        $this->assertApiResponse($editedneighborhood);
    }

    /**
     * @test
     */
    public function testDeleteneighborhood()
    {
        $neighborhood = $this->makeneighborhood();
        $this->json('DELETE', '/api/v1/neighborhoods/'.$neighborhood->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/neighborhoods/'.$neighborhood->id);

        $this->assertResponseStatus(404);
    }
}
