<?php

use App\Models\municipality_coordinates;
use App\Repositories\municipality_coordinatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class municipality_coordinatesRepositoryTest extends TestCase
{
    use Makemunicipality_coordinatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var municipality_coordinatesRepository
     */
    protected $municipalityCoordinatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->municipalityCoordinatesRepo = App::make(municipality_coordinatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->fakemunicipality_coordinatesData();
        $createdmunicipality_coordinates = $this->municipalityCoordinatesRepo->create($municipalityCoordinates);
        $createdmunicipality_coordinates = $createdmunicipality_coordinates->toArray();
        $this->assertArrayHasKey('id', $createdmunicipality_coordinates);
        $this->assertNotNull($createdmunicipality_coordinates['id'], 'Created municipality_coordinates must have id specified');
        $this->assertNotNull(municipality_coordinates::find($createdmunicipality_coordinates['id']), 'municipality_coordinates with given id must be in DB');
        $this->assertModelData($municipalityCoordinates, $createdmunicipality_coordinates);
    }

    /**
     * @test read
     */
    public function testReadmunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $dbmunicipality_coordinates = $this->municipalityCoordinatesRepo->find($municipalityCoordinates->id);
        $dbmunicipality_coordinates = $dbmunicipality_coordinates->toArray();
        $this->assertModelData($municipalityCoordinates->toArray(), $dbmunicipality_coordinates);
    }

    /**
     * @test update
     */
    public function testUpdatemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $fakemunicipality_coordinates = $this->fakemunicipality_coordinatesData();
        $updatedmunicipality_coordinates = $this->municipalityCoordinatesRepo->update($fakemunicipality_coordinates, $municipalityCoordinates->id);
        $this->assertModelData($fakemunicipality_coordinates, $updatedmunicipality_coordinates->toArray());
        $dbmunicipality_coordinates = $this->municipalityCoordinatesRepo->find($municipalityCoordinates->id);
        $this->assertModelData($fakemunicipality_coordinates, $dbmunicipality_coordinates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $resp = $this->municipalityCoordinatesRepo->delete($municipalityCoordinates->id);
        $this->assertTrue($resp);
        $this->assertNull(municipality_coordinates::find($municipalityCoordinates->id), 'municipality_coordinates should not exist in DB');
    }
}
