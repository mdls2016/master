<?php

use App\Models\entities;
use App\Repositories\entitiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class entitiesRepositoryTest extends TestCase
{
    use MakeentitiesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var entitiesRepository
     */
    protected $entitiesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->entitiesRepo = App::make(entitiesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateentities()
    {
        $entities = $this->fakeentitiesData();
        $createdentities = $this->entitiesRepo->create($entities);
        $createdentities = $createdentities->toArray();
        $this->assertArrayHasKey('id', $createdentities);
        $this->assertNotNull($createdentities['id'], 'Created entities must have id specified');
        $this->assertNotNull(entities::find($createdentities['id']), 'entities with given id must be in DB');
        $this->assertModelData($entities, $createdentities);
    }

    /**
     * @test read
     */
    public function testReadentities()
    {
        $entities = $this->makeentities();
        $dbentities = $this->entitiesRepo->find($entities->id);
        $dbentities = $dbentities->toArray();
        $this->assertModelData($entities->toArray(), $dbentities);
    }

    /**
     * @test update
     */
    public function testUpdateentities()
    {
        $entities = $this->makeentities();
        $fakeentities = $this->fakeentitiesData();
        $updatedentities = $this->entitiesRepo->update($fakeentities, $entities->id);
        $this->assertModelData($fakeentities, $updatedentities->toArray());
        $dbentities = $this->entitiesRepo->find($entities->id);
        $this->assertModelData($fakeentities, $dbentities->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteentities()
    {
        $entities = $this->makeentities();
        $resp = $this->entitiesRepo->delete($entities->id);
        $this->assertTrue($resp);
        $this->assertNull(entities::find($entities->id), 'entities should not exist in DB');
    }
}
