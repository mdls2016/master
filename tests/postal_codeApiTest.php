<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class postal_codeApiTest extends TestCase
{
    use Makepostal_codeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatepostal_code()
    {
        $postalCode = $this->fakepostal_codeData();
        $this->json('POST', '/api/v1/postalCodes', $postalCode);

        $this->assertApiResponse($postalCode);
    }

    /**
     * @test
     */
    public function testReadpostal_code()
    {
        $postalCode = $this->makepostal_code();
        $this->json('GET', '/api/v1/postalCodes/'.$postalCode->id);

        $this->assertApiResponse($postalCode->toArray());
    }

    /**
     * @test
     */
    public function testUpdatepostal_code()
    {
        $postalCode = $this->makepostal_code();
        $editedpostal_code = $this->fakepostal_codeData();

        $this->json('PUT', '/api/v1/postalCodes/'.$postalCode->id, $editedpostal_code);

        $this->assertApiResponse($editedpostal_code);
    }

    /**
     * @test
     */
    public function testDeletepostal_code()
    {
        $postalCode = $this->makepostal_code();
        $this->json('DELETE', '/api/v1/postalCodes/'.$postalCode->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/postalCodes/'.$postalCode->id);

        $this->assertResponseStatus(404);
    }
}
