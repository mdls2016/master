<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class themesApiTest extends TestCase
{
    use MakethemesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatethemes()
    {
        $themes = $this->fakethemesData();
        $this->json('POST', '/api/v1/themes', $themes);

        $this->assertApiResponse($themes);
    }

    /**
     * @test
     */
    public function testReadthemes()
    {
        $themes = $this->makethemes();
        $this->json('GET', '/api/v1/themes/'.$themes->id);

        $this->assertApiResponse($themes->toArray());
    }

    /**
     * @test
     */
    public function testUpdatethemes()
    {
        $themes = $this->makethemes();
        $editedthemes = $this->fakethemesData();

        $this->json('PUT', '/api/v1/themes/'.$themes->id, $editedthemes);

        $this->assertApiResponse($editedthemes);
    }

    /**
     * @test
     */
    public function testDeletethemes()
    {
        $themes = $this->makethemes();
        $this->json('DELETE', '/api/v1/themes/'.$themes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/themes/'.$themes->id);

        $this->assertResponseStatus(404);
    }
}
