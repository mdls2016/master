<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class districts_coordinatesApiTest extends TestCase
{
    use Makedistricts_coordinatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatedistricts_coordinates()
    {
        $districtsCoordinates = $this->fakedistricts_coordinatesData();
        $this->json('POST', '/api/v1/districtsCoordinates', $districtsCoordinates);

        $this->assertApiResponse($districtsCoordinates);
    }

    /**
     * @test
     */
    public function testReaddistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $this->json('GET', '/api/v1/districtsCoordinates/'.$districtsCoordinates->id);

        $this->assertApiResponse($districtsCoordinates->toArray());
    }

    /**
     * @test
     */
    public function testUpdatedistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $editeddistricts_coordinates = $this->fakedistricts_coordinatesData();

        $this->json('PUT', '/api/v1/districtsCoordinates/'.$districtsCoordinates->id, $editeddistricts_coordinates);

        $this->assertApiResponse($editeddistricts_coordinates);
    }

    /**
     * @test
     */
    public function testDeletedistricts_coordinates()
    {
        $districtsCoordinates = $this->makedistricts_coordinates();
        $this->json('DELETE', '/api/v1/districtsCoordinates/'.$districtsCoordinates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/districtsCoordinates/'.$districtsCoordinates->id);

        $this->assertResponseStatus(404);
    }
}
