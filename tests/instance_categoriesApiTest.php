<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instance_categoriesApiTest extends TestCase
{
    use Makeinstance_categoriesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateinstance_categories()
    {
        $instanceCategories = $this->fakeinstance_categoriesData();
        $this->json('POST', '/api/v1/instanceCategories', $instanceCategories);

        $this->assertApiResponse($instanceCategories);
    }

    /**
     * @test
     */
    public function testReadinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $this->json('GET', '/api/v1/instanceCategories/'.$instanceCategories->id);

        $this->assertApiResponse($instanceCategories->toArray());
    }

    /**
     * @test
     */
    public function testUpdateinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $editedinstance_categories = $this->fakeinstance_categoriesData();

        $this->json('PUT', '/api/v1/instanceCategories/'.$instanceCategories->id, $editedinstance_categories);

        $this->assertApiResponse($editedinstance_categories);
    }

    /**
     * @test
     */
    public function testDeleteinstance_categories()
    {
        $instanceCategories = $this->makeinstance_categories();
        $this->json('DELETE', '/api/v1/instanceCategories/'.$instanceCategories->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/instanceCategories/'.$instanceCategories->id);

        $this->assertResponseStatus(404);
    }
}
