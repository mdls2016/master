<?php

use App\Models\subthemes;
use App\Repositories\subthemesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class subthemesRepositoryTest extends TestCase
{
    use MakesubthemesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var subthemesRepository
     */
    protected $subthemesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->subthemesRepo = App::make(subthemesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatesubthemes()
    {
        $subthemes = $this->fakesubthemesData();
        $createdsubthemes = $this->subthemesRepo->create($subthemes);
        $createdsubthemes = $createdsubthemes->toArray();
        $this->assertArrayHasKey('id', $createdsubthemes);
        $this->assertNotNull($createdsubthemes['id'], 'Created subthemes must have id specified');
        $this->assertNotNull(subthemes::find($createdsubthemes['id']), 'subthemes with given id must be in DB');
        $this->assertModelData($subthemes, $createdsubthemes);
    }

    /**
     * @test read
     */
    public function testReadsubthemes()
    {
        $subthemes = $this->makesubthemes();
        $dbsubthemes = $this->subthemesRepo->find($subthemes->id);
        $dbsubthemes = $dbsubthemes->toArray();
        $this->assertModelData($subthemes->toArray(), $dbsubthemes);
    }

    /**
     * @test update
     */
    public function testUpdatesubthemes()
    {
        $subthemes = $this->makesubthemes();
        $fakesubthemes = $this->fakesubthemesData();
        $updatedsubthemes = $this->subthemesRepo->update($fakesubthemes, $subthemes->id);
        $this->assertModelData($fakesubthemes, $updatedsubthemes->toArray());
        $dbsubthemes = $this->subthemesRepo->find($subthemes->id);
        $this->assertModelData($fakesubthemes, $dbsubthemes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletesubthemes()
    {
        $subthemes = $this->makesubthemes();
        $resp = $this->subthemesRepo->delete($subthemes->id);
        $this->assertTrue($resp);
        $this->assertNull(subthemes::find($subthemes->id), 'subthemes should not exist in DB');
    }
}
