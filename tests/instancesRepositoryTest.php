<?php

use App\Models\instances;
use App\Repositories\instancesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class instancesRepositoryTest extends TestCase
{
    use MakeinstancesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var instancesRepository
     */
    protected $instancesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->instancesRepo = App::make(instancesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateinstances()
    {
        $instances = $this->fakeinstancesData();
        $createdinstances = $this->instancesRepo->create($instances);
        $createdinstances = $createdinstances->toArray();
        $this->assertArrayHasKey('id', $createdinstances);
        $this->assertNotNull($createdinstances['id'], 'Created instances must have id specified');
        $this->assertNotNull(instances::find($createdinstances['id']), 'instances with given id must be in DB');
        $this->assertModelData($instances, $createdinstances);
    }

    /**
     * @test read
     */
    public function testReadinstances()
    {
        $instances = $this->makeinstances();
        $dbinstances = $this->instancesRepo->find($instances->id);
        $dbinstances = $dbinstances->toArray();
        $this->assertModelData($instances->toArray(), $dbinstances);
    }

    /**
     * @test update
     */
    public function testUpdateinstances()
    {
        $instances = $this->makeinstances();
        $fakeinstances = $this->fakeinstancesData();
        $updatedinstances = $this->instancesRepo->update($fakeinstances, $instances->id);
        $this->assertModelData($fakeinstances, $updatedinstances->toArray());
        $dbinstances = $this->instancesRepo->find($instances->id);
        $this->assertModelData($fakeinstances, $dbinstances->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteinstances()
    {
        $instances = $this->makeinstances();
        $resp = $this->instancesRepo->delete($instances->id);
        $this->assertTrue($resp);
        $this->assertNull(instances::find($instances->id), 'instances should not exist in DB');
    }
}
