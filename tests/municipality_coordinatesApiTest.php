<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class municipality_coordinatesApiTest extends TestCase
{
    use Makemunicipality_coordinatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->fakemunicipality_coordinatesData();
        $this->json('POST', '/api/v1/municipalityCoordinates', $municipalityCoordinates);

        $this->assertApiResponse($municipalityCoordinates);
    }

    /**
     * @test
     */
    public function testReadmunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $this->json('GET', '/api/v1/municipalityCoordinates/'.$municipalityCoordinates->id);

        $this->assertApiResponse($municipalityCoordinates->toArray());
    }

    /**
     * @test
     */
    public function testUpdatemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $editedmunicipality_coordinates = $this->fakemunicipality_coordinatesData();

        $this->json('PUT', '/api/v1/municipalityCoordinates/'.$municipalityCoordinates->id, $editedmunicipality_coordinates);

        $this->assertApiResponse($editedmunicipality_coordinates);
    }

    /**
     * @test
     */
    public function testDeletemunicipality_coordinates()
    {
        $municipalityCoordinates = $this->makemunicipality_coordinates();
        $this->json('DELETE', '/api/v1/municipalityCoordinates/'.$municipalityCoordinates->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/municipalityCoordinates/'.$municipalityCoordinates->id);

        $this->assertResponseStatus(404);
    }
}
