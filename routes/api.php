<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('cors')->group(function() {

    Route::resource('circumscriptions', 'circumscriptionsAPIController');

    Route::resource('postal_codes', 'postal_codeAPIController');

    Route::resource('neighborhoods', 'neighborhoodAPIController');

    Route::resource('sections', 'sectionAPIController');

    Route::resource('entities', 'entitiesAPIController');

    Route::resource('districts', 'districtsAPIController');

    Route::resource('local_districts', 'local_districtAPIController');

    Route::resource('municipalities', 'municipalitiesAPIController');

    Route::resource('instances', 'instancesAPIController');

    Route::resource('categories', 'categoriesAPIController');

    Route::resource('subthemes', 'subthemesAPIController');

    Route::resource('themes', 'themesAPIController');

    Route::resource('marginalizations', 'marginalizationsAPIController');

    Route::resource('instancesthemes', 'instances_ThemesAPIController');

    Route::resource('instance_categories', 'instance_categoriesAPIController');

    Route::resource('local_districts', 'local_districtAPIController');

    Route::resource('settlements', 'settlementAPIController');

    Route::resource('entity_coordinates', 'entity_coordinatesAPIController');

    Route::resource('municipality_coordinates', 'municipality_coordinatesAPIController');

});


Route::resource('districts_coordinates', 'districts_coordinatesAPIController');

Route::resource('sections_coordinates', 'sections_coordinatesAPIController');

Route::resource('local_districts_coordinates', 'local_districts_coordinatesAPIController');