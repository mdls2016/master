<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createsections_coordinatesAPIRequest;
use App\Http\Requests\API\Updatesections_coordinatesAPIRequest;
use App\Models\sections_coordinates;
use App\Repositories\sections_coordinatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class sections_coordinatesController
 * @package App\Http\Controllers\API
 */

class sections_coordinatesAPIController extends AppBaseController
{
    /** @var  sections_coordinatesRepository */
    private $sectionsCoordinatesRepository;

    public function __construct(sections_coordinatesRepository $sectionsCoordinatesRepo)
    {
        $this->sectionsCoordinatesRepository = $sectionsCoordinatesRepo;
    }

    /**
     * Display a listing of the sections_coordinates.
     * GET|HEAD /sectionsCoordinates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sectionsCoordinatesRepository->pushCriteria(new RequestCriteria($request));
        $this->sectionsCoordinatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sectionsCoordinates = $this->sectionsCoordinatesRepository->all();

        return $this->sendResponse($sectionsCoordinates->toArray(), 'Sections Coordinates retrieved successfully');
    }

    /**
     * Store a newly created sections_coordinates in storage.
     * POST /sectionsCoordinates
     *
     * @param Createsections_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createsections_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        $sectionsCoordinates = $this->sectionsCoordinatesRepository->create($input);

        return $this->sendResponse($sectionsCoordinates->toArray(), 'Sections Coordinates saved successfully');
    }

    /**
     * Display the specified sections_coordinates.
     * GET|HEAD /sectionsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var sections_coordinates $sectionsCoordinates */
        $sectionsCoordinates = $this->sectionsCoordinatesRepository->findWithoutFail($id);

        if (empty($sectionsCoordinates)) {
            return $this->sendError('Sections Coordinates not found');
        }

        return $this->sendResponse($sectionsCoordinates->toArray(), 'Sections Coordinates retrieved successfully');
    }

    /**
     * Update the specified sections_coordinates in storage.
     * PUT/PATCH /sectionsCoordinates/{id}
     *
     * @param  int $id
     * @param Updatesections_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatesections_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var sections_coordinates $sectionsCoordinates */
        $sectionsCoordinates = $this->sectionsCoordinatesRepository->findWithoutFail($id);

        if (empty($sectionsCoordinates)) {
            return $this->sendError('Sections Coordinates not found');
        }

        $sectionsCoordinates = $this->sectionsCoordinatesRepository->update($input, $id);

        return $this->sendResponse($sectionsCoordinates->toArray(), 'sections_coordinates updated successfully');
    }

    /**
     * Remove the specified sections_coordinates from storage.
     * DELETE /sectionsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var sections_coordinates $sectionsCoordinates */
        $sectionsCoordinates = $this->sectionsCoordinatesRepository->findWithoutFail($id);

        if (empty($sectionsCoordinates)) {
            return $this->sendError('Sections Coordinates not found');
        }

        $sectionsCoordinates->delete();

        return $this->sendResponse($id, 'Sections Coordinates deleted successfully');
    }
}
