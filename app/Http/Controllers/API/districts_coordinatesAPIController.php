<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createdistricts_coordinatesAPIRequest;
use App\Http\Requests\API\Updatedistricts_coordinatesAPIRequest;
use App\Models\districts_coordinates;
use App\Repositories\districts_coordinatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class districts_coordinatesController
 * @package App\Http\Controllers\API
 */

class districts_coordinatesAPIController extends AppBaseController
{
    /** @var  districts_coordinatesRepository */
    private $districtsCoordinatesRepository;

    public function __construct(districts_coordinatesRepository $districtsCoordinatesRepo)
    {
        $this->districtsCoordinatesRepository = $districtsCoordinatesRepo;
    }

    /**
     * Display a listing of the districts_coordinates.
     * GET|HEAD /districtsCoordinates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->districtsCoordinatesRepository->pushCriteria(new RequestCriteria($request));
        $this->districtsCoordinatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $districtsCoordinates = $this->districtsCoordinatesRepository->all();

        return $this->sendResponse($districtsCoordinates->toArray(), 'Districts Coordinates retrieved successfully');
    }

    /**
     * Store a newly created districts_coordinates in storage.
     * POST /districtsCoordinates
     *
     * @param Createdistricts_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createdistricts_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        $districtsCoordinates = $this->districtsCoordinatesRepository->create($input);

        return $this->sendResponse($districtsCoordinates->toArray(), 'Districts Coordinates saved successfully');
    }

    /**
     * Display the specified districts_coordinates.
     * GET|HEAD /districtsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var districts_coordinates $districtsCoordinates */
        $districtsCoordinates = $this->districtsCoordinatesRepository->findWithoutFail($id);

        if (empty($districtsCoordinates)) {
            return $this->sendError('Districts Coordinates not found');
        }

        return $this->sendResponse($districtsCoordinates->toArray(), 'Districts Coordinates retrieved successfully');
    }

    /**
     * Update the specified districts_coordinates in storage.
     * PUT/PATCH /districtsCoordinates/{id}
     *
     * @param  int $id
     * @param Updatedistricts_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedistricts_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var districts_coordinates $districtsCoordinates */
        $districtsCoordinates = $this->districtsCoordinatesRepository->findWithoutFail($id);

        if (empty($districtsCoordinates)) {
            return $this->sendError('Districts Coordinates not found');
        }

        $districtsCoordinates = $this->districtsCoordinatesRepository->update($input, $id);

        return $this->sendResponse($districtsCoordinates->toArray(), 'districts_coordinates updated successfully');
    }

    /**
     * Remove the specified districts_coordinates from storage.
     * DELETE /districtsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var districts_coordinates $districtsCoordinates */
        $districtsCoordinates = $this->districtsCoordinatesRepository->findWithoutFail($id);

        if (empty($districtsCoordinates)) {
            return $this->sendError('Districts Coordinates not found');
        }

        $districtsCoordinates->delete();

        return $this->sendResponse($id, 'Districts Coordinates deleted successfully');
    }
}
