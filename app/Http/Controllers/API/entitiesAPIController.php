<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateentitiesAPIRequest;
use App\Http\Requests\API\UpdateentitiesAPIRequest;
use App\Models\entities;
use App\Repositories\entitiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class entitiesController
 * @package App\Http\Controllers\API
 */

class entitiesAPIController extends AppBaseController
{
    /** @var  entitiesRepository */
    private $entitiesRepository;

    public function __construct(entitiesRepository $entitiesRepo)
    {
        $this->entitiesRepository = $entitiesRepo;
    }

    /**
     * Display a listing of the entities.
     * GET|HEAD /entities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->entitiesRepository->pushCriteria(new RequestCriteria($request));
        $this->entitiesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $entities = $this->entitiesRepository
            ->with('circumscriptions')
            ->with('municipalities')
            ->all();

        return $this->sendResponse($entities->toArray(), 'Entities retrieved successfully');
    }

    /**
     * Store a newly created entities in storage.
     * POST /entities
     *
     * @param CreateentitiesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateentitiesAPIRequest $request)
    {
        $input = $request->all();

        $entities = $this->entitiesRepository->create($input);

        return $this->sendResponse($entities->toArray(), 'Entities saved successfully');
    }

    /**
     * Display the specified entities.
     * GET|HEAD /entities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var entities $entities */
        $entities = $this->entitiesRepository->findWithoutFail($id);

        if (empty($entities)) {
            return $this->sendError('Entities not found');
        }

        return $this->sendResponse($entities->toArray(), 'Entities retrieved successfully');
    }

    /**
     * Update the specified entities in storage.
     * PUT/PATCH /entities/{id}
     *
     * @param  int $id
     * @param UpdateentitiesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateentitiesAPIRequest $request)
    {
        $input = $request->all();
        $model = new entities();
        $model->fill($input);
        /** @var entities $entities */
        $entities = $this->entitiesRepository->findWithoutFail($id);

        if (empty($entities)) {
            return $this->sendError('Entities not found');
        }

        $entities = $this->entitiesRepository->update($model->toArray(), $id);

        return $this->sendResponse($entities->toArray(), 'entities updated successfully');
    }

    /**
     * Remove the specified entities from storage.
     * DELETE /entities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var entities $entities */
        $entities = $this->entitiesRepository->findWithoutFail($id);

        if (empty($entities)) {
            return $this->sendError('Entities not found');
        }

        $entities->delete();

        return $this->sendResponse($id, 'Entities deleted successfully');
    }
}
