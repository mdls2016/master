<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatesettlementAPIRequest;
use App\Http\Requests\API\UpdatesettlementAPIRequest;
use App\Models\settlements;
use App\Repositories\settlementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class settlementController
 * @package App\Http\Controllers\API
 */

class settlementAPIController extends AppBaseController
{
    /** @var  settlementRepository */
    private $settlementRepository;

    public function __construct(settlementRepository $settlementRepo)
    {
        $this->settlementRepository = $settlementRepo;
    }

    /**
     * Display a listing of the settlements.
     * GET|HEAD /settlements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->settlementRepository->pushCriteria(new RequestCriteria($request));
        $this->settlementRepository->pushCriteria(new LimitOffsetCriteria($request));
        $settlements = $this->settlementRepository->all();

        return $this->sendResponse($settlements->toArray(), 'Settlements retrieved successfully');
    }

    /**
     * Store a newly created settlements in storage.
     * POST /settlements
     *
     * @param CreatesettlementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatesettlementAPIRequest $request)
    {
        $input = $request->all();

        $settlements = $this->settlementRepository->create($input);

        return $this->sendResponse($settlements->toArray(), 'Settlement saved successfully');
    }

    /**
     * Display the specified settlements.
     * GET|HEAD /settlements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var settlements $settlement */
        $settlement = $this->settlementRepository->findWithoutFail($id);

        if (empty($settlement)) {
            return $this->sendError('Settlement not found');
        }

        return $this->sendResponse($settlement->toArray(), 'Settlement retrieved successfully');
    }

    /**
     * Update the specified settlements in storage.
     * PUT/PATCH /settlements/{id}
     *
     * @param  int $id
     * @param UpdatesettlementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesettlementAPIRequest $request)
    {
        $input = $request->all();
        $model = new settlements();
        $model->fill($input);
        /** @var settlements $settlement */
        $settlement = $this->settlementRepository->findWithoutFail($id);

        if (empty($settlement)) {
            return $this->sendError('Settlement not found');
        }

        $settlement = $this->settlementRepository->update($model->toArray(), $id);

        return $this->sendResponse($settlement->toArray(), 'settlements updated successfully');
    }

    /**
     * Remove the specified settlements from storage.
     * DELETE /settlements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var settlements $settlement */
        $settlement = $this->settlementRepository->findWithoutFail($id);

        if (empty($settlement)) {
            return $this->sendError('Settlement not found');
        }

        $settlement->delete();

        return $this->sendResponse($id, 'Settlement deleted successfully');
    }
}
