<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateneighborhoodAPIRequest;
use App\Http\Requests\API\UpdateneighborhoodAPIRequest;
use App\Models\neighborhoods;
use App\Repositories\neighborhoodRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class neighborhoodController
 * @package App\Http\Controllers\API
 */

class neighborhoodAPIController extends AppBaseController
{
    /** @var  neighborhoodRepository */
    private $neighborhoodRepository;

    public function __construct(neighborhoodRepository $neighborhoodRepo)
    {
        $this->neighborhoodRepository = $neighborhoodRepo;
    }

    /**
     * Display a listing of the neighborhoods.
     * GET|HEAD /neighborhoods
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->neighborhoodRepository->pushCriteria(new RequestCriteria($request));
        $this->neighborhoodRepository->pushCriteria(new LimitOffsetCriteria($request));
        $neighborhoods = $this->neighborhoodRepository
            ->with('municipalities')
            ->with('postal_codes')
            ->with('settlements')
            ->paginate(500);

        return $this->sendResponse($neighborhoods->toArray(), 'Neighborhoods retrieved successfully');
    }

    /**
     * Store a newly created neighborhoods in storage.
     * POST /neighborhoods
     *
     * @param CreateneighborhoodAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateneighborhoodAPIRequest $request)
    {
        $input = $request->all();

        $neighborhoods = $this->neighborhoodRepository->create($input);

        return $this->sendResponse($neighborhoods->toArray(), 'Neighborhood saved successfully');
    }

    /**
     * Display the specified neighborhoods.
     * GET|HEAD /neighborhoods/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var neighborhoods $neighborhood */
        $neighborhood = $this->neighborhoodRepository->findWithoutFail($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        return $this->sendResponse($neighborhood->toArray(), 'Neighborhood retrieved successfully');
    }

    /**
     * Update the specified neighborhoods in storage.
     * PUT/PATCH /neighborhoods/{id}
     *
     * @param  int $id
     * @param UpdateneighborhoodAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateneighborhoodAPIRequest $request)
    {
        $input = $request->all();
        $model = new neighborhoods();
        $model->fill($input);
        /** @var neighborhoods $neighborhood */
        $neighborhood = $this->neighborhoodRepository->findWithoutFail($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        $neighborhood = $this->neighborhoodRepository->update($model->toArray(), $id);

        return $this->sendResponse($neighborhood->toArray(), 'neighborhoods updated successfully');
    }

    /**
     * Remove the specified neighborhoods from storage.
     * DELETE /neighborhoods/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var neighborhoods $neighborhood */
        $neighborhood = $this->neighborhoodRepository->findWithoutFail($id);

        if (empty($neighborhood)) {
            return $this->sendError('Neighborhood not found');
        }

        $neighborhood->delete();

        return $this->sendResponse($id, 'Neighborhood deleted successfully');
    }
}
