<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createinstance_categoriesAPIRequest;
use App\Http\Requests\API\Updateinstance_categoriesAPIRequest;
use App\Models\instance_categories;
use App\Repositories\instance_categoriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class instance_categoriesController
 * @package App\Http\Controllers\API
 */

class instance_categoriesAPIController extends AppBaseController
{
    /** @var  instance_categoriesRepository */
    private $instanceCategoriesRepository;

    public function __construct(instance_categoriesRepository $instanceCategoriesRepo)
    {
        $this->instanceCategoriesRepository = $instanceCategoriesRepo;
    }

    /**
     * Display a listing of the instance_categories.
     * GET|HEAD /instanceCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->instanceCategoriesRepository->pushCriteria(new RequestCriteria($request));
        $this->instanceCategoriesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $instanceCategories = $this->instanceCategoriesRepository->all();

        return $this->sendResponse($instanceCategories->toArray(), 'Instance Categories retrieved successfully');
    }

    /**
     * Store a newly created instance_categories in storage.
     * POST /instanceCategories
     *
     * @param Createinstance_categoriesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createinstance_categoriesAPIRequest $request)
    {
        $input = $request->all();

        $instanceCategories = $this->instanceCategoriesRepository->create($input);

        return $this->sendResponse($instanceCategories->toArray(), 'Instance Categories saved successfully');
    }

    /**
     * Display the specified instance_categories.
     * GET|HEAD /instanceCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var instance_categories $instanceCategories */
        $instanceCategories = $this->instanceCategoriesRepository->findWithoutFail($id);

        if (empty($instanceCategories)) {
            return $this->sendError('Instance Categories not found');
        }

        return $this->sendResponse($instanceCategories->toArray(), 'Instance Categories retrieved successfully');
    }

    /**
     * Update the specified instance_categories in storage.
     * PUT/PATCH /instanceCategories/{id}
     *
     * @param  int $id
     * @param Updateinstance_categoriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateinstance_categoriesAPIRequest $request)
    {
        $input = $request->all();
        $model = new instance_categories();
        $model->fill($input);
        /** @var instance_categories $instanceCategories */
        $instanceCategories = $this->instanceCategoriesRepository->findWithoutFail($id);

        if (empty($instanceCategories)) {
            return $this->sendError('Instance Categories not found');
        }

        $instanceCategories = $this->instanceCategoriesRepository->update($model->toArray(), $id);

        return $this->sendResponse($instanceCategories->toArray(), 'instance_categories updated successfully');
    }

    /**
     * Remove the specified instance_categories from storage.
     * DELETE /instanceCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var instance_categories $instanceCategories */
        $instanceCategories = $this->instanceCategoriesRepository->findWithoutFail($id);

        if (empty($instanceCategories)) {
            return $this->sendError('Instance Categories not found');
        }

        $instanceCategories->delete();

        return $this->sendResponse($id, 'Instance Categories deleted successfully');
    }
}
