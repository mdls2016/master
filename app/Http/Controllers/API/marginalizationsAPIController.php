<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatemarginalizationsAPIRequest;
use App\Http\Requests\API\UpdatemarginalizationsAPIRequest;
use App\Models\marginalizations;
use App\Repositories\marginalizationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class marginalizationsController
 * @package App\Http\Controllers\API
 */

class marginalizationsAPIController extends AppBaseController
{
    /** @var  marginalizationsRepository */
    private $marginalizationsRepository;

    public function __construct(marginalizationsRepository $marginalizationsRepo)
    {
        $this->marginalizationsRepository = $marginalizationsRepo;
    }

    /**
     * Display a listing of the marginalizations.
     * GET|HEAD /marginalizations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->marginalizationsRepository->pushCriteria(new RequestCriteria($request));
        $this->marginalizationsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $marginalizations = $this->marginalizationsRepository->all();

        return $this->sendResponse($marginalizations->toArray(), 'Marginalizations retrieved successfully');
    }

    /**
     * Store a newly created marginalizations in storage.
     * POST /marginalizations
     *
     * @param CreatemarginalizationsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatemarginalizationsAPIRequest $request)
    {
        $input = $request->all();

        $marginalizations = $this->marginalizationsRepository->create($input);

        return $this->sendResponse($marginalizations->toArray(), 'Marginalizations saved successfully');
    }

    /**
     * Display the specified marginalizations.
     * GET|HEAD /marginalizations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var marginalizations $marginalizations */
        $marginalizations = $this->marginalizationsRepository->findWithoutFail($id);

        if (empty($marginalizations)) {
            return $this->sendError('Marginalizations not found');
        }

        return $this->sendResponse($marginalizations->toArray(), 'Marginalizations retrieved successfully');
    }

    /**
     * Update the specified marginalizations in storage.
     * PUT/PATCH /marginalizations/{id}
     *
     * @param  int $id
     * @param UpdatemarginalizationsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemarginalizationsAPIRequest $request)
    {
        $input = $request->all();
        $model = new marginalizations();
        $model->fill($input);
        /** @var marginalizations $marginalizations */
        $marginalizations = $this->marginalizationsRepository->findWithoutFail($id);

        if (empty($marginalizations)) {
            return $this->sendError('Marginalizations not found');
        }

        $marginalizations = $this->marginalizationsRepository->update($model->toArray(), $id);

        return $this->sendResponse($marginalizations->toArray(), 'marginalizations updated successfully');
    }

    /**
     * Remove the specified marginalizations from storage.
     * DELETE /marginalizations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var marginalizations $marginalizations */
        $marginalizations = $this->marginalizationsRepository->findWithoutFail($id);

        if (empty($marginalizations)) {
            return $this->sendError('Marginalizations not found');
        }

        $marginalizations->delete();

        return $this->sendResponse($id, 'Marginalizations deleted successfully');
    }
}
