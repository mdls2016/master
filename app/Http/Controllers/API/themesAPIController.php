<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatethemesAPIRequest;
use App\Http\Requests\API\UpdatethemesAPIRequest;
use App\Models\themes;
use App\Repositories\themesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class themesController
 * @package App\Http\Controllers\API
 */

class themesAPIController extends AppBaseController
{
    /** @var  themesRepository */
    private $themesRepository;

    public function __construct(themesRepository $themesRepo)
    {
        $this->themesRepository = $themesRepo;
    }

    /**
     * Display a listing of the themes.
     * GET|HEAD /themes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->themesRepository->pushCriteria(new RequestCriteria($request));
        $this->themesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $themes = $this->themesRepository->with('subthemes')->all();

        return $this->sendResponse($themes->toArray(), 'Themes retrieved successfully');
    }

    /**
     * Store a newly created themes in storage.
     * POST /themes
     *
     * @param CreatethemesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatethemesAPIRequest $request)
    {
        $input = $request->all();

        $themes = $this->themesRepository->create($input);

        return $this->sendResponse($themes->toArray(), 'Themes saved successfully');
    }

    /**
     * Display the specified themes.
     * GET|HEAD /themes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var themes $themes */
        $themes = $this->themesRepository->findWithoutFail($id);

        if (empty($themes)) {
            return $this->sendError('Themes not found');
        }

        return $this->sendResponse($themes->toArray(), 'Themes retrieved successfully');
    }

    /**
     * Update the specified themes in storage.
     * PUT/PATCH /themes/{id}
     *
     * @param  int $id
     * @param UpdatethemesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatethemesAPIRequest $request)
    {
        $input = $request->all();
        $model = new themes();
        $model->fill($input);
        /** @var themes $themes */
        $themes = $this->themesRepository->findWithoutFail($id);

        if (empty($themes)) {
            return $this->sendError('Themes not found');
        }

        $themes = $this->themesRepository->update($model->toArray(), $id);

        return $this->sendResponse($themes->toArray(), 'themes updated successfully');
    }

    /**
     * Remove the specified themes from storage.
     * DELETE /themes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var themes $themes */
        $themes = $this->themesRepository->findWithoutFail($id);

        if (empty($themes)) {
            return $this->sendError('Themes not found');
        }

        $themes->delete();

        return $this->sendResponse($id, 'Themes deleted successfully');
    }
}
