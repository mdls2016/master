<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateinstancesAPIRequest;
use App\Http\Requests\API\UpdateinstancesAPIRequest;
use App\Models\instances;
use App\Repositories\instancesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class instancesController
 * @package App\Http\Controllers\API
 */

class instancesAPIController extends AppBaseController
{
    /** @var  instancesRepository */
    private $instancesRepository;

    public function __construct(instancesRepository $instancesRepo)
    {
        $this->instancesRepository = $instancesRepo;
    }

    /**
     * Display a listing of the instances.
     * GET|HEAD /instances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
          $this->instancesRepository->pushCriteria(new RequestCriteria($request));
        $this->instancesRepository->pushCriteria(new LimitOffsetCriteria($request));

        $instances = $this->instancesRepository->all();

        return $this->sendResponse($instances->toArray(), 'Instances retrieved successfully');
    }

    /**
     * Store a newly created instances in storage.
     * POST /instances
     *
     * @param CreateinstancesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateinstancesAPIRequest $request)
    {
        $input = $request->all();

        $instances = $this->instancesRepository->create($input);

        return $this->sendResponse($instances->toArray(), 'Instances saved successfully');
    }

    /**
     * Display the specified instances.
     * GET|HEAD /instances/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var instances $instances */
        $instances = $this->instancesRepository
            ->with('entities')
            ->with('municipalities')
            ->findWithoutFail($id);

        if (empty($instances)) {
            return $this->sendError('Instances not found');
        }

        return $this->sendResponse($instances->toArray(), 'Instances retrieved successfully');
    }

    /**
     * Update the specified instances in storage.
     * PUT/PATCH /instances/{id}
     *
     * @param  int $id
     * @param UpdateinstancesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateinstancesAPIRequest $request)
    {
        $input = $request->all();
        $model = new instances();
        $model->fill($input);
        /** @var instances $instances */
        $instances = $this->instancesRepository->findWithoutFail($id);

        if (empty($instances)) {
            return $this->sendError('Instances not found');
        }

        $instances = $this->instancesRepository->update($model->toArray(), $id);

        return $this->sendResponse($instances->toArray(), 'instances updated successfully');
    }

    /**
     * Remove the specified instances from storage.
     * DELETE /instances/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var instances $instances */
        $instances = $this->instancesRepository->findWithoutFail($id);

        if (empty($instances)) {
            return $this->sendError('Instances not found');
        }

        $instances->delete();

        return $this->sendResponse($id, 'Instances deleted successfully');
    }
}
