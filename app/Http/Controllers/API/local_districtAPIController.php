<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createlocal_districtAPIRequest;
use App\Http\Requests\API\Updatelocal_districtAPIRequest;
use App\Models\local_district;
use App\Repositories\local_districtRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class local_districtController
 * @package App\Http\Controllers\API
 */

class local_districtAPIController extends AppBaseController
{
    /** @var  local_districtRepository */
    private $localDistrictRepository;

    public function __construct(local_districtRepository $localDistrictRepo)
    {
        $this->localDistrictRepository = $localDistrictRepo;
    }

    /**
     * Display a listing of the local_district.
     * GET|HEAD /localDistricts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->localDistrictRepository->pushCriteria(new RequestCriteria($request));
        $this->localDistrictRepository->pushCriteria(new LimitOffsetCriteria($request));
        $localDistricts = $this->localDistrictRepository->all();

        return $this->sendResponse($localDistricts->toArray(), 'Local Districts retrieved successfully');
    }

    /**
     * Store a newly created local_district in storage.
     * POST /localDistricts
     *
     * @param Createlocal_districtAPIRequest $request
     *
     * @return Response
     */
    public function store(Createlocal_districtAPIRequest $request)
    {
        $input = $request->all();

        $localDistricts = $this->localDistrictRepository->create($input);

        return $this->sendResponse($localDistricts->toArray(), 'Local District saved successfully');
    }

    /**
     * Display the specified local_district.
     * GET|HEAD /localDistricts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var local_district $localDistrict */
        $localDistrict = $this->localDistrictRepository->findWithoutFail($id);

        if (empty($localDistrict)) {
            return $this->sendError('Local District not found');
        }

        return $this->sendResponse($localDistrict->toArray(), 'Local District retrieved successfully');
    }

    /**
     * Update the specified local_district in storage.
     * PUT/PATCH /localDistricts/{id}
     *
     * @param  int $id
     * @param Updatelocal_districtAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatelocal_districtAPIRequest $request)
    {
        $input = $request->all();

        /** @var local_district $localDistrict */
        $localDistrict = $this->localDistrictRepository->findWithoutFail($id);

        if (empty($localDistrict)) {
            return $this->sendError('Local District not found');
        }

        $localDistrict = $this->localDistrictRepository->update($input, $id);

        return $this->sendResponse($localDistrict->toArray(), 'local_district updated successfully');
    }

    /**
     * Remove the specified local_district from storage.
     * DELETE /localDistricts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var local_district $localDistrict */
        $localDistrict = $this->localDistrictRepository->findWithoutFail($id);

        if (empty($localDistrict)) {
            return $this->sendError('Local District not found');
        }

        $localDistrict->delete();

        return $this->sendResponse($id, 'Local District deleted successfully');
    }
}
