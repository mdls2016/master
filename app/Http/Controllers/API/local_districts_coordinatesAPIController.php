<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createlocal_districts_coordinatesAPIRequest;
use App\Http\Requests\API\Updatelocal_districts_coordinatesAPIRequest;
use App\Models\local_districts_coordinates;
use App\Repositories\local_districts_coordinatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class local_districts_coordinatesController
 * @package App\Http\Controllers\API
 */

class local_districts_coordinatesAPIController extends AppBaseController
{
    /** @var  local_districts_coordinatesRepository */
    private $localDistrictsCoordinatesRepository;

    public function __construct(local_districts_coordinatesRepository $localDistrictsCoordinatesRepo)
    {
        $this->localDistrictsCoordinatesRepository = $localDistrictsCoordinatesRepo;
    }

    /**
     * Display a listing of the local_districts_coordinates.
     * GET|HEAD /localDistrictsCoordinates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->localDistrictsCoordinatesRepository->pushCriteria(new RequestCriteria($request));
        $this->localDistrictsCoordinatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->all();

        return $this->sendResponse($localDistrictsCoordinates->toArray(), 'Local Districts Coordinates retrieved successfully');
    }

    /**
     * Store a newly created local_districts_coordinates in storage.
     * POST /localDistrictsCoordinates
     *
     * @param Createlocal_districts_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createlocal_districts_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->create($input);

        return $this->sendResponse($localDistrictsCoordinates->toArray(), 'Local Districts Coordinates saved successfully');
    }

    /**
     * Display the specified local_districts_coordinates.
     * GET|HEAD /localDistrictsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var local_districts_coordinates $localDistrictsCoordinates */
        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->findWithoutFail($id);

        if (empty($localDistrictsCoordinates)) {
            return $this->sendError('Local Districts Coordinates not found');
        }

        return $this->sendResponse($localDistrictsCoordinates->toArray(), 'Local Districts Coordinates retrieved successfully');
    }

    /**
     * Update the specified local_districts_coordinates in storage.
     * PUT/PATCH /localDistrictsCoordinates/{id}
     *
     * @param  int $id
     * @param Updatelocal_districts_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatelocal_districts_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var local_districts_coordinates $localDistrictsCoordinates */
        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->findWithoutFail($id);

        if (empty($localDistrictsCoordinates)) {
            return $this->sendError('Local Districts Coordinates not found');
        }

        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->update($input, $id);

        return $this->sendResponse($localDistrictsCoordinates->toArray(), 'local_districts_coordinates updated successfully');
    }

    /**
     * Remove the specified local_districts_coordinates from storage.
     * DELETE /localDistrictsCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var local_districts_coordinates $localDistrictsCoordinates */
        $localDistrictsCoordinates = $this->localDistrictsCoordinatesRepository->findWithoutFail($id);

        if (empty($localDistrictsCoordinates)) {
            return $this->sendError('Local Districts Coordinates not found');
        }

        $localDistrictsCoordinates->delete();

        return $this->sendResponse($id, 'Local Districts Coordinates deleted successfully');
    }
}
