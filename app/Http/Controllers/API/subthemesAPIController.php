<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatesubthemesAPIRequest;
use App\Http\Requests\API\UpdatesubthemesAPIRequest;
use App\Models\subthemes;
use App\Repositories\subthemesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class subthemesController
 * @package App\Http\Controllers\API
 */

class subthemesAPIController extends AppBaseController
{
    /** @var  subthemesRepository */
    private $subthemesRepository;

    public function __construct(subthemesRepository $subthemesRepo)
    {
        $this->subthemesRepository = $subthemesRepo;
    }

    /**
     * Display a listing of the subthemes.
     * GET|HEAD /subthemes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->subthemesRepository->pushCriteria(new RequestCriteria($request));
        $this->subthemesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $subthemes = $this->subthemesRepository
           // ->with('themes')
           // ->with('categories')
            ->all();

        return $this->sendResponse($subthemes->toArray(), 'Subthemes retrieved successfully');
    }

    /**
     * Store a newly created subthemes in storage.
     * POST /subthemes
     *
     * @param CreatesubthemesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatesubthemesAPIRequest $request)
    {
        $input = $request->all();

        $subthemes = $this->subthemesRepository->create($input);

        return $this->sendResponse($subthemes->toArray(), 'Subthemes saved successfully');
    }

    /**
     * Display the specified subthemes.
     * GET|HEAD /subthemes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var subthemes $subthemes */
        $subthemes = $this->subthemesRepository
            ->with('themes')
            ->with('categories')
            ->findWithoutFail($id);

        if (empty($subthemes)) {
            return $this->sendError('Subthemes not found');
        }

        return $this->sendResponse($subthemes->toArray(), 'Subthemes retrieved successfully');
    }

    /**
     * Update the specified subthemes in storage.
     * PUT/PATCH /subthemes/{id}
     *
     * @param  int $id
     * @param UpdatesubthemesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesubthemesAPIRequest $request)
    {
        $input = $request->all();
        $model = new subthemes();
        $model->fill($input);
        /** @var subthemes $subthemes */
        $subthemes = $this->subthemesRepository->findWithoutFail($id);

        if (empty($subthemes)) {
            return $this->sendError('Subthemes not found');
        }

        $subthemes = $this->subthemesRepository->update($model->toArray(), $id);

        return $this->sendResponse($subthemes->toArray(), 'subthemes updated successfully');
    }

    /**
     * Remove the specified subthemes from storage.
     * DELETE /subthemes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var subthemes $subthemes */
        $subthemes = $this->subthemesRepository->findWithoutFail($id);

        if (empty($subthemes)) {
            return $this->sendError('Subthemes not found');
        }

        $subthemes->delete();

        return $this->sendResponse($id, 'Subthemes deleted successfully');
    }
}
