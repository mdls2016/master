<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createpostal_codeAPIRequest;
use App\Http\Requests\API\Updatepostal_codeAPIRequest;
use App\Models\postal_codes;
use App\Repositories\postal_codeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class postal_codeController
 * @package App\Http\Controllers\API
 */

class postal_codeAPIController extends AppBaseController
{
    /** @var  postal_codeRepository */
    private $postalCodeRepository;

    public function __construct(postal_codeRepository $postalCodeRepo)
    {
        $this->postalCodeRepository = $postalCodeRepo;
    }

    /**
     * Display a listing of the postal_codes.
     * GET|HEAD /postalCodes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->postalCodeRepository->pushCriteria(new RequestCriteria($request));
        $this->postalCodeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $postalCodes = $this->postalCodeRepository->all();

        return $this->sendResponse($postalCodes->toArray(), 'Postal Codes retrieved successfully');
    }

    /**
     * Store a newly created postal_codes in storage.
     * POST /postalCodes
     *
     * @param Createpostal_codeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createpostal_codeAPIRequest $request)
    {
        $input = $request->all();

        $postalCodes = $this->postalCodeRepository->create($input);

        return $this->sendResponse($postalCodes->toArray(), 'Postal Code saved successfully');
    }

    /**
     * Display the specified postal_codes.
     * GET|HEAD /postalCodes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var postal_codes $postalCode */
        $postalCode = $this->postalCodeRepository->findWithoutFail($id);

        if (empty($postalCode)) {
            return $this->sendError('Postal Code not found');
        }

        return $this->sendResponse($postalCode->toArray(), 'Postal Code retrieved successfully');
    }

    /**
     * Update the specified postal_codes in storage.
     * PUT/PATCH /postalCodes/{id}
     *
     * @param  int $id
     * @param Updatepostal_codeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatepostal_codeAPIRequest $request)
    {
        $input = $request->all();
        $model = new postal_codes();
        $model->fill($input);
        /** @var postal_codes $postalCode */
        $postalCode = $this->postalCodeRepository->findWithoutFail($id);

        if (empty($postalCode)) {
            return $this->sendError('Postal Code not found');
        }

        $postalCode = $this->postalCodeRepository->update($model->toArray(), $id);

        return $this->sendResponse($postalCode->toArray(), 'postal_codes updated successfully');
    }

    /**
     * Remove the specified postal_codes from storage.
     * DELETE /postalCodes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var postal_codes $postalCode */
        $postalCode = $this->postalCodeRepository->findWithoutFail($id);

        if (empty($postalCode)) {
            return $this->sendError('Postal Code not found');
        }

        $postalCode->delete();

        return $this->sendResponse($id, 'Postal Code deleted successfully');
    }
}
