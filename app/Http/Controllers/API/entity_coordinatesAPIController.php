<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createentity_coordinatesAPIRequest;
use App\Http\Requests\API\Updateentity_coordinatesAPIRequest;
use App\Models\entity_coordinates;
use App\Repositories\entity_coordinatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class entity_coordinatesController
 * @package App\Http\Controllers\API
 */

class entity_coordinatesAPIController extends AppBaseController
{
    /** @var  entity_coordinatesRepository */
    private $entityCoordinatesRepository;

    public function __construct(entity_coordinatesRepository $entityCoordinatesRepo)
    {
        $this->entityCoordinatesRepository = $entityCoordinatesRepo;
    }

    /**
     * Display a listing of the entity_coordinates.
     * GET|HEAD /entityCoordinates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->entityCoordinatesRepository->pushCriteria(new RequestCriteria($request));
        $this->entityCoordinatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $entityCoordinates = $this->entityCoordinatesRepository->all();

        return $this->sendResponse($entityCoordinates->toArray(), 'Entity Coordinates retrieved successfully');
    }

    /**
     * Store a newly created entity_coordinates in storage.
     * POST /entityCoordinates
     *
     * @param Createentity_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createentity_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        $entityCoordinates = $this->entityCoordinatesRepository->create($input);

        return $this->sendResponse($entityCoordinates->toArray(), 'Entity Coordinates saved successfully');
    }

    /**
     * Display the specified entity_coordinates.
     * GET|HEAD /entityCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var entity_coordinates $entityCoordinates */
        $entityCoordinates = $this->entityCoordinatesRepository->findWithoutFail($id);

        if (empty($entityCoordinates)) {
            return $this->sendError('Entity Coordinates not found');
        }

        return $this->sendResponse($entityCoordinates->toArray(), 'Entity Coordinates retrieved successfully');
    }

    /**
     * Update the specified entity_coordinates in storage.
     * PUT/PATCH /entityCoordinates/{id}
     *
     * @param  int $id
     * @param Updateentity_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateentity_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var entity_coordinates $entityCoordinates */
        $entityCoordinates = $this->entityCoordinatesRepository->findWithoutFail($id);

        if (empty($entityCoordinates)) {
            return $this->sendError('Entity Coordinates not found');
        }

        $entityCoordinates = $this->entityCoordinatesRepository->update($input, $id);

        return $this->sendResponse($entityCoordinates->toArray(), 'entity_coordinates updated successfully');
    }

    /**
     * Remove the specified entity_coordinates from storage.
     * DELETE /entityCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var entity_coordinates $entityCoordinates */
        $entityCoordinates = $this->entityCoordinatesRepository->findWithoutFail($id);

        if (empty($entityCoordinates)) {
            return $this->sendError('Entity Coordinates not found');
        }

        $entityCoordinates->delete();

        return $this->sendResponse($id, 'Entity Coordinates deleted successfully');
    }
}
