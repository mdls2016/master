<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecircumscriptionsAPIRequest;
use App\Http\Requests\API\UpdatecircumscriptionsAPIRequest;
use App\Models\circumscriptions;
use App\Repositories\circumscriptionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class circumscriptionsController
 * @package App\Http\Controllers\API
 */

class circumscriptionsAPIController extends AppBaseController
{
    /** @var  circumscriptionsRepository */
    private $circumscriptionsRepository;

    public function __construct(circumscriptionsRepository $circumscriptionsRepo)
    {
        $this->circumscriptionsRepository = $circumscriptionsRepo;
    }

    /**
     * Display a listing of the circumscriptions.
     * GET|HEAD /circumscriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->circumscriptionsRepository->pushCriteria(new RequestCriteria($request));
        $this->circumscriptionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $circumscriptions = $this->circumscriptionsRepository->with('entities')->all();

        return $this->sendResponse($circumscriptions->toArray(), 'Circumscriptions retrieved successfully');
    }

    /**
     * Store a newly created circumscriptions in storage.
     * POST /circumscriptions
     *
     * @param CreatecircumscriptionsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatecircumscriptionsAPIRequest $request)
    {
        $input = $request->all();

        $circumscriptions = $this->circumscriptionsRepository->create($input);

        return $this->sendResponse($circumscriptions->toArray(), 'Circumscriptions saved successfully');
    }

    /**
     * Display the specified circumscriptions.
     * GET|HEAD /circumscriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var circumscriptions $circumscriptions */
        $circumscriptions = $this->circumscriptionsRepository->findWithoutFail($id);

        if (empty($circumscriptions)) {
            return $this->sendError('Circumscriptions not found');
        }

        return $this->sendResponse($circumscriptions->toArray(), 'Circumscriptions retrieved successfully');
    }

    /**
     * Update the specified circumscriptions in storage.
     * PUT/PATCH /circumscriptions/{id}
     *
     * @param  int $id
     * @param UpdatecircumscriptionsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecircumscriptionsAPIRequest $request)
    {
        $input = $request->all();
        $model = new circumscriptions();
        $model->fill($input);
        /** @var circumscriptions $circumscriptions */
        $circumscriptions = $this->circumscriptionsRepository->findWithoutFail($id);

        if (empty($circumscriptions)) {
            return $this->sendError('Circumscriptions not found');
        }

        $circumscriptions = $this->circumscriptionsRepository->update($model->toArray(), $id);

        return $this->sendResponse($circumscriptions->toArray(), 'circumscriptions updated successfully');
    }

    /**
     * Remove the specified circumscriptions from storage.
     * DELETE /circumscriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var circumscriptions $circumscriptions */
        $circumscriptions = $this->circumscriptionsRepository->findWithoutFail($id);

        if (empty($circumscriptions)) {
            return $this->sendError('Circumscriptions not found');
        }

        $circumscriptions->delete();

        return $this->sendResponse($id, 'Circumscriptions deleted successfully');
    }
}
