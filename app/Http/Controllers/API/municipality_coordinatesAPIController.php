<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createmunicipality_coordinatesAPIRequest;
use App\Http\Requests\API\Updatemunicipality_coordinatesAPIRequest;
use App\Models\municipality_coordinates;
use App\Repositories\municipality_coordinatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class municipality_coordinatesController
 * @package App\Http\Controllers\API
 */

class municipality_coordinatesAPIController extends AppBaseController
{
    /** @var  municipality_coordinatesRepository */
    private $municipalityCoordinatesRepository;

    public function __construct(municipality_coordinatesRepository $municipalityCoordinatesRepo)
    {
        $this->municipalityCoordinatesRepository = $municipalityCoordinatesRepo;
    }

    /**
     * Display a listing of the municipality_coordinates.
     * GET|HEAD /municipalityCoordinates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->municipalityCoordinatesRepository->pushCriteria(new RequestCriteria($request));
        $this->municipalityCoordinatesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $municipalityCoordinates = $this->municipalityCoordinatesRepository->all();

        return $this->sendResponse($municipalityCoordinates->toArray(), 'Municipality Coordinates retrieved successfully');
    }

    /**
     * Store a newly created municipality_coordinates in storage.
     * POST /municipalityCoordinates
     *
     * @param Createmunicipality_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function store(Createmunicipality_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        $municipalityCoordinates = $this->municipalityCoordinatesRepository->create($input);

        return $this->sendResponse($municipalityCoordinates->toArray(), 'Municipality Coordinates saved successfully');
    }

    /**
     * Display the specified municipality_coordinates.
     * GET|HEAD /municipalityCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var municipality_coordinates $municipalityCoordinates */
        $municipalityCoordinates = $this->municipalityCoordinatesRepository->findWithoutFail($id);

        if (empty($municipalityCoordinates)) {
            return $this->sendError('Municipality Coordinates not found');
        }

        return $this->sendResponse($municipalityCoordinates->toArray(), 'Municipality Coordinates retrieved successfully');
    }

    /**
     * Update the specified municipality_coordinates in storage.
     * PUT/PATCH /municipalityCoordinates/{id}
     *
     * @param  int $id
     * @param Updatemunicipality_coordinatesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemunicipality_coordinatesAPIRequest $request)
    {
        $input = $request->all();

        /** @var municipality_coordinates $municipalityCoordinates */
        $municipalityCoordinates = $this->municipalityCoordinatesRepository->findWithoutFail($id);

        if (empty($municipalityCoordinates)) {
            return $this->sendError('Municipality Coordinates not found');
        }

        $municipalityCoordinates = $this->municipalityCoordinatesRepository->update($input, $id);

        return $this->sendResponse($municipalityCoordinates->toArray(), 'municipality_coordinates updated successfully');
    }

    /**
     * Remove the specified municipality_coordinates from storage.
     * DELETE /municipalityCoordinates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var municipality_coordinates $municipalityCoordinates */
        $municipalityCoordinates = $this->municipalityCoordinatesRepository->findWithoutFail($id);

        if (empty($municipalityCoordinates)) {
            return $this->sendError('Municipality Coordinates not found');
        }

        $municipalityCoordinates->delete();

        return $this->sendResponse($id, 'Municipality Coordinates deleted successfully');
    }
}
