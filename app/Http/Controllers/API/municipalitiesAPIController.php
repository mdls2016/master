<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatemunicipalitiesAPIRequest;
use App\Http\Requests\API\UpdatemunicipalitiesAPIRequest;
use App\Models\municipalities;
use App\Repositories\municipalitiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class municipalitiesController
 * @package App\Http\Controllers\API
 */

class municipalitiesAPIController extends AppBaseController
{
    /** @var  municipalitiesRepository */
    private $municipalitiesRepository;

    public function __construct(municipalitiesRepository $municipalitiesRepo)
    {
        $this->municipalitiesRepository = $municipalitiesRepo;
    }

    /**
     * Display a listing of the municipalities.
     * GET|HEAD /municipalities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->municipalitiesRepository->pushCriteria(new RequestCriteria($request));
        $this->municipalitiesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $municipalities = $this->municipalitiesRepository
            ->with('entities')
            ->with('marginalizations')
            // ->with('neighborhoods')
            ->paginate(500);

        return $this->sendResponse($municipalities->toArray(), 'Municipalities retrieved successfully');
    }

    /**
     * Store a newly created municipalities in storage.
     * POST /municipalities
     *
     * @param CreatemunicipalitiesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatemunicipalitiesAPIRequest $request)
    {
        $input = $request->all();

        $municipalities = $this->municipalitiesRepository->create($input);

        return $this->sendResponse($municipalities->toArray(), 'Municipalities saved successfully');
    }

    /**
     * Display the specified municipalities.
     * GET|HEAD /municipalities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var municipalities $municipalities */
        $municipalities = $this->municipalitiesRepository
            ->with('neighborhoods')
            ->findWithoutFail($id);

        if (empty($municipalities)) {
            return $this->sendError('Municipalities not found');
        }

        return $this->sendResponse($municipalities->toArray(), 'Municipalities retrieved successfully');
    }

    /**
     * Update the specified municipalities in storage.
     * PUT/PATCH /municipalities/{id}
     *
     * @param  int $id
     * @param UpdatemunicipalitiesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemunicipalitiesAPIRequest $request)
    {
        $input = $request->all();
        $model = new municipalities();
        $model->fill($input);
        /** @var municipalities $municipalities */
        $municipalities = $this->municipalitiesRepository->findWithoutFail($id);

        if (empty($municipalities)) {
            return $this->sendError('Municipalities not found');
        }

        $municipalities = $this->municipalitiesRepository->update($model->toArray(), $id);

        return $this->sendResponse($municipalities->toArray(), 'municipalities updated successfully');
    }

    /**
     * Remove the specified municipalities from storage.
     * DELETE /municipalities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var municipalities $municipalities */
        $municipalities = $this->municipalitiesRepository->findWithoutFail($id);

        if (empty($municipalities)) {
            return $this->sendError('Municipalities not found');
        }

        $municipalities->delete();

        return $this->sendResponse($id, 'Municipalities deleted successfully');
    }
}
