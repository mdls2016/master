<?php

namespace App\Repositories;

use App\Models\sections;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class sectionRepository
 * @package App\Repositories
 * @version August 15, 2017, 6:59 pm UTC
 *
 * @method sections findWithoutFail($id, $columns = ['*'])
 * @method sections find($id, $columns = ['*'])
 * @method sections first($columns = ['*'])
*/
class sectionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_municipality',
        'identifier'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return sections::class;
    }
}
