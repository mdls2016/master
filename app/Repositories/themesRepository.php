<?php

namespace App\Repositories;

use App\Models\themes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class themesRepository
 * @package App\Repositories
 * @version August 23, 2017, 6:07 pm UTC
 *
 * @method themes findWithoutFail($id, $columns = ['*'])
 * @method themes find($id, $columns = ['*'])
 * @method themes first($columns = ['*'])
*/
class themesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return themes::class;
    }
}
