<?php

namespace App\Repositories;

use App\Models\circumscriptions;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class circumscriptionsRepository
 * @package App\Repositories
 * @version August 15, 2017, 5:34 pm UTC
 *
 * @method circumscriptions findWithoutFail($id, $columns = ['*'])
 * @method circumscriptions find($id, $columns = ['*'])
 * @method circumscriptions first($columns = ['*'])
*/
class circumscriptionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
      'identifier'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return circumscriptions::class;
    }
}
