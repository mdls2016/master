<?php

namespace App\Repositories;

use App\Models\local_districts_coordinates;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class local_districts_coordinatesRepository
 * @package App\Repositories
 * @version March 16, 2018, 6:23 pm UTC
 *
 * @method local_districts_coordinates findWithoutFail($id, $columns = ['*'])
 * @method local_districts_coordinates find($id, $columns = ['*'])
 * @method local_districts_coordinates first($columns = ['*'])
*/
class local_districts_coordinatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'coordinates',
        'identifier'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return local_districts_coordinates::class;
    }
}
