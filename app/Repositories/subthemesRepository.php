<?php

namespace App\Repositories;

use App\Models\subthemes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class subthemesRepository
 * @package App\Repositories
 * @version August 23, 2017, 6:06 pm UTC
 *
 * @method subthemes findWithoutFail($id, $columns = ['*'])
 * @method subthemes find($id, $columns = ['*'])
 * @method subthemes first($columns = ['*'])
*/
class subthemesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'theme_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return subthemes::class;
    }
}
