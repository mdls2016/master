<?php

namespace App\Repositories;

use App\Models\entities;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class entitiesRepository
 * @package App\Repositories
 * @version August 15, 2017, 7:02 pm UTC
 *
 * @method entities findWithoutFail($id, $columns = ['*'])
 * @method entities find($id, $columns = ['*'])
 * @method entities first($columns = ['*'])
*/
class entitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'circumscription_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return entities::class;
    }
}
