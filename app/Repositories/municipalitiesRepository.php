<?php

namespace App\Repositories;

use App\Models\municipalities;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class municipalitiesRepository
     * @package App\Repositories
     * @version August 15, 2017, 7:14 pm UTC
     *
     * @method municipalities findWithoutFail($id, $columns = ['*'])
 * @method municipalities find($id, $columns = ['*'])
 * @method municipalities first($columns = ['*'])
*/
class municipalitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'name',
        'district_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return municipalities::class;
    }
}
