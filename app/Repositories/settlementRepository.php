<?php

namespace App\Repositories;

use App\Models\settlements;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class settlementRepository
 * @package App\Repositories
 * @version September 1, 2017, 6:46 pm UTC
 *
 * @method settlements findWithoutFail($id, $columns = ['*'])
 * @method settlements find($id, $columns = ['*'])
 * @method settlements first($columns = ['*'])
*/
class settlementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return settlements::class;
    }
}
