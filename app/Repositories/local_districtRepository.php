<?php

namespace App\Repositories;

use App\Models\local_district;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class local_districtRepository
 * @package App\Repositories
 * @version January 23, 2018, 7:17 pm UTC
 *
 * @method local_district findWithoutFail($id, $columns = ['*'])
 * @method local_district find($id, $columns = ['*'])
 * @method local_district first($columns = ['*'])
*/
class local_districtRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'identifier'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return local_district::class;
    }
}
