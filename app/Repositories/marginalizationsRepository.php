<?php

namespace App\Repositories;

use App\Models\marginalizations;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class marginalizationsRepository
 * @package App\Repositories
 * @version September 27, 2017, 3:41 pm UTC
 *
 * @method marginalizations findWithoutFail($id, $columns = ['*'])
 * @method marginalizations find($id, $columns = ['*'])
 * @method marginalizations first($columns = ['*'])
*/
class marginalizationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return marginalizations::class;
    }
}
