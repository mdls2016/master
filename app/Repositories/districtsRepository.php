<?php

namespace App\Repositories;

use App\Models\districts;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class districtsRepository
 * @package App\Repositories
 * @version August 15, 2017, 7:05 pm UTC
 *
 * @method districts findWithoutFail($id, $columns = ['*'])
 * @method districts find($id, $columns = ['*'])
 * @method districts first($columns = ['*'])
*/
class districtsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier',
        'entity_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return districts::class;
    }
}
