<?php

namespace App\Repositories;

use App\Models\instances;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class instancesRepository
 * @package App\Repositories
 * @version August 31, 2017, 6:19 pm UTC
 *
 * @method instances findWithoutFail($id, $columns = ['*'])
 * @method instances find($id, $columns = ['*'])
 * @method instances first($columns = ['*'])
*/
class instancesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'participant_id',
        'name',
        'shortname',
        'entity_id',
        'web_page',
        'municipality_id',
        'description',
        'direction',
        'phone',
        'email',
        'neighborhood_id',
        'postal_code_id',
        'functionary_id',
        'name_functionary'

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return instances::class;
    }
}
