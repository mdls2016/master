<?php

namespace App\Repositories;

use App\Models\municipality_coordinates;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class municipality_coordinatesRepository
 * @package App\Repositories
 * @version March 6, 2018, 4:22 am UTC
 *
 * @method municipality_coordinates findWithoutFail($id, $columns = ['*'])
 * @method municipality_coordinates find($id, $columns = ['*'])
 * @method municipality_coordinates first($columns = ['*'])
*/
class municipality_coordinatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'municipality_id',
        'name',
        'coordinates'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return municipality_coordinates::class;
    }
}
