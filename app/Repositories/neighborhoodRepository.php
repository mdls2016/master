<?php

namespace App\Repositories;

use App\Models\neighborhoods;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class neighborhoodRepository
 * @package App\Repositories
 * @version August 15, 2017, 6:27 pm UTC
 *
 * @method neighborhoods findWithoutFail($id, $columns = ['*'])
 * @method neighborhoods find($id, $columns = ['*'])
 * @method neighborhoods first($columns = ['*'])
*/
class neighborhoodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_municipality',
        'name',
        'id_postal_code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return neighborhoods::class;
    }
}
