<?php

namespace App\Repositories;

use App\Models\districts_coordinates;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class districts_coordinatesRepository
 * @package App\Repositories
 * @version March 15, 2018, 6:00 pm UTC
 *
 * @method districts_coordinates findWithoutFail($id, $columns = ['*'])
 * @method districts_coordinates find($id, $columns = ['*'])
 * @method districts_coordinates first($columns = ['*'])
*/
class districts_coordinatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'identifier',
        'coordinates'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return districts_coordinates::class;
    }
}
