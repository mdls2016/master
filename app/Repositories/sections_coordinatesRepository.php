<?php

namespace App\Repositories;

use App\Models\sections_coordinates;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class sections_coordinatesRepository
 * @package App\Repositories
 * @version March 15, 2018, 11:55 pm UTC
 *
 * @method sections_coordinates findWithoutFail($id, $columns = ['*'])
 * @method sections_coordinates find($id, $columns = ['*'])
 * @method sections_coordinates first($columns = ['*'])
*/
class sections_coordinatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coordinates',
        'district_id',
        'section_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return sections_coordinates::class;
    }
}
