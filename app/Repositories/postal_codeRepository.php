<?php

namespace App\Repositories;

use App\Models\postal_codes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class postal_codeRepository
 * @package App\Repositories
 * @version August 15, 2017, 6:19 pm UTC
 *
 * @method postal_codes findWithoutFail($id, $columns = ['*'])
 * @method postal_codes find($id, $columns = ['*'])
 * @method postal_codes first($columns = ['*'])
*/
class postal_codeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return postal_codes::class;
    }
}
