<?php

namespace App\Repositories;

use App\Models\entity_coordinates;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class entity_coordinatesRepository
 * @package App\Repositories
 * @version March 6, 2018, 4:00 am UTC
 *
 * @method entity_coordinates findWithoutFail($id, $columns = ['*'])
 * @method entity_coordinates find($id, $columns = ['*'])
 * @method entity_coordinates first($columns = ['*'])
*/
class entity_coordinatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entity_id',
        'name',
        'coordinates'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return entity_coordinates::class;
    }
}
