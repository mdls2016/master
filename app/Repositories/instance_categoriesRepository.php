<?php

namespace App\Repositories;

use App\Models\instance_categories;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class instance_categoriesRepository
 * @package App\Repositories
 * @version November 2, 2017, 12:00 am UTC
 *
 * @method instance_categories findWithoutFail($id, $columns = ['*'])
 * @method instance_categories find($id, $columns = ['*'])
 * @method instance_categories first($columns = ['*'])
*/

class instance_categoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'instance_id',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return instance_categories::class;
    }
}
