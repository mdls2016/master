<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class local_district
 * @package App\Models
 * @version January 23, 2018, 7:17 pm UTC
 *
 * @property integer entity_id
 * @property string identifier
 */
class local_district extends Model
{
    use SoftDeletes;

    public $table = 'local_districts';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity_id',
        'identifier'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entity_id' => 'integer',
        'identifier' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'entity_id' => 'required',
        'identifier' => 'required'
    ];

    
}
