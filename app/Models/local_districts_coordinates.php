<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class local_districts_coordinates
 * @package App\Models
 * @version March 16, 2018, 6:23 pm UTC
 *
 * @property integer entity_id
 * @property mediumText coordinates
 * @property string identifier
 */
class local_districts_coordinates extends Model
{
    use SoftDeletes;

    public $table = 'local_districts_coordinates';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity_id',
        'coordinates',
        'identifier'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entity_id' => 'integer',
        'identifier' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
