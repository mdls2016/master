<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class marginalizations
 * @package App\Models
 * @version September 27, 2017, 3:41 pm UTC
 *
 * @property string name
 * @property string description
 */
class marginalizations extends Model
{
    use SoftDeletes;

    public $table = 'marginalizations';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'max:45'
    ];

    
}
