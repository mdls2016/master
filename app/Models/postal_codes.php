<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class postal_codes
 * @package App\Models
 * @version August 15, 2017, 6:19 pm UTC
 *
 * @method static postal_codes find($id=null, $columns = array())
 * @method static postal_codes|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer id_neighborhood
 * @property integer identifier
 */
class postal_codes extends Model
{
    use SoftDeletes;

    public $table = 'postal_codes';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'identifier'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'identifier' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'identifier' => 'required,min:5'
    ];

    
}
