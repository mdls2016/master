<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class categories
 * @package App\Models
 * @version August 23, 2017, 6:04 pm UTC
 *
 * @method static categories find($id=null, $columns = array())
 * @method static categories|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer subtheme_id
 * @property string name
 */
class categories extends Model
{
    use SoftDeletes;

    public $table = 'categories';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'subtheme_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'subtheme_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subtheme_id' => 'required',
        'name' => 'required'
    ];
    // Padre
    public function subthemes() {
        return $this->belongsTo('App\\Models\\subthemes', "subtheme_id");
    }

    
}
