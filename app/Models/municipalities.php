<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class municipalities
 * @package App\Models
 * @version August 15, 2017, 7:14 pm UTC
 *
 * @method static municipalities find($id=null, $columns = array())
 * @method static municipalities|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer entity_id
 * @property string identifier
 * @property string name
 * @property integer population
 * @property boolean indigenous
 * @property boolean cnch
 * @property integer marginalization_id
 */
class municipalities extends Model
{
    use SoftDeletes;

    public $table = 'municipalities';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity_id',
        'identifier',
        'name',
        'population',
        'indigenous',
        'cnch',
        'marginalization_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entity_id' => 'integer',
        'identifier' => 'string',
        'name' => 'string',
        'population' => 'integer',
        'indigenous' => 'boolean',
        'cnch' => 'boolean',
        'marginalization_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'entity_id' => 'required',
        'identifier' => 'max:45',
        'name' => 'max:45',
        'marginalization_id' => 'required'
    ];
    // Dependen de:
    public function entities() {
        return $this->belongsTo('App\\Models\\entities', "entity_id");
    }
    public function marginalizations() {
        return $this->belongsTo('App\\Models\\marginalizations', "marginalization_id");
    }
    // Derivan:
    public function neighborhoods() {
        return $this->hasMany('App\\Models\\neighborhoods', "id_municipality");
    }

}
