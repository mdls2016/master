<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class themes
 * @package App\Models
 * @version August 23, 2017, 6:07 pm UTC
 *
 * @method static themes find($id=null, $columns = array())
 * @method static themes|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string name
 * @property string icon
 */
class themes extends Model
{
    use SoftDeletes;

    public $table = 'themes';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'icon',
        'ionic_icon'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'icon' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function subThemes() {
        return $this->hasMany('App\\Models\\subthemes', "theme_id");
    }

    
}
