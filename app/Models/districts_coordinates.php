<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class districts_coordinates
 * @package App\Models
 * @version March 15, 2018, 6:00 pm UTC
 *
 * @property integer entity_id
 * @property string identifier
 * @property mediumText coordinates
 */
class districts_coordinates extends Model
{
    use SoftDeletes;

    public $table = 'districts_coordinates';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity_id',
        'identifier',
        'coordinates'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entity_id' => 'integer',
        'identifier' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
