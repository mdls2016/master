<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class entities
 * @package App\Models
 * @version August 15, 2017, 7:02 pm UTC
 *
 * @method static entities find($id=null, $columns = array())
 * @method static entities|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer circumscription_id
 * @property string identifier
 * @property string name
 */
class entities extends Model
{
    use SoftDeletes;

    public $table = 'entities';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'circumscription_id',
        'identifier',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'circumscription_id' => 'integer',
        'identifier' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'circumscription_id' => 'required',
        'identifier' => 'max:45',
        'name' => 'max:45'
    ];
    // Padre
    public function circumscriptions() {
        return $this->belongsTo('App\\Models\\circumscriptions', "circumscription_id");
    }
    // Hijo
    public function municipalities() {
        return $this->hasMany('App\\Models\\municipalities', "entity_id");
    }
}
