<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class sections_coordinates
 * @package App\Models
 * @version March 15, 2018, 11:55 pm UTC
 *
 * @property mediumText coordinates
 * @property integer district_id
 * @property integer section_id
 */
class sections_coordinates extends Model
{
    use SoftDeletes;

    public $table = 'sections_coordinates';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'coordinates',
        'district_id',
        'section_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'district_id' => 'integer',
        'section_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'district_id' => 'municipality_id integer:unsigned'
    ];

    
}
