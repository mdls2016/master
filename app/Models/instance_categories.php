<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class instance_categories
 * @package App\Models
 * @version November 2, 2017, 12:00 am UTC
 *
 * @property integer instance_id
 * @property integer category_id
 */
class instance_categories extends Model
{
    use SoftDeletes;

    public $table = 'instance_categories';
    

    protected $dates = ['deleted_at'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public $fillable = [
        'instance_id',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'instance_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
