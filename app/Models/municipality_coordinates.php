<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class municipality_coordinates
 * @package App\Models
 * @version March 6, 2018, 4:22 am UTC
 *
 * @property integer municipality_id
 * @property string name
 * @property mediumtext coordinates
 */
class municipality_coordinates extends Model
{
    use SoftDeletes;

    public $table = 'municipality_coordinates';

    protected $hidden = ['deleted_at','created_at','updated_at'];
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'municipality_id',
        'name',
        'coordinates'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'municipality_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'municipality_id' => 'required',
        'name' => 'required',
        'coordinates' => 'required'
    ];

    
}
