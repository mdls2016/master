<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class entity_coordinates
 * @package App\Models
 * @version March 6, 2018, 4:00 am UTC
 *
 * @property integer entity_id
 * @property string name
 * @property mediumtext coordinates
 */
class entity_coordinates extends Model
{
    use SoftDeletes;

    public $table = 'entity_coordinates';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity_id',
        'name',
        'coordinates'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entity_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'entity_id' => 'required',
        'name' => 'required',
        'coordinates' => 'required'
    ];

    
}
