<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class neighborhoods
 * @package App\Models
 * @version August 15, 2017, 6:27 pm UTC
 *
 * @method static neighborhoods find($id=null, $columns = array())
 * @method static neighborhoods|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer id_municipality
 * @property string name
 */
class neighborhoods extends Model
{
    use SoftDeletes;

    public $table = 'neighborhoods';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_municipality',
        'name'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_municipality' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_municipality' => 'required'
    ];
    // Dependen de:
    public function municipalities() {
        return $this->belongsTo('App\\Models\\municipalities', "id_municipality");
    }
    public function postal_codes() {
        return $this->belongsTo('App\\Models\\postal_codes', "id_postal_code");
    }
    public function settlements() {
        return $this->belongsTo('App\\Models\\settlements', "id_settlement");
    }

}
