<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class districts
 * @package App\Models
 * @version August 15, 2017, 7:05 pm UTC
 *
 * @method static districts find($id=null, $columns = array())
 * @method static districts|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string identifier
 * @property integer entity_id
 */
class districts extends Model
{
    use SoftDeletes;

    public $table = 'districts';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'identifier',
        'entity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'identifier' => 'string',
        'entity_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'identifier' => 'max:45',
        'entity_id' => 'required'
    ];

    
}
