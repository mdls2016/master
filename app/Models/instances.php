<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class instances
 * @package App\Models
 * @version August 31, 2017, 6:19 pm UTC
 *
 * @property integer participant_id
 * @property string name
 * @property string shortname
 * @property integer entity_id
 * @property integer municipality_id
 * @property integer description
 * @property boolean secretary_state
 * @property boolean secretary_municipality
 */
class instances extends Model
{
    use SoftDeletes;

    public $table = 'instances';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'participant_id',
        'name',
        'shortname',
        'entity_id',
        'web_page',
        'municipality_id',
        'description',
        'direction',
        'phone',
        'email',
        'neighborhood_id',
        'postal_code_id',
        'functionary_id',
        'name_functionary',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'participant_id' => 'integer',
        'name' => 'string',
        'shortname' => 'string',
        'entity_id' => 'integer',
        'municipality_id' => 'integer',
        'description' => 'string',
        'direction' => 'string',
        'web_page' => 'string',
        'neighborhood_id' => 'integer',
        'postal_code_id' => 'integer',
        'functionary_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'participant_id' => 'required',
        'phone' => 'max:30'
    ];
    public function entities() {
        return $this->belongsTo('App\\Models\\entities', "entity_id");
    }
    public function municipalities() {
        return $this->belongsTo('App\\Models\\municipalities', "municipality_id");
    }

}
