<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class sections
 * @package App\Models
 * @version August 15, 2017, 6:59 pm UTC
 *
 * @method static sections find($id=null, $columns = array())
 * @method static sections|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer id_municipality
 * @property string identifier
 */
class sections extends Model
{
    use SoftDeletes;

    public $table = 'sections';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_municipality',
        'identifier'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_municipality' => 'integer',
        'identifier' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_municipality' => 'required',
        'identifier' => 'required, max:45'
    ];

    
}
