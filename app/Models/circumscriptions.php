<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class circumscriptions
 * @package App\Models
 * @version August 15, 2017, 5:34 pm UTC
 *
 * @method static circumscriptions find($id=null, $columns = array())
 * @method static circumscriptions|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 */
class circumscriptions extends Model
{
    use SoftDeletes;

    public $table = 'circumscriptions';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function entities() {
        return $this->hasMany('App\\Models\\entities', "circumscription_id");
    }

    
}
