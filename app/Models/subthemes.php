<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class subthemes
 * @package App\Models
 * @version August 23, 2017, 6:06 pm UTC
 *
 * @method static subthemes find($id=null, $columns = array())
 * @method static subthemes|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer theme_id
 * @property string name
 */
class subthemes extends Model
{
    use SoftDeletes;

    public $table = 'subthemes';

    protected $hidden = ['deleted_at','created_at','updated_at'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'theme_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'theme_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'theme_id' => 'required'
    ];
    // Padre
    public function themes() {
        return $this->belongsTo('App\\Models\\themes', "theme_id");
    }
    // Hijo
    public function categories() {
        return $this->hasMany('App\\Models\\categories', "subtheme_id");
    }

    
}
