<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstancesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortname')->default('')->nullable();
            $table->integer('participant_id');
            $table->integer('entity_id')->default(0)->nullable();
            $table->integer('municipality_id')->default(0)->nullable();
            $table->string('web_page')->default('')->nullable();
            $table->string('direction')->default('')->nullable();
            $table->string('phone')->default('')->nullable();
            $table->string('email')->default('')->nullable();
            $table->string('responsable')->default('')->nullable();
            $table->string('description')->default('')->nullable();
            $table->boolean('secretary_state')->default(false);
            $table->boolean('secretary_municipality')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instances');
    }
}
