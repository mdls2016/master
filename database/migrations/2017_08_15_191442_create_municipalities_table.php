<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMunicipalitiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipalities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id');
            $table->string('identifier');

            $table->string('name');
            $table->integer('population');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('indigenous')->default(false);
            $table->boolean('cnch')->default(false);
            $table->integer('marginalization_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('municipalities');
    }
}
