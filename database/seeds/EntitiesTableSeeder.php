<?php

use Illuminate\Database\Seeder;

class EntitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('entities')->delete();

        \DB::table('entities')->insert(array (
            0 =>
            array (
                'id' => 1,
                'circumscription_id' => 2,
                'count_municipalities' => 11,
                'count_districts' => 3,
                'identifier' => '01',
                'name' => 'Aguascalientes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'circumscription_id' => 1,
                'count_municipalities' => 5,
                'count_districts' => 8,
                'identifier' => '02',
                'name' => 'Baja California',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'circumscription_id' => 1,
                'count_municipalities' => 5,
                'count_districts' => 2,
                'identifier' => '03',
                'name' => 'Baja California Sur',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'circumscription_id' => 3,
                'count_municipalities' => 11,
                'count_districts' => 2,
                'identifier' => '04',
                'name' => 'Campeche',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'circumscription_id' => 2,
                'count_municipalities' => 38,
                'count_districts' => 7,
                'identifier' => '05',
                'name' => 'Coahuila de Zaragoza',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'circumscription_id' => 5,
                'count_municipalities' => 10,
                'count_districts' => 2,
                'identifier' => '06',
                'name' => 'Colima',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'circumscription_id' => 3,
                'count_municipalities' => 118,
                'count_districts' => 12,
                'identifier' => '07',
                'name' => 'Chiapas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'circumscription_id' => 1,
                'count_municipalities' => 68,
                'count_districts' => 9,
                'identifier' => '08',
                'name' => 'Chihuahua',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'circumscription_id' => 4,
                'count_municipalities' => 16,
                'count_districts' => 27,
                'identifier' => '09',
                'name' => 'Ciudad de México',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'circumscription_id' => 1,
                'count_municipalities' => 39,
                'count_districts' => 4,
                'identifier' => '10',
                'name' => 'Durango',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'circumscription_id' => 2,
                'count_municipalities' => 46,
                'count_districts' => 14,
                'identifier' => '11',
                'name' => 'Guanajuato',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'circumscription_id' => 4,
                'count_municipalities' => 81,
                'count_districts' => 9,
                'identifier' => '12',
                'name' => 'Guerrero',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'circumscription_id' => 5,
                'count_municipalities' => 84,
                'count_districts' => 7,
                'identifier' => '13',
                'name' => 'Hidalgo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'circumscription_id' => 1,
                'count_municipalities' => 125,
                'count_districts' => 19,
                'identifier' => '14',
                'name' => 'Jalisco',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                'circumscription_id' => 5,
                'count_municipalities' => 125,
                'count_districts' => 40,
                'identifier' => '15',
                'name' => 'Estado de México',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 16,
                'circumscription_id' => 5,
                'count_municipalities' => 113,
                'count_districts' => 12,
                'identifier' => '16',
                'name' => 'Michoacán de Ocampo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 17,
                'circumscription_id' => 4,
                'count_municipalities' => 33,
                'count_districts' => 5,
                'identifier' => '17',
                'name' => 'Morelos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 18,
                'circumscription_id' => 1,
                'count_municipalities' => 20,
                'count_districts' => 3,
                'identifier' => '18',
                'name' => 'Nayarit',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 19,
                'circumscription_id' => 2,
                'count_municipalities' => 51,
                'count_districts' => 12,
                'identifier' => '19',
                'name' => 'Nuevo León',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 20,
                'circumscription_id' => 3,
                'count_municipalities' => 570,
                'count_districts' => 11,
                'identifier' => '20',
                'name' => 'Oaxaca',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 21,
                'circumscription_id' => 4,
                'count_municipalities' => 217,
                'count_districts' => 16,
                'identifier' => '21',
                'name' => 'Puebla',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 22,
                'circumscription_id' => 2,
                'count_municipalities' => 18,
                'count_districts' => 4,
                'identifier' => '22',
                'name' => 'Querétaro',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 23,
                'circumscription_id' => 3,
                'count_municipalities' => 10,
                'count_districts' => 3,
                'identifier' => '23',
                'name' => 'Quintana Roo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 24,
                'circumscription_id' => 2,
                'count_municipalities' => 58,
                'count_districts' => 7,
                'identifier' => '24',
                'name' => 'San Luis Potosí',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 25,
                'circumscription_id' => 1,
                'count_municipalities' => 18,
                'count_districts' => 8,
                'identifier' => '25',
                'name' => 'Sinaloa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 26,
                'circumscription_id' => 1,
                'count_municipalities' => 72,
                'count_districts' => 7,
                'identifier' => '26',
                'name' => 'Sonora',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 27,
                'circumscription_id' => 3,
                'count_municipalities' => 17,
                'count_districts' => 6,
                'identifier' => '27',
                'name' => 'Tabasco',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 28,
                'circumscription_id' => 2,
                'count_municipalities' => 43,
                'count_districts' => 8,
                'identifier' => '28',
                'name' => 'Tamaulipas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 29,
                'circumscription_id' => 4,
                'count_municipalities' => 60,
                'count_districts' => 3,
                'identifier' => '29',
                'name' => 'Tlaxcala',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 30,
                'circumscription_id' => 3,
                'count_municipalities' => 212,
                'count_districts' => 21,
                'identifier' => '30',
                'name' => 'Veracruz',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 31,
                'circumscription_id' => 3,
                'count_municipalities' => 106,
                'count_districts' => 5,
                'identifier' => '31',
                'name' => 'Yucatán',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 32,
                'circumscription_id' => 2,
                'count_municipalities' => 58,
                'count_districts' => 4,
                'identifier' => '32',
                'name' => 'Zacatecas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
