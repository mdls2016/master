<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('categories')->delete();

        \DB::table('categories')->insert(array (
            0 =>
            array (
                'id' => 1,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Artísticas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Deportivas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'subtheme_id' => 1,
            'coh' => true,
                'name' => 'Educación básica (primaria)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'subtheme_id' => 1,
            'coh' => true,
                'name' => 'Educación inicial (antes de primaria)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'subtheme_id' => 1,
            'coh' => true,
                'name' => 'Educación media (secundaria)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'subtheme_id' => 1,
            'coh' => true,
                'name' => 'Educación media-superior (preparatoria)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Estudios en el extranjero',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Idiomas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Investigaciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Licenciaturas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Post-Grado',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 16,
                'subtheme_id' => 3,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 17,
                'subtheme_id' => 3,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 19,
                'subtheme_id' => 3,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 20,
                'subtheme_id' => 3,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 47,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Asistencia técnica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 49,
                'subtheme_id' => 9,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 50,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Ciudad libre de graffiti',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 51,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Creación de rellenos sanitarios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 52,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Reciclado de basura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 53,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Recolección de basura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 54,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Retiro de residuos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 55,
                'subtheme_id' => 10,
                'coh' => true,
                'name' => 'Saneamiento de aguas residuales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 57,
                'subtheme_id' => 12,
                'coh' => true,
                'name' => 'Animales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 66,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Campañas de promoción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 67,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Comercialización de productos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 69,
                'subtheme_id' => 15,
                'coh' => true,
                'name' => 'Asesoría',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 70,
                'subtheme_id' => 15,
                'coh' => true,
                'name' => 'Capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 75,
                'subtheme_id' => 16,
                'coh' => true,
                'name' => 'Asesoría técnica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 76,
                'subtheme_id' => 17,
                'coh' => true,
                'name' => 'Fitosanitarias',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 77,
                'subtheme_id' => 17,
                'coh' => true,
                'name' => 'Inocuidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 78,
                'subtheme_id' => 18,
                'coh' => true,
                'name' => 'Sanidad animal',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 89,
                'subtheme_id' => 18,
                'coh' => true,
                'name' => 'Sanidad vegetal',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 92,
                'subtheme_id' => 19,
                'coh' => true,
                'name' => 'Acopio de granos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 93,
                'subtheme_id' => 19,
                'coh' => true,
                'name' => 'Modernización',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 94,
                'subtheme_id' => 19,
                'coh' => true,
                'name' => 'Perforación de pozos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 95,
                'subtheme_id' => 19,
                'coh' => true,
                'name' => 'Sistemas de riego',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 102,
                'subtheme_id' => 23,
                'coh' => true,
                'name' => 'Adquisición',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 103,
                'subtheme_id' => 24,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 104,
                'subtheme_id' => 23,
                'coh' => true,
                'name' => 'Ampliación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 106,
                'subtheme_id' => 23,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 107,
                'subtheme_id' => 26,
                'coh' => true,
                'name' => 'Asesoramiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 109,
                'subtheme_id' => 29,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 110,
                'subtheme_id' => 30,
                'coh' => true,
                'name' => 'A municipios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 111,
                'subtheme_id' => 30,
                'coh' => true,
                'name' => 'A operadores',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 112,
                'subtheme_id' => 31,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 114,
                'subtheme_id' => 33,
                'coh' => true,
                'name' => 'Empresarios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 115,
                'subtheme_id' => 33,
                'coh' => true,
                'name' => 'Exportaciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 119,
                'subtheme_id' => 33,
                'coh' => true,
                'name' => 'Pymes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 124,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Aprovechamiento de recursos regionales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 125,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Capacitación en el proceso administrativo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 126,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Capacitación para el empleo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 127,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Creación de nuevas empresas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 129,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Pymes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 130,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Salud en el empleo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 132,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Adulto',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 133,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Adulto mayor',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 134,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Empleo temporal',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 140,
                'subtheme_id' => 39,
                'coh' => true,
                'name' => 'Actividades recreativas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 142,
                'subtheme_id' => 39,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 148,
                'subtheme_id' => 42,
                'coh' => true,
                'name' => 'Personas abandonadas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 152,
                'subtheme_id' => 44,
                'coh' => true,
                'name' => 'Líderes sindicales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 153,
                'subtheme_id' => 45,
                'coh' => true,
                'name' => 'Conferencias',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 154,
                'subtheme_id' => 45,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 155,
                'subtheme_id' => 45,
                'coh' => true,
                'name' => 'Promoción de igualdad de género',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 156,
                'subtheme_id' => 45,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 176,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Asesoría',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 177,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 178,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Concursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 181,
                'subtheme_id' => 61,
                'coh' => true,
                'name' => 'Banco de sangre',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 182,
                'subtheme_id' => 61,
                'coh' => true,
                'name' => 'Órganos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 185,
                'subtheme_id' => 62,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 186,
                'subtheme_id' => 62,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 187,
                'subtheme_id' => 63,
                'coh' => true,
                'name' => 'Aparatos ortopédicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 188,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Comunidades saludables',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 189,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Control de epidemias',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 191,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Enfermedades de transmisión sexual',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 192,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Laboral',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 193,
                'subtheme_id' => 65,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 194,
                'subtheme_id' => 65,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 195,
                'subtheme_id' => 65,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 196,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Nutrición',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 197,
                'subtheme_id' => 65,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 198,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Planificación familiar',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 199,
                'subtheme_id' => 66,
                'coh' => true,
                'name' => 'Adquisición',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 200,
                'subtheme_id' => 66,
                'coh' => true,
                'name' => 'Donación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 201,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Actividad física',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 202,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Prevención de accidentes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 203,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Prevención de adicciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 204,
                'subtheme_id' => 68,
                'coh' => true,
                'name' => 'Asistencia técnica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 205,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Prevención de cáncer',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 206,
                'subtheme_id' => 68,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 207,
                'subtheme_id' => 69,
                'coh' => true,
                'name' => 'Puentes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 208,
                'subtheme_id' => 64,
                'coh' => true,
                'name' => 'Violencia familiar',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 209,
                'subtheme_id' => 69,
                'coh' => true,
                'name' => 'Caminos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 217,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Aparatos médicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 218,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Atención médica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 219,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Brigadas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 220,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Equipo médico y funcional',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 221,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Medicamentos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 222,
                'subtheme_id' => 71,
                'coh' => true,
                'name' => 'Seguros',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 223,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Comunidades rurales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 224,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Estudios de diagnóstico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 225,
                'subtheme_id' => 74,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 226,
                'subtheme_id' => 75,
                'coh' => true,
                'name' => 'Alimentación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 228,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Participación ciudadana',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 229,
                'subtheme_id' => 77,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 230,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 231,
                'subtheme_id' => 44,
                'coh' => true,
                'name' => 'Participación ciudadana',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 232,
                'subtheme_id' => 87,
                'coh' => true,
                'name' => 'Apoyos económicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 234,
                'subtheme_id' => 79,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 235,
                'subtheme_id' => 79,
                'coh' => true,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 236,
                'subtheme_id' => 79,
                'coh' => true,
                'name' => 'Despensas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 237,
                'subtheme_id' => 87,
                'coh' => true,
                'name' => 'Participación ciudadana',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 238,
                'subtheme_id' => 80,
                'coh' => true,
                'name' => 'Atención a migrantes repatriados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 239,
                'subtheme_id' => 80,
                'coh' => true,
                'name' => 'Educación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 241,
                'subtheme_id' => 80,
                'coh' => true,
                'name' => 'Proyectos comunitarios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 242,
                'subtheme_id' => 81,
                'coh' => true,
                'name' => 'Actividades recreativas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 243,
                'subtheme_id' => 82,
                'coh' => true,
                'name' => 'Aparatos ortopédicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 244,
                'subtheme_id' => 82,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 247,
                'subtheme_id' => 84,
                'coh' => true,
                'name' => 'Apoyo económico a damnificados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 248,
                'subtheme_id' => 63,
                'coh' => true,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 249,
                'subtheme_id' => 85,
                'coh' => true,
                'name' => 'Alimentación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 250,
                'subtheme_id' => 85,
                'coh' => true,
                'name' => 'Despensas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 254,
                'subtheme_id' => 89,
                'coh' => true,
                'name' => 'Asistencial',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 255,
                'subtheme_id' => 90,
                'coh' => true,
                'name' => 'Ingreso',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 256,
                'subtheme_id' => 91,
                'coh' => true,
                'name' => 'Alimentación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 258,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Agua potable',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 259,
                'subtheme_id' => 93,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 260,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Alcantarillado',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 261,
                'subtheme_id' => 93,
                'coh' => true,
                'name' => 'Sociales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 262,
                'subtheme_id' => 93,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 263,
                'subtheme_id' => 94,
                'coh' => true,
                'name' => 'Descuentos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 264,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Drenaje',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 265,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Electrificación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 268,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Energía alterna',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 269,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Limpieza',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 270,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Pavimentación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 271,
                'subtheme_id' => 14,
                'coh' => true,
                'name' => 'Proyectos sociales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 273,
                'subtheme_id' => 15,
                'coh' => true,
                'name' => 'Fondos de aseguramiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 275,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'A autoridades electas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 276,
                'subtheme_id' => 99,
                'coh' => true,
                'name' => 'Concursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 277,
                'subtheme_id' => 100,
                'coh' => true,
                'name' => 'Actividades infantiles',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 278,
                'subtheme_id' => 100,
                'coh' => true,
                'name' => 'Actividades juveniles',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 279,
                'subtheme_id' => 101,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 281,
                'subtheme_id' => 29,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 282,
                'subtheme_id' => 103,
                'coh' => true,
                'name' => 'Agroindustriales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 283,
                'subtheme_id' => 103,
                'coh' => true,
                'name' => 'Calidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 284,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Actividades de recreación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 287,
                'subtheme_id' => 23,
                'coh' => true,
                'name' => 'Paquete de material',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 288,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'A funcionarios públicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 289,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Técnicas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 290,
                'subtheme_id' => 1,
                'coh' => true,
                'name' => 'Tesistas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 291,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 292,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Educación especial',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 293,
                'subtheme_id' => 59,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 294,
                'subtheme_id' => 62,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 295,
                'subtheme_id' => 62,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 296,
                'subtheme_id' => 63,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 297,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 298,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Concursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 299,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Conferencias',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 300,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 301,
                'subtheme_id' => 8,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 302,
                'subtheme_id' => 9,
                'coh' => true,
                'name' => 'Financiamiento a fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 303,
                'subtheme_id' => 9,
                'coh' => true,
                'name' => 'Inversión para proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 304,
                'subtheme_id' => 9,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 305,
                'subtheme_id' => 12,
                'coh' => true,
                'name' => 'Áreas naturales protegidas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 306,
                'subtheme_id' => 12,
                'coh' => true,
                'name' => 'Flora y fauna',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 307,
                'subtheme_id' => 12,
                'coh' => true,
                'name' => 'Reservas ecológicas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 308,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Relleno sanitario',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 309,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Tratamiento de aguas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 310,
                'subtheme_id' => 16,
                'coh' => true,
                'name' => 'Comercialización de productos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 311,
                'subtheme_id' => 16,
                'coh' => true,
                'name' => 'Información, investigación y tecnología',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 312,
                'subtheme_id' => 17,
            'coh' => true,
                'name' => 'Salubridad y/o sanidad (vacunación)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 313,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 314,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'Adquisición de ganado',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 315,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'Comercialización de productos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 316,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'Mejoramiento genético',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 317,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'Proyectos productivos para el área rural',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 318,
                'subtheme_id' => 104,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 319,
                'subtheme_id' => 114,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 320,
                'subtheme_id' => 114,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 321,
                'subtheme_id' => 114,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 322,
                'subtheme_id' => 114,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 323,
                'subtheme_id' => 99,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 324,
                'subtheme_id' => 99,
                'coh' => true,
                'name' => 'Premios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 325,
                'subtheme_id' => 99,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 326,
                'subtheme_id' => 100,
                'coh' => true,
                'name' => 'Exposiciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 327,
                'subtheme_id' => 100,
                'coh' => true,
                'name' => 'Galerías',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 328,
                'subtheme_id' => 101,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 329,
                'subtheme_id' => 101,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 330,
                'subtheme_id' => 101,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 331,
                'subtheme_id' => 23,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 332,
                'subtheme_id' => 24,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 333,
                'subtheme_id' => 24,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 334,
                'subtheme_id' => 24,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 335,
                'subtheme_id' => 26,
                'coh' => true,
                'name' => 'Escrituración',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 336,
                'subtheme_id' => 26,
                'coh' => true,
                'name' => 'Orientación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 337,
                'subtheme_id' => 105,
                'coh' => true,
                'name' => 'Apoyo a damnificados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 338,
                'subtheme_id' => 105,
                'coh' => true,
                'name' => 'Reconstrucción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 339,
                'subtheme_id' => 105,
                'coh' => true,
                'name' => 'Paquete de material',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 340,
                'subtheme_id' => 27,
                'coh' => true,
                'name' => 'Asistencia técnica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 341,
                'subtheme_id' => 27,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 342,
                'subtheme_id' => 27,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 343,
                'subtheme_id' => 28,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 344,
                'subtheme_id' => 28,
                'coh' => true,
                'name' => 'Atracción de inversión',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 345,
                'subtheme_id' => 28,
                'coh' => true,
                'name' => 'Campañas de promoción turística',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 346,
                'subtheme_id' => 28,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 347,
                'subtheme_id' => 29,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 348,
                'subtheme_id' => 29,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 349,
                'subtheme_id' => 30,
                'coh' => true,
                'name' => 'A supervisores',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 350,
                'subtheme_id' => 31,
                'coh' => true,
                'name' => 'Adquisición de transportes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 351,
                'subtheme_id' => 31,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 352,
                'subtheme_id' => 31,
                'coh' => true,
                'name' => 'Modernización',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 353,
                'subtheme_id' => 31,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 357,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Jóvenes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 358,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Madres solteras',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 359,
                'subtheme_id' => 35,
                'coh' => true,
                'name' => 'Personas con discapacidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 360,
                'subtheme_id' => 103,
                'coh' => true,
                'name' => 'Exportación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 361,
                'subtheme_id' => 103,
                'coh' => true,
                'name' => 'Internacionales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 362,
                'subtheme_id' => 103,
                'coh' => true,
                'name' => 'Propiedad intelectual',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 363,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 364,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Atracción de inversión extranjera',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 365,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Créditos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 366,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Incubación de negocios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 367,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Productivos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 368,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 369,
                'subtheme_id' => 115,
                'coh' => true,
                'name' => 'Tecnología',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 370,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 371,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Becas deportivas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 372,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Deporte infantil',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 373,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Deporte juvenil',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 374,
                'subtheme_id' => 67,
                'coh' => true,
                'name' => 'Deporte para adultos mayores',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 375,
                'subtheme_id' => 39,
                'coh' => true,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 376,
                'subtheme_id' => 39,
                'coh' => true,
                'name' => 'Despensas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 377,
                'subtheme_id' => 39,
                'coh' => true,
                'name' => 'Servicios de salud',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 378,
                'subtheme_id' => 42,
                'coh' => true,
                'name' => 'Personas marginadas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 379,
                'subtheme_id' => 42,
                'coh' => true,
                'name' => 'Situaciones emergentes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 380,
                'subtheme_id' => 74,
                'coh' => true,
                'name' => 'Sociales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 381,
                'subtheme_id' => 74,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 382,
                'subtheme_id' => 75,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 383,
                'subtheme_id' => 75,
                'coh' => true,
                'name' => 'Participación social',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 384,
                'subtheme_id' => 75,
                'coh' => true,
                'name' => 'Servicios de salud',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 385,
                'subtheme_id' => 75,
                'coh' => true,
                'name' => 'Servicios educativos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 386,
                'subtheme_id' => 76,
                'coh' => true,
                'name' => 'Hijos de madres trabajadoras',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 387,
                'subtheme_id' => 76,
                'coh' => true,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 388,
                'subtheme_id' => 76,
                'coh' => true,
                'name' => 'Niños con discapacidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 389,
                'subtheme_id' => 77,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 390,
                'subtheme_id' => 77,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 391,
                'subtheme_id' => 77,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 392,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Creación y fomento de talento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 393,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Descuentos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 394,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Educación sexual',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 395,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Inclusión social',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 396,
                'subtheme_id' => 78,
                'coh' => true,
                'name' => 'Prevención de adicciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 397,
                'subtheme_id' => 81,
                'coh' => true,
                'name' => 'Alimentación y nutrición',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 398,
                'subtheme_id' => 81,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 399,
                'subtheme_id' => 81,
                'coh' => true,
                'name' => 'Deporte',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 400,
                'subtheme_id' => 81,
                'coh' => true,
                'name' => 'Servicios educativos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 401,
                'subtheme_id' => 82,
                'coh' => true,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 402,
                'subtheme_id' => 82,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 403,
                'subtheme_id' => 84,
                'coh' => true,
                'name' => 'Apoyos en especie',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 404,
                'subtheme_id' => 85,
                'coh' => true,
                'name' => 'Leche',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 405,
                'subtheme_id' => 89,
                'coh' => true,
                'name' => 'Contingencia',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 406,
                'subtheme_id' => 89,
                'coh' => true,
                'name' => 'En especie',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 407,
                'subtheme_id' => 89,
                'coh' => true,
                'name' => 'Transporte',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 408,
                'subtheme_id' => 94,
                'coh' => true,
                'name' => 'Hospitalización',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 409,
                'subtheme_id' => 94,
                'coh' => true,
                'name' => 'Medicamentos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 410,
                'subtheme_id' => 94,
                'coh' => true,
                'name' => 'Transplantes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 411,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'A policías',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 412,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 413,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'Personal capacitado',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 414,
                'subtheme_id' => 98,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 415,
                'subtheme_id' => 68,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 416,
                'subtheme_id' => 116,
                'coh' => true,
                'name' => 'Bordos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 417,
                'subtheme_id' => 116,
                'coh' => true,
                'name' => 'Presas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 418,
                'subtheme_id' => 116,
                'coh' => true,
                'name' => 'Redes de abastecimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 419,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Alumbrado público',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 420,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Calles',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 421,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Panteones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 422,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Parques',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 423,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Pavimentación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 424,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Rastro',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 425,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'A policías',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 426,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'Asistencia legal',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 427,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'Campañas de prevención de delitos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 428,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'Cursos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 429,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'Talleres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 430,
                'subtheme_id' => 108,
                'coh' => true,
                'name' => 'Alarmas vecinales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 431,
                'subtheme_id' => 108,
                'coh' => true,
                'name' => 'Patrullaje',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 432,
                'subtheme_id' => 108,
                'coh' => true,
                'name' => 'Vigilancia',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 433,
                'subtheme_id' => 108,
                'coh' => true,
                'name' => 'Violencia familiar',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 434,
                'subtheme_id' => 109,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 435,
                'subtheme_id' => 109,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 436,
                'subtheme_id' => 109,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 437,
                'subtheme_id' => 109,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 438,
                'subtheme_id' => 118,
                'coh' => true,
                'name' => 'A fondo perdido',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 439,
                'subtheme_id' => 118,
                'coh' => true,
                'name' => 'Innovación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 440,
                'subtheme_id' => 118,
                'coh' => true,
                'name' => 'Investigación en ciencia',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 441,
                'subtheme_id' => 118,
                'coh' => true,
                'name' => 'Investigación en tecnología',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 442,
                'subtheme_id' => 118,
                'coh' => true,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 443,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Clúster',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 444,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Construcción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 445,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 446,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Mantenimiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 447,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Modernización',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 448,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Parques industriales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 449,
                'subtheme_id' => 119,
                'coh' => true,
                'name' => 'Rehabilitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 450,
                'subtheme_id' => 120,
                'coh' => true,
                'name' => 'Capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 451,
                'subtheme_id' => 120,
                'coh' => true,
                'name' => 'Desarrollo de software',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 452,
                'subtheme_id' => 107,
                'coh' => true,
                'name' => 'Apoyo económico',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 453,
                'subtheme_id' => 107,
                'coh' => true,
                'name' => 'Prevención',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 454,
                'subtheme_id' => 120,
                'coh' => true,
                'name' => 'Nuevas tecnologías',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 455,
                'subtheme_id' => 120,
                'coh' => true,
                'name' => 'Seguridad informática',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 456,
                'subtheme_id' => 120,
                'coh' => true,
                'name' => 'Servicios de TI',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 457,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Comercialización',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 458,
                'subtheme_id' => 79,
                'coh' => true,
                'name' => 'Becas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 459,
                'subtheme_id' => 121,
                'coh' => true,
                'name' => 'Creación página web',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 460,
                'subtheme_id' => 121,
                'coh' => true,
                'name' => 'Ventas en línea',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 461,
                'subtheme_id' => 122,
                'coh' => true,
                'name' => 'Revistas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 462,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Mercados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 463,
                'subtheme_id' => 117,
                'coh' => true,
                'name' => 'Centrales de abasto',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 464,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Centrales de abasto',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 465,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Mercados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 466,
                'subtheme_id' => 92,
                'coh' => true,
                'name' => 'Rastro',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 467,
                'subtheme_id' => 106,
                'coh' => true,
                'name' => 'Violencia contra las mujeres',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 468,
                'subtheme_id' => 80,
                'coh' => true,
                'name' => 'Becas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 469,
                'subtheme_id' => 34,
                'coh' => true,
                'name' => 'Exportaciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
