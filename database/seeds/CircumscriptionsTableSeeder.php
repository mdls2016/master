<?php

use Illuminate\Database\Seeder;

class CircumscriptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('circumscriptions')->delete();

        \DB::table('circumscriptions')->insert(array (
            0 =>
            array (
                'id' => 1,

                'identifier' => '01',

                'deleted_at' => NULL,
                'created_at' => '2013-04-11 22:32:50',
                'updated_at' => '2013-04-11 22:32:50',
            ),
            1 =>
            array (
                'id' => 2,

                'identifier' => '02',
                'deleted_at' => NULL,
                'created_at' => '2013-04-11 22:32:50',
                'updated_at' => '2013-04-11 22:32:50',
            ),
            2 =>
            array (
                'id' => 3,

                'identifier' => '03',

                'deleted_at' => NULL,
                'created_at' => '2013-04-11 22:32:50',
                'updated_at' => '2013-04-11 22:32:50',
            ),
            3 =>
            array (
                'id' => 4,

                'identifier' => '04',

                'deleted_at' => NULL,
                'created_at' => '2013-04-11 22:32:50',
                'updated_at' => '2013-04-11 22:32:50',
            ),
            4 =>
            array (
                'id' => 5,

                'identifier' => '05',

                'deleted_at' => NULL,
                'created_at' => '2013-04-11 22:32:50',
                'updated_at' => '2013-04-11 22:32:50',
            ),

        ));


    }
}
