<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('sections')->delete();

        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 1,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '356',
                'created_at' => '2017-09-25 22:41:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '357',
                'created_at' => '2017-09-25 22:41:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '358',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '359',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '360',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '361',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '362',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '363',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '364',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '365',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2582',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '366',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '367',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '368',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '369',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 16,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '370',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 17,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '371',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 18,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '372',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 19,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '373',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 20,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '374',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 21,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '375',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 22,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '376',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 23,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '377',
                'created_at' => '2017-09-25 22:41:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 24,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '378',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 25,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '379',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 26,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '380',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 27,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '381',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 28,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '395',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 29,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '382',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 30,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '383',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 31,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '384',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 32,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '385',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 33,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '386',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 34,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '387',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 35,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '396',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 36,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '397',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 37,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '388',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 38,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '389',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 39,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '398',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 40,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2038',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 41,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '390',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 42,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '391',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 43,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '392',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 44,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '393',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 45,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '394',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 46,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '399',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 47,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '400',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 48,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '401',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 49,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '402',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 50,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '403',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 51,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '1999',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 52,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2010',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 53,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '404',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 54,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '405',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 55,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '406',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 56,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '407',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 57,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '408',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 58,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '409',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 59,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2023',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 60,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '410',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 61,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '411',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 62,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '412',
                'created_at' => '2017-09-25 22:41:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 63,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '413',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 64,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2000',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 65,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '414',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 66,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '415',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 67,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2001',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 68,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2002',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 69,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2009',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 70,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '416',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 71,
                // 'id_municipality' => 966,
                'district_id' => 1,
                'identifier' => '417',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 72,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2037',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 73,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2003',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 74,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2004',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 75,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2005',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 76,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2006',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 77,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2007',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 78,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2008',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 79,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2059',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 80,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2011',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 81,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2012',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 82,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2013',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 83,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2014',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 84,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2015',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 85,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2016',
                'created_at' => '2017-09-25 22:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 86,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2017',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 87,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2018',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 88,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2019',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 89,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2020',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 90,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2021',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 91,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2022',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 92,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2024',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 93,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2025',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 94,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2026',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 95,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2027',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 96,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2028',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 97,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2029',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 98,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2030',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 99,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2031',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 100,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2032',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 101,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2033',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 102,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2034',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 103,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2035',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 104,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2036',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 105,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2039',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 106,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2040',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 107,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2041',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 108,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2042',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 109,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2043',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 110,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2044',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 111,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2045',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 112,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2046',
                'created_at' => '2017-09-25 22:41:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 113,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2047',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 114,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2050',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 115,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2431',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 116,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2048',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 117,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2051',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 118,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2052',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 119,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2053',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 120,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2054',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 121,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2055',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 122,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2056',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 123,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2057',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 124,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2058',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 125,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2432',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 126,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2049',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 127,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '74',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 128,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2060',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 129,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2061',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 130,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2062',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 131,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2063',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 132,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2064',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 133,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2065',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 134,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2066',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 135,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2067',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 136,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2068',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 137,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2069',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 138,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2070',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 139,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2071',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 140,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2072',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 141,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2073',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 142,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2074',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 143,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2075',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 144,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2076',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 145,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2077',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 146,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2078',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 147,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2459',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 148,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2079',
                'created_at' => '2017-09-25 22:41:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 149,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2080',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 150,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2081',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 151,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2082',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 152,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2083',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 153,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2084',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 154,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '75',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 155,
                // 'id_municipality' => 995,
                'district_id' => 1,
                'identifier' => '2085',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 156,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '63',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 157,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1802',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 158,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '66',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 159,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '68',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 160,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '69',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 161,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '70',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 162,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '71',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 163,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '72',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 164,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '73',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 165,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '76',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 166,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '78',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 167,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '79',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 168,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '80',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 169,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '81',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 170,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '82',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 171,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '83',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 172,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '84',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 173,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '85',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 174,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '86',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 175,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '87',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 176,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '88',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 177,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '89',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 178,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '90',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 179,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '91',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 180,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '92',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 181,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '93',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 182,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '94',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 183,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '111',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 184,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '95',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 185,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '96',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 186,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '97',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 187,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '98',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 188,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '99',
                'created_at' => '2017-09-25 22:41:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 189,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '100',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 190,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '101',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 191,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '112',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 192,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '113',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 193,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '102',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 194,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '103',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 195,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '104',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 196,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '105',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 197,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '106',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 198,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '107',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 199,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2170',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 200,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '108',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 201,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '109',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 202,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '110',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 203,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2171',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 204,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '114',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 205,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '115',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 206,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '116',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 207,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '117',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 208,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '118',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 209,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '119',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 210,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '120',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 211,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '121',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 212,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '122',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 213,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '123',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 214,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '124',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 215,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '125',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 216,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '126',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 217,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '127',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 218,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '128',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 219,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '130',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 220,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '131',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 221,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '132',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 222,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '133',
                'created_at' => '2017-09-25 22:41:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 223,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '134',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 224,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '135',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 225,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '136',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 226,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '137',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 227,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '138',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 228,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '139',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 229,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '140',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 230,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '150',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 231,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2166',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 232,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2167',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 233,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2168',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 234,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2169',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 235,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2172',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 236,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2173',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 237,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2174',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 238,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2175',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 239,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2176',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 240,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2177',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 241,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2178',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 242,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2179',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 243,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2180',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 244,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2181',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 245,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2182',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 246,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2183',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 247,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2184',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 248,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2185',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 249,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2186',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 250,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2187',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 251,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2188',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 252,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2189',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 253,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2190',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 254,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2191',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 255,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2192',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 256,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2193',
                'created_at' => '2017-09-25 22:41:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 257,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2194',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 258,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2195',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 259,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2196',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 260,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2197',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 261,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2198',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 262,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2199',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 263,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2200',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 264,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2201',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 265,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2202',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 266,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2203',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 267,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2417',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 268,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2204',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 269,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2205',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 270,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2206',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 271,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2207',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 272,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2208',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 273,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2209',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 274,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2210',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 275,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2211',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 276,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2416',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 277,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2418',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 278,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2419',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 279,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2420',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 280,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2421',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 281,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2422',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 282,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2423',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 283,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2424',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 284,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2425',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 285,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2426',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 286,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2427',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 287,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2428',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 288,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2429',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 289,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2430',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 290,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2433',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 291,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2434',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 292,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2435',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 293,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2436',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 294,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2438',
                'created_at' => '2017-09-25 22:42:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 295,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2439',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 296,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2440',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 297,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2441',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 298,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2442',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 299,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2443',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 300,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2444',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 301,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2445',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 302,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2446',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 303,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2460',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 304,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2638',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 305,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2447',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 306,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2448',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 307,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2449',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 308,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2450',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 309,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2451',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 310,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2452',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 311,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2453',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 312,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2454',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 313,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2455',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 314,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2456',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 315,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2457',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 316,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2458',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 317,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2461',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 318,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2462',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 319,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2463',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 320,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2464',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 321,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2465',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 322,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2466',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 323,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2467',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 324,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2468',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 325,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2469',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 326,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2470',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 327,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2471',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 328,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2472',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 329,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2473',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 =>
            array (
                'id' => 330,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2474',
                'created_at' => '2017-09-25 22:42:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 =>
            array (
                'id' => 331,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2475',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 =>
            array (
                'id' => 332,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2476',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 =>
            array (
                'id' => 333,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2477',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 =>
            array (
                'id' => 334,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2478',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 =>
            array (
                'id' => 335,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2479',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 =>
            array (
                'id' => 336,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2480',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 =>
            array (
                'id' => 337,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2481',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 =>
            array (
                'id' => 338,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2482',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 =>
            array (
                'id' => 339,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2483',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 =>
            array (
                'id' => 340,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2484',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 =>
            array (
                'id' => 341,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2485',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 =>
            array (
                'id' => 342,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2486',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 =>
            array (
                'id' => 343,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2487',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 =>
            array (
                'id' => 344,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2488',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 =>
            array (
                'id' => 345,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2489',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 =>
            array (
                'id' => 346,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2490',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 =>
            array (
                'id' => 347,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2491',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 =>
            array (
                'id' => 348,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2492',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 =>
            array (
                'id' => 349,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2493',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 =>
            array (
                'id' => 350,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2494',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 =>
            array (
                'id' => 351,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2495',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 =>
            array (
                'id' => 352,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2496',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 =>
            array (
                'id' => 353,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2497',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 =>
            array (
                'id' => 354,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2498',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 =>
            array (
                'id' => 355,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2499',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 =>
            array (
                'id' => 356,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2500',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 =>
            array (
                'id' => 357,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2511',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 =>
            array (
                'id' => 358,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2501',
                'created_at' => '2017-09-25 22:42:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 =>
            array (
                'id' => 359,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2502',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 =>
            array (
                'id' => 360,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2503',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 =>
            array (
                'id' => 361,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2504',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 =>
            array (
                'id' => 362,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2505',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 =>
            array (
                'id' => 363,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2506',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 =>
            array (
                'id' => 364,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2507',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 =>
            array (
                'id' => 365,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2508',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 =>
            array (
                'id' => 366,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2509',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 =>
            array (
                'id' => 367,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2510',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 =>
            array (
                'id' => 368,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2512',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 =>
            array (
                'id' => 369,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2513',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 =>
            array (
                'id' => 370,
                // 'id_municipality' => 953,
                'district_id' => 2,
                'identifier' => '2514',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 =>
            array (
                'id' => 371,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '430',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 =>
            array (
                'id' => 372,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '431',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 =>
            array (
                'id' => 373,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '432',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 =>
            array (
                'id' => 374,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '433',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 =>
            array (
                'id' => 375,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '434',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 =>
            array (
                'id' => 376,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '465',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 =>
            array (
                'id' => 377,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '435',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 =>
            array (
                'id' => 378,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '436',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 =>
            array (
                'id' => 379,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '437',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 =>
            array (
                'id' => 380,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '438',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 =>
            array (
                'id' => 381,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '440',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 =>
            array (
                'id' => 382,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '441',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 =>
            array (
                'id' => 383,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '482',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 =>
            array (
                'id' => 384,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '442',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 =>
            array (
                'id' => 385,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '443',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 =>
            array (
                'id' => 386,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '444',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 =>
            array (
                'id' => 387,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '445',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 =>
            array (
                'id' => 388,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '446',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 =>
            array (
                'id' => 389,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '447',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 =>
            array (
                'id' => 390,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '448',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 =>
            array (
                'id' => 391,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '449',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 =>
            array (
                'id' => 392,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '450',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 =>
            array (
                'id' => 393,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '451',
                'created_at' => '2017-09-25 22:42:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 =>
            array (
                'id' => 394,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '452',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 =>
            array (
                'id' => 395,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '453',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 =>
            array (
                'id' => 396,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '454',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 =>
            array (
                'id' => 397,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '455',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 =>
            array (
                'id' => 398,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '456',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 =>
            array (
                'id' => 399,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '457',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 =>
            array (
                'id' => 400,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '458',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 =>
            array (
                'id' => 401,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '459',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 =>
            array (
                'id' => 402,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '460',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 =>
            array (
                'id' => 403,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '461',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 =>
            array (
                'id' => 404,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '462',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 =>
            array (
                'id' => 405,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '463',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 =>
            array (
                'id' => 406,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '464',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 =>
            array (
                'id' => 407,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '466',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 =>
            array (
                'id' => 408,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '467',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 =>
            array (
                'id' => 409,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '468',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 =>
            array (
                'id' => 410,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '469',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 =>
            array (
                'id' => 411,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '470',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 =>
            array (
                'id' => 412,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '471',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 =>
            array (
                'id' => 413,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2141',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 =>
            array (
                'id' => 414,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '472',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 =>
            array (
                'id' => 415,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '473',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 =>
            array (
                'id' => 416,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '474',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 =>
            array (
                'id' => 417,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '475',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 =>
            array (
                'id' => 418,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '476',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 =>
            array (
                'id' => 419,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '477',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 =>
            array (
                'id' => 420,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '478',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 =>
            array (
                'id' => 421,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '479',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 =>
            array (
                'id' => 422,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '480',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 =>
            array (
                'id' => 423,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '481',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 =>
            array (
                'id' => 424,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '483',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 =>
            array (
                'id' => 425,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '484',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 =>
            array (
                'id' => 426,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '485',
                'created_at' => '2017-09-25 22:42:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 =>
            array (
                'id' => 427,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '486',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 =>
            array (
                'id' => 428,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '487',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 =>
            array (
                'id' => 429,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '488',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 =>
            array (
                'id' => 430,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '489',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 =>
            array (
                'id' => 431,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '490',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 =>
            array (
                'id' => 432,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '491',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 =>
            array (
                'id' => 433,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2153',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 =>
            array (
                'id' => 434,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2531',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 =>
            array (
                'id' => 435,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '492',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 =>
            array (
                'id' => 436,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2136',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 =>
            array (
                'id' => 437,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2137',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 =>
            array (
                'id' => 438,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2138',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 =>
            array (
                'id' => 439,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2139',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 =>
            array (
                'id' => 440,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2140',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 =>
            array (
                'id' => 441,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2142',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 =>
            array (
                'id' => 442,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2143',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 =>
            array (
                'id' => 443,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2144',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 =>
            array (
                'id' => 444,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2145',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 =>
            array (
                'id' => 445,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2146',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 =>
            array (
                'id' => 446,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2147',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 =>
            array (
                'id' => 447,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2148',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 =>
            array (
                'id' => 448,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2149',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 =>
            array (
                'id' => 449,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2150',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 =>
            array (
                'id' => 450,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2151',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 =>
            array (
                'id' => 451,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2152',
                'created_at' => '2017-09-25 22:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 =>
            array (
                'id' => 452,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2606',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 =>
            array (
                'id' => 453,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2154',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 =>
            array (
                'id' => 454,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2155',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 =>
            array (
                'id' => 455,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2156',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 =>
            array (
                'id' => 456,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2157',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 =>
            array (
                'id' => 457,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2158',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 =>
            array (
                'id' => 458,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2159',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 =>
            array (
                'id' => 459,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2160',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 =>
            array (
                'id' => 460,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2161',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 =>
            array (
                'id' => 461,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2162',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 =>
            array (
                'id' => 462,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2163',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 =>
            array (
                'id' => 463,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2164',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 =>
            array (
                'id' => 464,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2165',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 =>
            array (
                'id' => 465,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2515',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 =>
            array (
                'id' => 466,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2516',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 =>
            array (
                'id' => 467,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2517',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 =>
            array (
                'id' => 468,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2518',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 =>
            array (
                'id' => 469,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2519',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 =>
            array (
                'id' => 470,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2520',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 =>
            array (
                'id' => 471,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2521',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 =>
            array (
                'id' => 472,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2522',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 =>
            array (
                'id' => 473,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2523',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 =>
            array (
                'id' => 474,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2524',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 =>
            array (
                'id' => 475,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2525',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 =>
            array (
                'id' => 476,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2526',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 =>
            array (
                'id' => 477,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2527',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 =>
            array (
                'id' => 478,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2528',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 =>
            array (
                'id' => 479,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2529',
                'created_at' => '2017-09-25 22:42:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 =>
            array (
                'id' => 480,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2530',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 =>
            array (
                'id' => 481,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2532',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 =>
            array (
                'id' => 482,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2533',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 =>
            array (
                'id' => 483,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2534',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 =>
            array (
                'id' => 484,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2535',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 =>
            array (
                'id' => 485,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2536',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 =>
            array (
                'id' => 486,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2537',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 =>
            array (
                'id' => 487,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2538',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 =>
            array (
                'id' => 488,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2539',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 =>
            array (
                'id' => 489,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2540',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 =>
            array (
                'id' => 490,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2541',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 =>
            array (
                'id' => 491,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2542',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 =>
            array (
                'id' => 492,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2543',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 =>
            array (
                'id' => 493,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1768',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 =>
            array (
                'id' => 494,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2544',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 =>
            array (
                'id' => 495,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2545',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 =>
            array (
                'id' => 496,
                // 'id_municipality' => 968,
                'district_id' => 3,
                'identifier' => '2546',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 =>
            array (
                'id' => 497,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1761',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 =>
            array (
                'id' => 498,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1762',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 =>
            array (
                'id' => 499,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1763',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 =>
            array (
                'id' => 500,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1764',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 501,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1765',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 502,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1766',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 503,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1767',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 504,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1800',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 505,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1769',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 506,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1770',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 507,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1771',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 508,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1772',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 509,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1773',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 510,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1774',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 511,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1775',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 512,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1776',
                'created_at' => '2017-09-25 22:42:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 513,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1777',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 514,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1801',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 515,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1778',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 516,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1779',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 517,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1780',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 518,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1781',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 519,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1782',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 520,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1783',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 521,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1784',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 522,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1785',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 523,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1786',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 524,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1787',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 525,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1799',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 526,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1788',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 527,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1789',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 528,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1790',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 529,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1791',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 530,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1792',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 531,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1793',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 532,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1794',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 533,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1795',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 534,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1796',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 535,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1797',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 536,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1798',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 537,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1803',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 538,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1804',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 539,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1805',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 540,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1806',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 541,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1807',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 542,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1808',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 543,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1809',
                'created_at' => '2017-09-25 22:42:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 544,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1810',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 545,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1811',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 546,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1812',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 547,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1813',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 548,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1814',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 549,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1815',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 550,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1816',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 551,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1817',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 552,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1818',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 553,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1819',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 554,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1820',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 555,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1821',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 556,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1822',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 557,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1823',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 558,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1824',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 559,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1825',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 560,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1887',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 561,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1826',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 562,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1827',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 563,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1828',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 564,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1829',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 565,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1830',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 566,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1831',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 567,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1832',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 568,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1833',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 569,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1834',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 570,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1835',
                'created_at' => '2017-09-25 22:42:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 571,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1836',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 572,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1837',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 573,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1838',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 574,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1839',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 575,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1840',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 576,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1841',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 577,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1842',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 578,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1843',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 579,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1844',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 580,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1845',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 581,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1846',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 582,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1847',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 583,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1848',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 584,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1849',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 585,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1850',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 586,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1851',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 587,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1852',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 588,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1853',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 589,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1854',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 590,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1855',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 591,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1856',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 592,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1857',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 593,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1858',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 594,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1859',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 595,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1860',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 596,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1861',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 597,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1862',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 598,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1863',
                'created_at' => '2017-09-25 22:42:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 599,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1864',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 600,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1865',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 601,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1866',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 602,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1867',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 603,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1868',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 604,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1987',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 605,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1869',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 606,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1870',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 607,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1871',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 608,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1872',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 609,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1873',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 610,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1874',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 611,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1875',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 612,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1886',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 613,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1876',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 614,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1877',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 615,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1878',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 616,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1879',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 617,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1880',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 618,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1881',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 619,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1882',
                'created_at' => '2017-09-25 22:42:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 620,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1883',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 621,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1884',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 622,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1885',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 623,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1888',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 624,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1889',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 625,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1890',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 626,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1891',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 627,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1892',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 628,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1893',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 629,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1894',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 630,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1895',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 631,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1896',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 632,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1897',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 633,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1898',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 634,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1899',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 635,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1900',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 636,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1901',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 637,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1902',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 638,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1903',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 639,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1904',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 640,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1905',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 641,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1906',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 642,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1907',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 643,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1908',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 644,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1909',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 645,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1033',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 646,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1910',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 647,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1911',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 648,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1912',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 649,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1913',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 650,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1914',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 651,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1915',
                'created_at' => '2017-09-25 22:42:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 652,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1916',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 653,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1917',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 654,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1918',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 655,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1919',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 656,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1920',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 657,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1921',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 658,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1922',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 659,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1923',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 660,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1924',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 661,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1925',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 662,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1926',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 663,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1927',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 664,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1928',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 665,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1929',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 666,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1930',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 667,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1931',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 668,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1932',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 669,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1933',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 670,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1934',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 671,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1935',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 672,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1936',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 673,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1937',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 674,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1938',
                'created_at' => '2017-09-25 22:42:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 675,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1939',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 676,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1940',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 677,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1941',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 678,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1942',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 679,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1943',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 680,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1944',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 681,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1945',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 682,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1946',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 683,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1947',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 684,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1948',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 685,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1949',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 686,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1446',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 687,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1950',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 688,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1951',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 689,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1952',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 690,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1953',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 691,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1954',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 692,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1955',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 693,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1956',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 694,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1957',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 695,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1958',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 696,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1959',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 697,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1960',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 698,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1961',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 699,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1962',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 700,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1963',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 701,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1964',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 702,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1965',
                'created_at' => '2017-09-25 22:42:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 703,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1966',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 704,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1967',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 705,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1988',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 706,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1968',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 707,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1969',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 708,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1970',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 709,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1971',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 710,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1972',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 711,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1973',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 712,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1974',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 713,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1975',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 714,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1976',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 715,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1977',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 716,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1978',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 717,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1979',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 718,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1980',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 719,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1981',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 720,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1982',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 721,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1983',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 722,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1984',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 723,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1985',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 724,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1986',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 725,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1989',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 726,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1990',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 727,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1991',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 728,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1992',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 729,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1993',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 730,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1994',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 731,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1995',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 732,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1996',
                'created_at' => '2017-09-25 22:42:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 733,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1997',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 734,
                // 'id_municipality' => 994,
                'district_id' => 4,
                'identifier' => '1998',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 735,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '975',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 736,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '976',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 737,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1034',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 738,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '977',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 739,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '978',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 740,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '979',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 741,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '980',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 742,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '981',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 743,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '982',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 744,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '983',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 745,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '984',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 746,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '987',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 747,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1432',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 748,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1433',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 749,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1445',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 750,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1057',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 751,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1434',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 752,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1435',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 753,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1436',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 754,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1437',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 755,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1438',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 756,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1439',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 757,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1440',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 758,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1441',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 759,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1442',
                'created_at' => '2017-09-25 22:42:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 760,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1443',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 761,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1444',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 762,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1447',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 763,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1448',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 764,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1449',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 765,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1450',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 766,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1451',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 767,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1452',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 768,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1453',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 769,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1454',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 770,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1455',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 771,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '994',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 772,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1456',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 773,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1457',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 774,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1458',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 775,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1459',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 776,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1460',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 777,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1461',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 778,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1462',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 779,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1467',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 780,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1468',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 781,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1469',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 782,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1521',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 783,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1470',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 784,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1471',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 785,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1472',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 786,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1473',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 787,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1474',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 788,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1475',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 789,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1476',
                'created_at' => '2017-09-25 22:42:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 790,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1477',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 791,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1478',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 792,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1479',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 793,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1480',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 794,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1075',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 795,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1495',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 796,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1496',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 797,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1497',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 798,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1498',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 799,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1499',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 800,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1500',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 801,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1517',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 802,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1518',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 803,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1519',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 804,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1520',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 805,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1100',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 806,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1522',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 807,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1523',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 808,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1524',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 809,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1525',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 810,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1544',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 811,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1545',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 812,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1546',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 813,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1547',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 814,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1548',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 815,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1549',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 816,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1550',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 817,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1551',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 818,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1552',
                'created_at' => '2017-09-25 22:42:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 819,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1553',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 820,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1554',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 821,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1555',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 822,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1556',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 823,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1588',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 824,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1589',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 825,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1590',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 826,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1591',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 827,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1146',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 828,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1592',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 829,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1593',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 =>
            array (
                'id' => 830,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1612',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 =>
            array (
                'id' => 831,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1613',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 =>
            array (
                'id' => 832,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1614',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 =>
            array (
                'id' => 833,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1615',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 =>
            array (
                'id' => 834,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1616',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 =>
            array (
                'id' => 835,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1617',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 =>
            array (
                'id' => 836,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1618',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 =>
            array (
                'id' => 837,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1619',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 =>
            array (
                'id' => 838,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1620',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 =>
            array (
                'id' => 839,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '1621',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 =>
            array (
                'id' => 840,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2124',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 =>
            array (
                'id' => 841,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2125',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 =>
            array (
                'id' => 842,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2126',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 =>
            array (
                'id' => 843,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2127',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 =>
            array (
                'id' => 844,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2128',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 =>
            array (
                'id' => 845,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2129',
                'created_at' => '2017-09-25 22:42:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 =>
            array (
                'id' => 846,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2130',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 =>
            array (
                'id' => 847,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2131',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 =>
            array (
                'id' => 848,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2132',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 =>
            array (
                'id' => 849,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1147',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 =>
            array (
                'id' => 850,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2133',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 =>
            array (
                'id' => 851,
                // 'id_municipality' => 987,
                'district_id' => 5,
                'identifier' => '2134',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 =>
            array (
                'id' => 852,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '985',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 =>
            array (
                'id' => 853,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '986',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 =>
            array (
                'id' => 854,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '988',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 =>
            array (
                'id' => 855,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '989',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 =>
            array (
                'id' => 856,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '990',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 =>
            array (
                'id' => 857,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '991',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 =>
            array (
                'id' => 858,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '992',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 =>
            array (
                'id' => 859,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '993',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 =>
            array (
                'id' => 860,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '995',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 =>
            array (
                'id' => 861,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '996',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 =>
            array (
                'id' => 862,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '997',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 =>
            array (
                'id' => 863,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '998',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 =>
            array (
                'id' => 864,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '999',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 =>
            array (
                'id' => 865,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1000',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 =>
            array (
                'id' => 866,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1001',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 =>
            array (
                'id' => 867,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1002',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 =>
            array (
                'id' => 868,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1003',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 =>
            array (
                'id' => 869,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1004',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 =>
            array (
                'id' => 870,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1005',
                'created_at' => '2017-09-25 22:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 =>
            array (
                'id' => 871,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1246',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 =>
            array (
                'id' => 872,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1006',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 =>
            array (
                'id' => 873,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1007',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 =>
            array (
                'id' => 874,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1008',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 =>
            array (
                'id' => 875,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1009',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 =>
            array (
                'id' => 876,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1010',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 =>
            array (
                'id' => 877,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1011',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 =>
            array (
                'id' => 878,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1012',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 =>
            array (
                'id' => 879,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1013',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 =>
            array (
                'id' => 880,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1014',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 =>
            array (
                'id' => 881,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1015',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 =>
            array (
                'id' => 882,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1032',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 =>
            array (
                'id' => 883,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1016',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 =>
            array (
                'id' => 884,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1017',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 =>
            array (
                'id' => 885,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1018',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 =>
            array (
                'id' => 886,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1019',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 =>
            array (
                'id' => 887,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1020',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 =>
            array (
                'id' => 888,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1021',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 =>
            array (
                'id' => 889,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1022',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 =>
            array (
                'id' => 890,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1023',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 =>
            array (
                'id' => 891,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1024',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 =>
            array (
                'id' => 892,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1025',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 =>
            array (
                'id' => 893,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1026',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 =>
            array (
                'id' => 894,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1027',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 =>
            array (
                'id' => 895,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1028',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 =>
            array (
                'id' => 896,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1029',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 =>
            array (
                'id' => 897,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1030',
                'created_at' => '2017-09-25 22:42:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 =>
            array (
                'id' => 898,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1031',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 =>
            array (
                'id' => 899,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1035',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 =>
            array (
                'id' => 900,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1036',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 =>
            array (
                'id' => 901,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1037',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 =>
            array (
                'id' => 902,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1038',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 =>
            array (
                'id' => 903,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1039',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 =>
            array (
                'id' => 904,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1040',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 =>
            array (
                'id' => 905,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1041',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 =>
            array (
                'id' => 906,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1042',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 =>
            array (
                'id' => 907,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1043',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 =>
            array (
                'id' => 908,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1044',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 =>
            array (
                'id' => 909,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1284',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 =>
            array (
                'id' => 910,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1045',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 =>
            array (
                'id' => 911,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1046',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 =>
            array (
                'id' => 912,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1047',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 =>
            array (
                'id' => 913,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1048',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 =>
            array (
                'id' => 914,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1049',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 =>
            array (
                'id' => 915,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1050',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 =>
            array (
                'id' => 916,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1051',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 =>
            array (
                'id' => 917,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1052',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 =>
            array (
                'id' => 918,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1053',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 =>
            array (
                'id' => 919,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1054',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 =>
            array (
                'id' => 920,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1055',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 =>
            array (
                'id' => 921,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1056',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 =>
            array (
                'id' => 922,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1058',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 =>
            array (
                'id' => 923,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1059',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 =>
            array (
                'id' => 924,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1060',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 =>
            array (
                'id' => 925,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1061',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 =>
            array (
                'id' => 926,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1062',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 =>
            array (
                'id' => 927,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1063',
                'created_at' => '2017-09-25 22:42:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 =>
            array (
                'id' => 928,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1064',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 =>
            array (
                'id' => 929,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1065',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 =>
            array (
                'id' => 930,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1066',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 =>
            array (
                'id' => 931,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1067',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 =>
            array (
                'id' => 932,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1068',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 =>
            array (
                'id' => 933,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1069',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 =>
            array (
                'id' => 934,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1070',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 =>
            array (
                'id' => 935,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1071',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 =>
            array (
                'id' => 936,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1072',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 =>
            array (
                'id' => 937,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1073',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 =>
            array (
                'id' => 938,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1074',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 =>
            array (
                'id' => 939,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1076',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 =>
            array (
                'id' => 940,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1077',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 =>
            array (
                'id' => 941,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1078',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 =>
            array (
                'id' => 942,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1079',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 =>
            array (
                'id' => 943,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1080',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 =>
            array (
                'id' => 944,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1081',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 =>
            array (
                'id' => 945,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1082',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 =>
            array (
                'id' => 946,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1083',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 =>
            array (
                'id' => 947,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1084',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 =>
            array (
                'id' => 948,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1085',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 =>
            array (
                'id' => 949,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1086',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 =>
            array (
                'id' => 950,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1087',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 =>
            array (
                'id' => 951,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1485',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 =>
            array (
                'id' => 952,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1088',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 =>
            array (
                'id' => 953,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1089',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 =>
            array (
                'id' => 954,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1090',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 =>
            array (
                'id' => 955,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1091',
                'created_at' => '2017-09-25 22:42:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 =>
            array (
                'id' => 956,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1092',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 =>
            array (
                'id' => 957,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1093',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 =>
            array (
                'id' => 958,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1094',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 =>
            array (
                'id' => 959,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1095',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 =>
            array (
                'id' => 960,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1096',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 =>
            array (
                'id' => 961,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1097',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 =>
            array (
                'id' => 962,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1098',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 =>
            array (
                'id' => 963,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1099',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 =>
            array (
                'id' => 964,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1101',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 =>
            array (
                'id' => 965,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1102',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 =>
            array (
                'id' => 966,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1103',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 =>
            array (
                'id' => 967,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1104',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 =>
            array (
                'id' => 968,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1105',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 =>
            array (
                'id' => 969,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1106',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 =>
            array (
                'id' => 970,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1107',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 =>
            array (
                'id' => 971,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1108',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 =>
            array (
                'id' => 972,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1109',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 =>
            array (
                'id' => 973,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1110',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 =>
            array (
                'id' => 974,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1111',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 =>
            array (
                'id' => 975,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1112',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 =>
            array (
                'id' => 976,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1113',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 =>
            array (
                'id' => 977,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1114',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 =>
            array (
                'id' => 978,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1115',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 =>
            array (
                'id' => 979,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1116',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 =>
            array (
                'id' => 980,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1117',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 =>
            array (
                'id' => 981,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1118',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 =>
            array (
                'id' => 982,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1119',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 =>
            array (
                'id' => 983,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1120',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 =>
            array (
                'id' => 984,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1121',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 =>
            array (
                'id' => 985,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1127',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 =>
            array (
                'id' => 986,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1128',
                'created_at' => '2017-09-25 22:42:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 =>
            array (
                'id' => 987,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1129',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 =>
            array (
                'id' => 988,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1130',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 =>
            array (
                'id' => 989,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1131',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 =>
            array (
                'id' => 990,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1140',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 =>
            array (
                'id' => 991,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1141',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 =>
            array (
                'id' => 992,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1463',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 =>
            array (
                'id' => 993,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1464',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 =>
            array (
                'id' => 994,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1465',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 =>
            array (
                'id' => 995,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1466',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 =>
            array (
                'id' => 996,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1481',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 =>
            array (
                'id' => 997,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1482',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 =>
            array (
                'id' => 998,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1483',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 =>
            array (
                'id' => 999,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1484',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 =>
            array (
                'id' => 1000,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1486',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 1001,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1487',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 1002,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1488',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 1003,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1489',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 1004,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1490',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 1005,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1491',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 1006,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1492',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 1007,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1493',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 1008,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1516',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 1009,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1494',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 1010,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1501',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 1011,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1502',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 1012,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1503',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 1013,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1504',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 1014,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1505',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 1015,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1506',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 1016,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1514',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 1017,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1515',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 1018,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1507',
                'created_at' => '2017-09-25 22:42:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 1019,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1508',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 1020,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1509',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 1021,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1510',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 1022,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1511',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 1023,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1512',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 1024,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1513',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 1025,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1558',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 1026,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1526',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 1027,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1527',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 1028,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1528',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 1029,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1529',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 1030,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1530',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 1031,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1531',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 1032,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1532',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 1033,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1533',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 1034,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1534',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 1035,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1559',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 1036,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1584',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 1037,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1535',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 1038,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1536',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 1039,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1537',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 1040,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1538',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 1041,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1539',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 1042,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1540',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 1043,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1541',
                'created_at' => '2017-09-25 22:42:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 1044,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1542',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 1045,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1543',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 1046,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1557',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 1047,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1560',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 1048,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1561',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 1049,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1562',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 1050,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1563',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 1051,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1564',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 1052,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1565',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 1053,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1566',
                'created_at' => '2017-09-25 22:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 1054,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1567',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 1055,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1568',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 1056,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1569',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 1057,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1570',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 1058,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1571',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 1059,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1572',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 1060,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1573',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 1061,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1574',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 1062,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1575',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 1063,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1576',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 1064,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1577',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 1065,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1578',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 1066,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1579',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 1067,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1580',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 1068,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1581',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 1069,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1582',
                'created_at' => '2017-09-25 22:42:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 1070,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1583',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 1071,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1585',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 1072,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1586',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 1073,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1587',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 1074,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1594',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 1075,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1595',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 1076,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1596',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 1077,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1597',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 1078,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1598',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 1079,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1599',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 1080,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1600',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 1081,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1601',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 1082,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1602',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 1083,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1603',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 1084,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1604',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 1085,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1605',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 1086,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1606',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 1087,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1607',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 1088,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1608',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 1089,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1609',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 1090,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1610',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 1091,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1611',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 1092,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1622',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 1093,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1623',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 1094,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1624',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 1095,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1625',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 1096,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1626',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 1097,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1627',
                'created_at' => '2017-09-25 22:42:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 1098,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1628',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 1099,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1629',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 1100,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1630',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 1101,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1631',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 1102,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1632',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 1103,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1633',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 1104,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1634',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 1105,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1635',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 1106,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1636',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 1107,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1637',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 1108,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1638',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 1109,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1639',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 1110,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1643',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 1111,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1644',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 1112,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1645',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 1113,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1646',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 1114,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1647',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 1115,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1648',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 1116,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '1649',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 1117,
                // 'id_municipality' => 987,
                'district_id' => 6,
                'identifier' => '2135',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 1118,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '43',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 1119,
                // 'id_municipality' => 948,
                'district_id' => 7,
                'identifier' => '1',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 1120,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '9',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 1121,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '10',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 1122,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '44',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 1123,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1148',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 1124,
                // 'id_municipality' => 948,
                'district_id' => 7,
                'identifier' => '2',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 1125,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '39',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 1126,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '40',
                'created_at' => '2017-09-25 22:42:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 1127,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '41',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 1128,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2607',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 1129,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '3',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 1130,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '4',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 1131,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '42',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 1132,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '45',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 1133,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '5',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 1134,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '6',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 1135,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '7',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 1136,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '8',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 1137,
                // 'id_municipality' => 949,
                'district_id' => 7,
                'identifier' => '11',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 1138,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '46',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 1139,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '47',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 1140,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '56',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 1141,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '355',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 1142,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1743',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 1143,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '48',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 1144,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '49',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 1145,
                // 'id_municipality' => 955,
                'district_id' => 7,
                'identifier' => '168',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 1146,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '51',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 1147,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '52',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 1148,
                // 'id_municipality' => 955,
                'district_id' => 7,
                'identifier' => '171',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 1149,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1149',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 1150,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '54',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 1151,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '816',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 1152,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '55',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 1153,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2561',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 1154,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '57',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 1155,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '59',
                'created_at' => '2017-09-25 22:42:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 1156,
                // 'id_municipality' => 959,
                'district_id' => 7,
                'identifier' => '233',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 1157,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '61',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 1158,
                // 'id_municipality' => 952,
                'district_id' => 7,
                'identifier' => '62',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 1159,
                // 'id_municipality' => 955,
                'district_id' => 7,
                'identifier' => '169',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 1160,
                // 'id_municipality' => 955,
                'district_id' => 7,
                'identifier' => '170',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 1161,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1150',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 1162,
                // 'id_municipality' => 957,
                'district_id' => 7,
                'identifier' => '218',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 1163,
                // 'id_municipality' => 957,
                'district_id' => 7,
                'identifier' => '219',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 1164,
                // 'id_municipality' => 959,
                'district_id' => 7,
                'identifier' => '234',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 1165,
                // 'id_municipality' => 957,
                'district_id' => 7,
                'identifier' => '220',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 1166,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '348',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 1167,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '349',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 1168,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1151',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 1169,
                // 'id_municipality' => 959,
                'district_id' => 7,
                'identifier' => '232',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 1170,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '670',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 1171,
                // 'id_municipality' => 959,
                'district_id' => 7,
                'identifier' => '235',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 1172,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '350',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 1173,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2293',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 1174,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1152',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 1175,
                // 'id_municipality' => 959,
                'district_id' => 7,
                'identifier' => '236',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 1176,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '346',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 1177,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '347',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 1178,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2608',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 1179,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '352',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 1180,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '354',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 1181,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2294',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 1182,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '523',
                'created_at' => '2017-09-25 22:42:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 1183,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '524',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 1184,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2295',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 1185,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2609',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 1186,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '811',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 1187,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '812',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 1188,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '817',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 1189,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '818',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 1190,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2296',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 1191,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '813',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 1192,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '814',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 1193,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '815',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 1194,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2297',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 1195,
                // 'id_municipality' => 975,
                'district_id' => 7,
                'identifier' => '819',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 1196,
                // 'id_municipality' => 976,
                'district_id' => 7,
                'identifier' => '820',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 1197,
                // 'id_municipality' => 980,
                'district_id' => 7,
                'identifier' => '855',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 1198,
                // 'id_municipality' => 980,
                'district_id' => 7,
                'identifier' => '856',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 1199,
                // 'id_municipality' => 980,
                'district_id' => 7,
                'identifier' => '857',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 1200,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2298',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 1201,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2299',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 1202,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1285',
                'created_at' => '2017-09-25 22:42:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 1203,
                // 'id_municipality' => 976,
                'district_id' => 7,
                'identifier' => '821',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 1204,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1731',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 1205,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1732',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 1206,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2300',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 1207,
                // 'id_municipality' => 980,
                'district_id' => 7,
                'identifier' => '854',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 1208,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2301',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 1209,
                // 'id_municipality' => 980,
                'district_id' => 7,
                'identifier' => '858',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 1210,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2302',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 1211,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1153',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 1212,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '925',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 1213,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '927',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 1214,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '928',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 1215,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1737',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 1216,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1738',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 1217,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '930',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 1218,
                // 'id_municipality' => 988,
                'district_id' => 7,
                'identifier' => '1700',
                'created_at' => '2017-09-25 22:42:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 1219,
                // 'id_municipality' => 988,
                'district_id' => 7,
                'identifier' => '1701',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 1220,
                // 'id_municipality' => 988,
                'district_id' => 7,
                'identifier' => '1702',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 1221,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1730',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 1222,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1733',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 1223,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1739',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 1224,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1740',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 1225,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1741',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 1226,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2303',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 1227,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1734',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 1228,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1735',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 1229,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1736',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 1230,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1742',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 1231,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1744',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 1232,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1745',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 1233,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1746',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 1234,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1760',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 1235,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2111',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 1236,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2112',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 1237,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2113',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 1238,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2304',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 1239,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2562',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 1240,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1749',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 1241,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1750',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 1242,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '1751',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 1243,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2118',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 1244,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1752',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 1245,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1753',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 1246,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1754',
                'created_at' => '2017-09-25 22:42:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 1247,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '2414',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 1248,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1755',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 1249,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1756',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 1250,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1757',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 1251,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1758',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 1252,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2563',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 1253,
                // 'id_municipality' => 993,
                'district_id' => 7,
                'identifier' => '1759',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 1254,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2548',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 1255,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2564',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 1256,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2114',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 1257,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2565',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 1258,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2566',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 1259,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2115',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 1260,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2120',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 1261,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2549',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 1262,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2550',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 1263,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2116',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 1264,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2280',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 1265,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2281',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 1266,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2282',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 1267,
                // 'id_municipality' => 997,
                'district_id' => 7,
                'identifier' => '2117',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 1268,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2283',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 1269,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2284',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 1270,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2285',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 1271,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2286',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 1272,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2119',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 1273,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2287',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 1274,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2551',
                'created_at' => '2017-09-25 22:42:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 1275,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2121',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 1276,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2288',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 1277,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2289',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 1278,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2122',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 1279,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2290',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 1280,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2291',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 1281,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2292',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 1282,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2567',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 1283,
                // 'id_municipality' => 998,
                'district_id' => 7,
                'identifier' => '2123',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 1284,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2274',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 1285,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2275',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 1286,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2276',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 1287,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2277',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 1288,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2278',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 1289,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2279',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 1290,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2305',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 1291,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2306',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 1292,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2552',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 1293,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2553',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 1294,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2307',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 1295,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '2408',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 1296,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '2409',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 1297,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2554',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 1298,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2555',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 1299,
                // 'id_municipality' => 985,
                'district_id' => 7,
                'identifier' => '2410',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 1300,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2556',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 1301,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2557',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 1302,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2558',
                'created_at' => '2017-09-25 22:42:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 1303,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2559',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 1304,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2560',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 1305,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2568',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 1306,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '2411',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 1307,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '2412',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 1308,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '2413',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 1309,
                // 'id_municipality' => 992,
                'district_id' => 7,
                'identifier' => '2415',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 1310,
                // 'id_municipality' => 957,
                'district_id' => 7,
                'identifier' => '2437',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 1311,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2547',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 1312,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2569',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 1313,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2570',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 1314,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2571',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 1315,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2572',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 1316,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2573',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 1317,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2574',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 1318,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2575',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 1319,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2576',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 1320,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2577',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 1321,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2578',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 1322,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2579',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 1323,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2580',
                'created_at' => '2017-09-25 22:42:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 1324,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2581',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 1325,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2583',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 1326,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2584',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 1327,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2585',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 1328,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2586',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 1329,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2587',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 =>
            array (
                'id' => 1330,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2588',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 =>
            array (
                'id' => 1331,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2589',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 =>
            array (
                'id' => 1332,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2590',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 =>
            array (
                'id' => 1333,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2591',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 =>
            array (
                'id' => 1334,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2592',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 =>
            array (
                'id' => 1335,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2593',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 =>
            array (
                'id' => 1336,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2594',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 =>
            array (
                'id' => 1337,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2595',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 =>
            array (
                'id' => 1338,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2596',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 =>
            array (
                'id' => 1339,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2597',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 =>
            array (
                'id' => 1340,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2598',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 =>
            array (
                'id' => 1341,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2599',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 =>
            array (
                'id' => 1342,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2600',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 =>
            array (
                'id' => 1343,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2601',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 =>
            array (
                'id' => 1344,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2602',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 =>
            array (
                'id' => 1345,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2603',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 =>
            array (
                'id' => 1346,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2604',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 =>
            array (
                'id' => 1347,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2605',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 =>
            array (
                'id' => 1348,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2610',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 =>
            array (
                'id' => 1349,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2611',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 =>
            array (
                'id' => 1350,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2612',
                'created_at' => '2017-09-25 22:42:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 =>
            array (
                'id' => 1351,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2613',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 =>
            array (
                'id' => 1352,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2614',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 =>
            array (
                'id' => 1353,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2615',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 =>
            array (
                'id' => 1354,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2616',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 =>
            array (
                'id' => 1355,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2617',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 =>
            array (
                'id' => 1356,
                // 'id_municipality' => 965,
                'district_id' => 7,
                'identifier' => '2618',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 =>
            array (
                'id' => 1357,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2626',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 =>
            array (
                'id' => 1358,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '572',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 =>
            array (
                'id' => 1359,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2619',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 =>
            array (
                'id' => 1360,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2620',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 =>
            array (
                'id' => 1361,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2621',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 =>
            array (
                'id' => 1362,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2622',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 =>
            array (
                'id' => 1363,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2623',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 =>
            array (
                'id' => 1364,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2624',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 =>
            array (
                'id' => 1365,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2625',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 =>
            array (
                'id' => 1366,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2637',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 =>
            array (
                'id' => 1367,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2627',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 =>
            array (
                'id' => 1368,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2628',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 =>
            array (
                'id' => 1369,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2629',
                'created_at' => '2017-09-25 22:42:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 =>
            array (
                'id' => 1370,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2630',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 =>
            array (
                'id' => 1371,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2631',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 =>
            array (
                'id' => 1372,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2632',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 =>
            array (
                'id' => 1373,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2633',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 =>
            array (
                'id' => 1374,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2634',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 =>
            array (
                'id' => 1375,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2635',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 =>
            array (
                'id' => 1376,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2636',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 =>
            array (
                'id' => 1377,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2639',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 =>
            array (
                'id' => 1378,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2640',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 =>
            array (
                'id' => 1379,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2641',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 =>
            array (
                'id' => 1380,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2642',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 =>
            array (
                'id' => 1381,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2643',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 =>
            array (
                'id' => 1382,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2644',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 =>
            array (
                'id' => 1383,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2645',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 =>
            array (
                'id' => 1384,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2646',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 =>
            array (
                'id' => 1385,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2647',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 =>
            array (
                'id' => 1386,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2648',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 =>
            array (
                'id' => 1387,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2649',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 =>
            array (
                'id' => 1388,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1286',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 =>
            array (
                'id' => 1389,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2650',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 =>
            array (
                'id' => 1390,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2651',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 =>
            array (
                'id' => 1391,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2652',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 =>
            array (
                'id' => 1392,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2653',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 =>
            array (
                'id' => 1393,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2654',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 =>
            array (
                'id' => 1394,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2655',
                'created_at' => '2017-09-25 22:42:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 =>
            array (
                'id' => 1395,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2656',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 =>
            array (
                'id' => 1396,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2657',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 =>
            array (
                'id' => 1397,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2658',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 =>
            array (
                'id' => 1398,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2659',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 =>
            array (
                'id' => 1399,
                // 'id_municipality' => 972,
                'district_id' => 7,
                'identifier' => '2660',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 =>
            array (
                'id' => 1400,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '605',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 =>
            array (
                'id' => 1401,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '141',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 =>
            array (
                'id' => 1402,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '142',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 =>
            array (
                'id' => 1403,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '143',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 =>
            array (
                'id' => 1404,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '144',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 =>
            array (
                'id' => 1405,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '606',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 =>
            array (
                'id' => 1406,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '616',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 =>
            array (
                'id' => 1407,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '145',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 =>
            array (
                'id' => 1408,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '146',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 =>
            array (
                'id' => 1409,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '147',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 =>
            array (
                'id' => 1410,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '148',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 =>
            array (
                'id' => 1411,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '604',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 =>
            array (
                'id' => 1412,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1301',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 =>
            array (
                'id' => 1413,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '607',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 =>
            array (
                'id' => 1414,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '608',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 =>
            array (
                'id' => 1415,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '609',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 =>
            array (
                'id' => 1416,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '610',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 =>
            array (
                'id' => 1417,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '611',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 =>
            array (
                'id' => 1418,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '612',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 =>
            array (
                'id' => 1419,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '613',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 =>
            array (
                'id' => 1420,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '614',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 =>
            array (
                'id' => 1421,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '615',
                'created_at' => '2017-09-25 22:42:42',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 =>
            array (
                'id' => 1422,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '617',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 =>
            array (
                'id' => 1423,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '618',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 =>
            array (
                'id' => 1424,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '619',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 =>
            array (
                'id' => 1425,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '620',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 =>
            array (
                'id' => 1426,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '621',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 =>
            array (
                'id' => 1427,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '628',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 =>
            array (
                'id' => 1428,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1302',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 =>
            array (
                'id' => 1429,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '622',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 =>
            array (
                'id' => 1430,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '623',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 =>
            array (
                'id' => 1431,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '624',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 =>
            array (
                'id' => 1432,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '625',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 =>
            array (
                'id' => 1433,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '626',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 =>
            array (
                'id' => 1434,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '627',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 =>
            array (
                'id' => 1435,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1303',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 =>
            array (
                'id' => 1436,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '639',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 =>
            array (
                'id' => 1437,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '640',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 =>
            array (
                'id' => 1438,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '641',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 =>
            array (
                'id' => 1439,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '642',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 =>
            array (
                'id' => 1440,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '643',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 =>
            array (
                'id' => 1441,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '644',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 =>
            array (
                'id' => 1442,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '645',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 =>
            array (
                'id' => 1443,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '646',
                'created_at' => '2017-09-25 22:42:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 =>
            array (
                'id' => 1444,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '668',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 =>
            array (
                'id' => 1445,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '669',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 =>
            array (
                'id' => 1446,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1304',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 =>
            array (
                'id' => 1447,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '647',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 =>
            array (
                'id' => 1448,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '649',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 =>
            array (
                'id' => 1449,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '650',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 =>
            array (
                'id' => 1450,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '651',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 =>
            array (
                'id' => 1451,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '652',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 =>
            array (
                'id' => 1452,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '653',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 =>
            array (
                'id' => 1453,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '573',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 =>
            array (
                'id' => 1454,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '671',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 =>
            array (
                'id' => 1455,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '672',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 =>
            array (
                'id' => 1456,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '673',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 =>
            array (
                'id' => 1457,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '674',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 =>
            array (
                'id' => 1458,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '675',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 =>
            array (
                'id' => 1459,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '676',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 =>
            array (
                'id' => 1460,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '677',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 =>
            array (
                'id' => 1461,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '692',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 =>
            array (
                'id' => 1462,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '678',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 =>
            array (
                'id' => 1463,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '679',
                'created_at' => '2017-09-25 22:42:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 =>
            array (
                'id' => 1464,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '680',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 =>
            array (
                'id' => 1465,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '681',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 =>
            array (
                'id' => 1466,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '682',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 =>
            array (
                'id' => 1467,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '683',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 =>
            array (
                'id' => 1468,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '688',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 =>
            array (
                'id' => 1469,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '689',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 =>
            array (
                'id' => 1470,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '690',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 =>
            array (
                'id' => 1471,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '691',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 =>
            array (
                'id' => 1472,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2708',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 =>
            array (
                'id' => 1473,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '693',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 =>
            array (
                'id' => 1474,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '694',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 =>
            array (
                'id' => 1475,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '697',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 =>
            array (
                'id' => 1476,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '698',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 =>
            array (
                'id' => 1477,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '699',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 =>
            array (
                'id' => 1478,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '700',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 =>
            array (
                'id' => 1479,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '701',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 =>
            array (
                'id' => 1480,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '702',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 =>
            array (
                'id' => 1481,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '703',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 =>
            array (
                'id' => 1482,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '704',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 =>
            array (
                'id' => 1483,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '705',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 =>
            array (
                'id' => 1484,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '706',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 =>
            array (
                'id' => 1485,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '707',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 =>
            array (
                'id' => 1486,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '574',
                'created_at' => '2017-09-25 22:42:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 =>
            array (
                'id' => 1487,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '708',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 =>
            array (
                'id' => 1488,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '709',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 =>
            array (
                'id' => 1489,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '741',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 =>
            array (
                'id' => 1490,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '742',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 =>
            array (
                'id' => 1491,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '743',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 =>
            array (
                'id' => 1492,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2212',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 =>
            array (
                'id' => 1493,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2213',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 =>
            array (
                'id' => 1494,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '575',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 =>
            array (
                'id' => 1495,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2214',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 =>
            array (
                'id' => 1496,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2215',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 =>
            array (
                'id' => 1497,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2216',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 =>
            array (
                'id' => 1498,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2217',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 =>
            array (
                'id' => 1499,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2218',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 =>
            array (
                'id' => 1500,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2219',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 1501,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2220',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 1502,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2221',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 1503,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2222',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 1504,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2223',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 1505,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2224',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 1506,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2225',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 1507,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2226',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 1508,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2227',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 1509,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2228',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 1510,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2229',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 1511,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2230',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 1512,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2231',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 1513,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2232',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 1514,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2233',
                'created_at' => '2017-09-25 22:42:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 1515,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2234',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 1516,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2235',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 1517,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2236',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 1518,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2237',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 1519,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2238',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 1520,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2239',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 1521,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2240',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 1522,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2241',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 1523,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2242',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 1524,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2243',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 1525,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2244',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 1526,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2709',
                'created_at' => '2017-09-25 22:42:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 1527,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2245',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 1528,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2246',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 1529,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2247',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 1530,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2248',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 1531,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2249',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 1532,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2250',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 1533,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2251',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 1534,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2252',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 1535,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2253',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 1536,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2254',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 1537,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2255',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 1538,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2256',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 1539,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2257',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 1540,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2258',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 1541,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2259',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 1542,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2260',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 1543,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2261',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 1544,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2262',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 1545,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2263',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 1546,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2264',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 1547,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2265',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 1548,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2266',
                'created_at' => '2017-09-25 22:42:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 1549,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2267',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 1550,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1154',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 1551,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2268',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 1552,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2269',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 1553,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2270',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 1554,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2271',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 1555,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2272',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 1556,
                // 'id_municipality' => 953,
                'district_id' => 8,
                'identifier' => '2273',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 1557,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2695',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 1558,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '12',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 1559,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '13',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 1560,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2696',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 1561,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2697',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 1562,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2698',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 1563,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2699',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 1564,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '26',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 1565,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '27',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 1566,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2700',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 1567,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2701',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 1568,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2702',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 1569,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2703',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 1570,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2704',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 1571,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2705',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 1572,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2706',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 1573,
                // 'id_municipality' => 973,
                'district_id' => 8,
                'identifier' => '2707',
                'created_at' => '2017-09-25 22:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 1574,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '15',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 1575,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '18',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 1576,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '16',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 1577,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '17',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 1578,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '23',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 1579,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '24',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 1580,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '25',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 1581,
                // 'id_municipality' => 950,
                'district_id' => 9,
                'identifier' => '19',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 1582,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '20',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 1583,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '21',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 1584,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '22',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 1585,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '28',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 1586,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '29',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 1587,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '30',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 1588,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '31',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 1589,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1155',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 1590,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1156',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 1591,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1157',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 1592,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '32',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 1593,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '33',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 1594,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '157',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 1595,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '946',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 1596,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '34',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 1597,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '35',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 1598,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '36',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 1599,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '37',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 1600,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1158',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 1601,
                // 'id_municipality' => 951,
                'district_id' => 9,
                'identifier' => '38',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 1602,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '222',
                'created_at' => '2017-09-25 22:42:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 1603,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '151',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 1604,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '223',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 1605,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '224',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 1606,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '950',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 1607,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '152',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 1608,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '153',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 1609,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '245',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 1610,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '825',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 1611,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '154',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 1612,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '155',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 1613,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '156',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 1614,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '256',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 1615,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '158',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 1616,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '883',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 1617,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '159',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 1618,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '884',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 1619,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '160',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 1620,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '263',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 1621,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '161',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 1622,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '162',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 1623,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '163',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 1624,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '281',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 1625,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '164',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 1626,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '286',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 1627,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1159',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 1628,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '165',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 1629,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '166',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 1630,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '885',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 1631,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1160',
                'created_at' => '2017-09-25 22:42:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 1632,
                // 'id_municipality' => 954,
                'district_id' => 9,
                'identifier' => '167',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 1633,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '221',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 1634,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '303',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 1635,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '225',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 1636,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '226',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 1637,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '262',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 1638,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '227',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 1639,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '886',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 1640,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1161',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 1641,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '228',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 1642,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '230',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 1643,
                // 'id_municipality' => 958,
                'district_id' => 9,
                'identifier' => '231',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 1644,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '237',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 1645,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '951',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 1646,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '238',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 1647,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '239',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 1648,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '240',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 1649,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '241',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 1650,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '242',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 1651,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '243',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 1652,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '244',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 1653,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '246',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 1654,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '247',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 1655,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1162',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 1656,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1163',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 1657,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1164',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 1658,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '252',
                'created_at' => '2017-09-25 22:42:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 1659,
                // 'id_municipality' => 960,
                'district_id' => 9,
                'identifier' => '253',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 1660,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '257',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 1661,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '258',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 1662,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '259',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 1663,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '260',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 1664,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '261',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 1665,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '264',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 1666,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '272',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 1667,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '265',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 1668,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1165',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 1669,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '266',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 1670,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '273',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 1671,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '267',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 1672,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '274',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 1673,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1166',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 1674,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '268',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 1675,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '269',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 1676,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1167',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 1677,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1168',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 1678,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1169',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 1679,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '270',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 1680,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '271',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 1681,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1170',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 1682,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1171',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 1683,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '275',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 1684,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '278',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 1685,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1172',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 1686,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1173',
                'created_at' => '2017-09-25 22:42:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 1687,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '276',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 1688,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '277',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 1689,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '279',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 1690,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '280',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 1691,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '931',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 1692,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '282',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 1693,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '283',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 1694,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '284',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 1695,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '285',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 1696,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1174',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 1697,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '287',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 1698,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '288',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 1699,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '294',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 1700,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1175',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 1701,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '289',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 1702,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '290',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 1703,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '932',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 1704,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '291',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 1705,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '296',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 1706,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '292',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 1707,
                // 'id_municipality' => 961,
                'district_id' => 9,
                'identifier' => '293',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 1708,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '933',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 1709,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1176',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 1710,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '295',
                'created_at' => '2017-09-25 22:42:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 1711,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '299',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 1712,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '934',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 1713,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1177',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 1714,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '297',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 1715,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '298',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 1716,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1178',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 1717,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1179',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 1718,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '300',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 1719,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '301',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 1720,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1180',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 1721,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1181',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 1722,
                // 'id_municipality' => 962,
                'district_id' => 9,
                'identifier' => '304',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 1723,
                // 'id_municipality' => 963,
                'district_id' => 9,
                'identifier' => '305',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 1724,
                // 'id_municipality' => 963,
                'district_id' => 9,
                'identifier' => '306',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 1725,
                // 'id_municipality' => 963,
                'district_id' => 9,
                'identifier' => '307',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 1726,
                // 'id_municipality' => 963,
                'district_id' => 9,
                'identifier' => '308',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 1727,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '309',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 1728,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '887',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 1729,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1182',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 1730,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '310',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 1731,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '311',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 1732,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '315',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 1733,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '316',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 1734,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '317',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 1735,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '892',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 1736,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '312',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 1737,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '322',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 1738,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '323',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 1739,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '893',
                'created_at' => '2017-09-25 22:42:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 1740,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '894',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 1741,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1183',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 1742,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1184',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 1743,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1185',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 1744,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1186',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 1745,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '313',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 1746,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '333',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 1747,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '334',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 1748,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '335',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 1749,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1187',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 1750,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '314',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 1751,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '935',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 1752,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '318',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 1753,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '339',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 1754,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '319',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 1755,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '340',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 1756,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '495',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 1757,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1188',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 1758,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '320',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 1759,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '321',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 1760,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1189',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 1761,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '324',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 1762,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '325',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 1763,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '326',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 1764,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1190',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 1765,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '327',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 1766,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '328',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 1767,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '329',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 1768,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '330',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 1769,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1191',
                'created_at' => '2017-09-25 22:42:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 1770,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '331',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 1771,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '421',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 1772,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '422',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 1773,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '496',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 1774,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '332',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 1775,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '336',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 1776,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '936',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 1777,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1192',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 1778,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '337',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 1779,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '494',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 1780,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '826',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 1781,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '338',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 1782,
                // 'id_municipality' => 978,
                'district_id' => 9,
                'identifier' => '832',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 1783,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1193',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 1784,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '341',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 1785,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '342',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 1786,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '937',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 1787,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1194',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 1788,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '343',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 1789,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '939',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 1790,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '344',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 1791,
                // 'id_municipality' => 964,
                'district_id' => 9,
                'identifier' => '345',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 1792,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '863',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 1793,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '418',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 1794,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '419',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 1795,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '420',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 1796,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2314',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 1797,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '423',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 1798,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '424',
                'created_at' => '2017-09-25 22:42:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 1799,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '864',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 1800,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1195',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 1801,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '426',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 1802,
                // 'id_municipality' => 967,
                'district_id' => 9,
                'identifier' => '427',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 1803,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '940',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 1804,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '497',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 1805,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '504',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 1806,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '505',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 1807,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1196',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 1808,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '498',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 1809,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '499',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 1810,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1197',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 1811,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2315',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 1812,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '502',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 1813,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '503',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 1814,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '865',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 1815,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '506',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 1816,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '507',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 1817,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '508',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 1818,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '509',
                'created_at' => '2017-09-25 22:42:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 1819,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '866',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 1820,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '867',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 1821,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '868',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 1822,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '510',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 1823,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '511',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 1824,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '512',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 1825,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '513',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 1826,
                // 'id_municipality' => 969,
                'district_id' => 9,
                'identifier' => '514',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 1827,
                // 'id_municipality' => 970,
                'district_id' => 9,
                'identifier' => '515',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 1828,
                // 'id_municipality' => 970,
                'district_id' => 9,
                'identifier' => '516',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 1829,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '869',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 =>
            array (
                'id' => 1830,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '870',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 =>
            array (
                'id' => 1831,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1198',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 =>
            array (
                'id' => 1832,
                // 'id_municipality' => 970,
                'district_id' => 9,
                'identifier' => '517',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 =>
            array (
                'id' => 1833,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '871',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 =>
            array (
                'id' => 1834,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '872',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 =>
            array (
                'id' => 1835,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '873',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 =>
            array (
                'id' => 1836,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1199',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 =>
            array (
                'id' => 1837,
                // 'id_municipality' => 971,
                'district_id' => 9,
                'identifier' => '518',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 =>
            array (
                'id' => 1838,
                // 'id_municipality' => 971,
                'district_id' => 9,
                'identifier' => '520',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 =>
            array (
                'id' => 1839,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1200',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 =>
            array (
                'id' => 1840,
                // 'id_municipality' => 971,
                'district_id' => 9,
                'identifier' => '519',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 =>
            array (
                'id' => 1841,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '809',
                'created_at' => '2017-09-25 22:42:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 =>
            array (
                'id' => 1842,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '810',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 =>
            array (
                'id' => 1843,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '822',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 =>
            array (
                'id' => 1844,
                // 'id_municipality' => 971,
                'district_id' => 9,
                'identifier' => '521',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 =>
            array (
                'id' => 1845,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '823',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 =>
            array (
                'id' => 1846,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '824',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 =>
            array (
                'id' => 1847,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '874',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 =>
            array (
                'id' => 1848,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '875',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 =>
            array (
                'id' => 1849,
                // 'id_municipality' => 971,
                'district_id' => 9,
                'identifier' => '522',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 =>
            array (
                'id' => 1850,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '805',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 =>
            array (
                'id' => 1851,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '806',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 =>
            array (
                'id' => 1852,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '807',
                'created_at' => '2017-09-25 22:43:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 =>
            array (
                'id' => 1853,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '876',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 =>
            array (
                'id' => 1854,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2316',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 =>
            array (
                'id' => 1855,
                // 'id_municipality' => 974,
                'district_id' => 9,
                'identifier' => '808',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 =>
            array (
                'id' => 1856,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '827',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 =>
            array (
                'id' => 1857,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '877',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 =>
            array (
                'id' => 1858,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '878',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 =>
            array (
                'id' => 1859,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '879',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 =>
            array (
                'id' => 1860,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '880',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 =>
            array (
                'id' => 1861,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2317',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 =>
            array (
                'id' => 1862,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '828',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 =>
            array (
                'id' => 1863,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '829',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 =>
            array (
                'id' => 1864,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '941',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 =>
            array (
                'id' => 1865,
                // 'id_municipality' => 977,
                'district_id' => 9,
                'identifier' => '830',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 =>
            array (
                'id' => 1866,
                // 'id_municipality' => 978,
                'district_id' => 9,
                'identifier' => '831',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 =>
            array (
                'id' => 1867,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '942',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 =>
            array (
                'id' => 1868,
                // 'id_municipality' => 978,
                'district_id' => 9,
                'identifier' => '833',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 =>
            array (
                'id' => 1869,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '943',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 =>
            array (
                'id' => 1870,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2318',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 =>
            array (
                'id' => 1871,
                // 'id_municipality' => 978,
                'district_id' => 9,
                'identifier' => '834',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 =>
            array (
                'id' => 1872,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '860',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 =>
            array (
                'id' => 1873,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '861',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 =>
            array (
                'id' => 1874,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '862',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 =>
            array (
                'id' => 1875,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '881',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 =>
            array (
                'id' => 1876,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '882',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 =>
            array (
                'id' => 1877,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '888',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 =>
            array (
                'id' => 1878,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '889',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 =>
            array (
                'id' => 1879,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '890',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 =>
            array (
                'id' => 1880,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '891',
                'created_at' => '2017-09-25 22:43:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 =>
            array (
                'id' => 1881,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '895',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 =>
            array (
                'id' => 1882,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '896',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 =>
            array (
                'id' => 1883,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '897',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 =>
            array (
                'id' => 1884,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '898',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 =>
            array (
                'id' => 1885,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '938',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 =>
            array (
                'id' => 1886,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '899',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 =>
            array (
                'id' => 1887,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '900',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 =>
            array (
                'id' => 1888,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '901',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 =>
            array (
                'id' => 1889,
                // 'id_municipality' => 983,
                'district_id' => 9,
                'identifier' => '911',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 =>
            array (
                'id' => 1890,
                // 'id_municipality' => 983,
                'district_id' => 9,
                'identifier' => '912',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 =>
            array (
                'id' => 1891,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '944',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 =>
            array (
                'id' => 1892,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2402',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 =>
            array (
                'id' => 1893,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '902',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 =>
            array (
                'id' => 1894,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '903',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 =>
            array (
                'id' => 1895,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '904',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 =>
            array (
                'id' => 1896,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '906',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 =>
            array (
                'id' => 1897,
                // 'id_municipality' => 981,
                'district_id' => 9,
                'identifier' => '907',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 =>
            array (
                'id' => 1898,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '923',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 =>
            array (
                'id' => 1899,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '945',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 =>
            array (
                'id' => 1900,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '913',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 =>
            array (
                'id' => 1901,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '914',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 =>
            array (
                'id' => 1902,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '915',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 =>
            array (
                'id' => 1903,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '916',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 =>
            array (
                'id' => 1904,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '917',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 =>
            array (
                'id' => 1905,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '918',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 =>
            array (
                'id' => 1906,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1241',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 =>
            array (
                'id' => 1907,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '919',
                'created_at' => '2017-09-25 22:43:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 =>
            array (
                'id' => 1908,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '920',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 =>
            array (
                'id' => 1909,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '921',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 =>
            array (
                'id' => 1910,
                // 'id_municipality' => 984,
                'district_id' => 9,
                'identifier' => '922',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 =>
            array (
                'id' => 1911,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '947',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 =>
            array (
                'id' => 1912,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '948',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 =>
            array (
                'id' => 1913,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '949',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 =>
            array (
                'id' => 1914,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '952',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 =>
            array (
                'id' => 1915,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '953',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 =>
            array (
                'id' => 1916,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1201',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 =>
            array (
                'id' => 1917,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1202',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 =>
            array (
                'id' => 1918,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '954',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 =>
            array (
                'id' => 1919,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '957',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 =>
            array (
                'id' => 1920,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1203',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 =>
            array (
                'id' => 1921,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1204',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 =>
            array (
                'id' => 1922,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1205',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 =>
            array (
                'id' => 1923,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '956',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 =>
            array (
                'id' => 1924,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1206',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 =>
            array (
                'id' => 1925,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1207',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 =>
            array (
                'id' => 1926,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '958',
                'created_at' => '2017-09-25 22:43:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 =>
            array (
                'id' => 1927,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '959',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 =>
            array (
                'id' => 1928,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1208',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 =>
            array (
                'id' => 1929,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1209',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 =>
            array (
                'id' => 1930,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1210',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 =>
            array (
                'id' => 1931,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '960',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 =>
            array (
                'id' => 1932,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '964',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 =>
            array (
                'id' => 1933,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '961',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 =>
            array (
                'id' => 1934,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '962',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 =>
            array (
                'id' => 1935,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '963',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 =>
            array (
                'id' => 1936,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1211',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 =>
            array (
                'id' => 1937,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '965',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 =>
            array (
                'id' => 1938,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '966',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 =>
            array (
                'id' => 1939,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '967',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 =>
            array (
                'id' => 1940,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '968',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 =>
            array (
                'id' => 1941,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1713',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 =>
            array (
                'id' => 1942,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1212',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 =>
            array (
                'id' => 1943,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1213',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 =>
            array (
                'id' => 1944,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1214',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 =>
            array (
                'id' => 1945,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '969',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 =>
            array (
                'id' => 1946,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '970',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 =>
            array (
                'id' => 1947,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '971',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 =>
            array (
                'id' => 1948,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '972',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 =>
            array (
                'id' => 1949,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '973',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 =>
            array (
                'id' => 1950,
                // 'id_municipality' => 986,
                'district_id' => 9,
                'identifier' => '974',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 =>
            array (
                'id' => 1951,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1712',
                'created_at' => '2017-09-25 22:43:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 =>
            array (
                'id' => 1952,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1215',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 =>
            array (
                'id' => 1953,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1216',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 =>
            array (
                'id' => 1954,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1714',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 =>
            array (
                'id' => 1955,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1715',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 =>
            array (
                'id' => 1956,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1217',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 =>
            array (
                'id' => 1957,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1218',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 =>
            array (
                'id' => 1958,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1219',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 =>
            array (
                'id' => 1959,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1220',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 =>
            array (
                'id' => 1960,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1221',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 =>
            array (
                'id' => 1961,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1222',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 =>
            array (
                'id' => 1962,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1716',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 =>
            array (
                'id' => 1963,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1717',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 =>
            array (
                'id' => 1964,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1223',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 =>
            array (
                'id' => 1965,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1224',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 =>
            array (
                'id' => 1966,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1225',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 =>
            array (
                'id' => 1967,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1226',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 =>
            array (
                'id' => 1968,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1718',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 =>
            array (
                'id' => 1969,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1720',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 =>
            array (
                'id' => 1970,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1227',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 =>
            array (
                'id' => 1971,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1719',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 =>
            array (
                'id' => 1972,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1228',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 =>
            array (
                'id' => 1973,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1721',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 =>
            array (
                'id' => 1974,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1240',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 =>
            array (
                'id' => 1975,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1722',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 =>
            array (
                'id' => 1976,
                // 'id_municipality' => 990,
                'district_id' => 9,
                'identifier' => '1723',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 =>
            array (
                'id' => 1977,
                // 'id_municipality' => 991,
                'district_id' => 9,
                'identifier' => '1724',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 =>
            array (
                'id' => 1978,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2092',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 =>
            array (
                'id' => 1979,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1242',
                'created_at' => '2017-09-25 22:43:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 =>
            array (
                'id' => 1980,
                // 'id_municipality' => 991,
                'district_id' => 9,
                'identifier' => '1725',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 =>
            array (
                'id' => 1981,
                // 'id_municipality' => 991,
                'district_id' => 9,
                'identifier' => '1726',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 =>
            array (
                'id' => 1982,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1238',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 =>
            array (
                'id' => 1983,
                // 'id_municipality' => 991,
                'district_id' => 9,
                'identifier' => '1727',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 =>
            array (
                'id' => 1984,
                // 'id_municipality' => 991,
                'district_id' => 9,
                'identifier' => '1728',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 =>
            array (
                'id' => 1985,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2086',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 =>
            array (
                'id' => 1986,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2087',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 =>
            array (
                'id' => 1987,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2102',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 =>
            array (
                'id' => 1988,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2088',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 =>
            array (
                'id' => 1989,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2089',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 =>
            array (
                'id' => 1990,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2090',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 =>
            array (
                'id' => 1991,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2091',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 =>
            array (
                'id' => 1992,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1243',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 =>
            array (
                'id' => 1993,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2093',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 =>
            array (
                'id' => 1994,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2094',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 =>
            array (
                'id' => 1995,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2099',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 =>
            array (
                'id' => 1996,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1244',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 =>
            array (
                'id' => 1997,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2095',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 =>
            array (
                'id' => 1998,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2096',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 =>
            array (
                'id' => 1999,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1126',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 =>
            array (
                'id' => 2000,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1132',
                'created_at' => '2017-09-25 22:43:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 2001,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2097',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2002,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2098',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 2003,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2100',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 2004,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2101',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 2005,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1133',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 2006,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1239',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 2007,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2103',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 2008,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2104',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 2009,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1134',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 2010,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1135',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 2011,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1136',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 2012,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1137',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 2013,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2105',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 2014,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2106',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 2015,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1138',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 2016,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1139',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 2017,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2107',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 2018,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2108',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 2019,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2109',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 2020,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1142',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 2021,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1143',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 2022,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1144',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 2023,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1145',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 2024,
                // 'id_municipality' => 996,
                'district_id' => 9,
                'identifier' => '2110',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 2025,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1122',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 2026,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1123',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 2027,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1124',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 2028,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1125',
                'created_at' => '2017-09-25 22:43:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 2029,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1229',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 2030,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1230',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 2031,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1231',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 2032,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1232',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 2033,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1233',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 2034,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1234',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 2035,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1235',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 2036,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1236',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 2037,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1237',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 2038,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1245',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 2039,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1247',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 2040,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1248',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 2041,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1249',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 2042,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1250',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 2043,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1251',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 2044,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1252',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 2045,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1253',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 2046,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1254',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 2047,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1255',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 2048,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1256',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 2049,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1257',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 2050,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1258',
                'created_at' => '2017-09-25 22:43:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 2051,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1259',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 2052,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1260',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 2053,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1261',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 2054,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1262',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 2055,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1263',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 2056,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1264',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 2057,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1265',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 2058,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1266',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 2059,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1267',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 2060,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1268',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 2061,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1269',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 2062,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1270',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 2063,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1271',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 2064,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1272',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 2065,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1273',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 2066,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1274',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 2067,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1275',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 2068,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1276',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 2069,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1277',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 2070,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1278',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 2071,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1279',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 2072,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1280',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 2073,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1281',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 2074,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1282',
                'created_at' => '2017-09-25 22:43:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 2075,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1283',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 2076,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1287',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 2077,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1288',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 2078,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1289',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 2079,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1290',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 2080,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1291',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 2081,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1292',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 2082,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1293',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 2083,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1294',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 2084,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1295',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 2085,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1296',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 2086,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1297',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 2087,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1298',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 2088,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1299',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 2089,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1300',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 2090,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1305',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 2091,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1306',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 2092,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1307',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 2093,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1308',
                'created_at' => '2017-09-25 22:43:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 2094,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1309',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 2095,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1310',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 2096,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1311',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 2097,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1312',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 2098,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1313',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 2099,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1314',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 2100,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1315',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 2101,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1675',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 2102,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1316',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 2103,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1317',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 2104,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1318',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 2105,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1319',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 2106,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1320',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 2107,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1321',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 2108,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1322',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 2109,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1323',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 2110,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1324',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 2111,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1325',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 2112,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1326',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 2113,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1327',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 2114,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1328',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 2115,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1329',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 2116,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1330',
                'created_at' => '2017-09-25 22:43:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 2117,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1331',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 2118,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1332',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 2119,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1333',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 2120,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1334',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 2121,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1335',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 2122,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1336',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 2123,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1337',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 2124,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1691',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 2125,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1338',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 2126,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1339',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 2127,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1340',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 2128,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1341',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 2129,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1342',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 2130,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1343',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 2131,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1344',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 2132,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1345',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 2133,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1346',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 2134,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1347',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 2135,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1348',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 2136,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1349',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 2137,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1350',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 2138,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1692',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 2139,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1351',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 2140,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1352',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 2141,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1353',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 2142,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1354',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 2143,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1355',
                'created_at' => '2017-09-25 22:43:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 2144,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1356',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 2145,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1357',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 2146,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1358',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 2147,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1359',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 2148,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1360',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 2149,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1361',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 2150,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1362',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 2151,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1363',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 2152,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1364',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 2153,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1365',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 2154,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1427',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 2155,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1366',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 2156,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1367',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 2157,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1368',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 2158,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1369',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 2159,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1370',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 2160,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1371',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 2161,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1372',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 2162,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1373',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 2163,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1374',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 2164,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1375',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 2165,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1376',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 2166,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1377',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 =>
            array (
                'id' => 2167,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1378',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 =>
            array (
                'id' => 2168,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1379',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 =>
            array (
                'id' => 2169,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1397',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 =>
            array (
                'id' => 2170,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1380',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 =>
            array (
                'id' => 2171,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1381',
                'created_at' => '2017-09-25 22:43:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 =>
            array (
                'id' => 2172,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1382',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 =>
            array (
                'id' => 2173,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1383',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 =>
            array (
                'id' => 2174,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1384',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 =>
            array (
                'id' => 2175,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1385',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 =>
            array (
                'id' => 2176,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1386',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 =>
            array (
                'id' => 2177,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1387',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 =>
            array (
                'id' => 2178,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1423',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 =>
            array (
                'id' => 2179,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1388',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 =>
            array (
                'id' => 2180,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1389',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 =>
            array (
                'id' => 2181,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1390',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 =>
            array (
                'id' => 2182,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1391',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 =>
            array (
                'id' => 2183,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1392',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 =>
            array (
                'id' => 2184,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1393',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 =>
            array (
                'id' => 2185,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1394',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 =>
            array (
                'id' => 2186,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1395',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 =>
            array (
                'id' => 2187,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1396',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 =>
            array (
                'id' => 2188,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1424',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 =>
            array (
                'id' => 2189,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1398',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 =>
            array (
                'id' => 2190,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1399',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 =>
            array (
                'id' => 2191,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1400',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 =>
            array (
                'id' => 2192,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1401',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 =>
            array (
                'id' => 2193,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1402',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 =>
            array (
                'id' => 2194,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1403',
                'created_at' => '2017-09-25 22:43:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 =>
            array (
                'id' => 2195,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1404',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 =>
            array (
                'id' => 2196,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1673',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 =>
            array (
                'id' => 2197,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1405',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 =>
            array (
                'id' => 2198,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1406',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 =>
            array (
                'id' => 2199,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1407',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 =>
            array (
                'id' => 2200,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1408',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 =>
            array (
                'id' => 2201,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1409',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 =>
            array (
                'id' => 2202,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1410',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 =>
            array (
                'id' => 2203,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1411',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 =>
            array (
                'id' => 2204,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1674',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 =>
            array (
                'id' => 2205,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1412',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 =>
            array (
                'id' => 2206,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1413',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 =>
            array (
                'id' => 2207,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1414',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 =>
            array (
                'id' => 2208,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1415',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 =>
            array (
                'id' => 2209,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1425',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 =>
            array (
                'id' => 2210,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1416',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 =>
            array (
                'id' => 2211,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1417',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 =>
            array (
                'id' => 2212,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1418',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 =>
            array (
                'id' => 2213,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1419',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 =>
            array (
                'id' => 2214,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1420',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 =>
            array (
                'id' => 2215,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1426',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 =>
            array (
                'id' => 2216,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1693',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 =>
            array (
                'id' => 2217,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1421',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 =>
            array (
                'id' => 2218,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1422',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 =>
            array (
                'id' => 2219,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1428',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 =>
            array (
                'id' => 2220,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1429',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 =>
            array (
                'id' => 2221,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1430',
                'created_at' => '2017-09-25 22:43:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 =>
            array (
                'id' => 2222,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1694',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 =>
            array (
                'id' => 2223,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1431',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 =>
            array (
                'id' => 2224,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1641',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 =>
            array (
                'id' => 2225,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1642',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 =>
            array (
                'id' => 2226,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1650',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 =>
            array (
                'id' => 2227,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1651',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 =>
            array (
                'id' => 2228,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1652',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 =>
            array (
                'id' => 2229,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1653',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 =>
            array (
                'id' => 2230,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1654',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 =>
            array (
                'id' => 2231,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1655',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 =>
            array (
                'id' => 2232,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1656',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 =>
            array (
                'id' => 2233,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1657',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 =>
            array (
                'id' => 2234,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1658',
                'created_at' => '2017-09-25 22:43:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 =>
            array (
                'id' => 2235,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1659',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 =>
            array (
                'id' => 2236,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1660',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 =>
            array (
                'id' => 2237,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1661',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 =>
            array (
                'id' => 2238,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1662',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 =>
            array (
                'id' => 2239,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1663',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 =>
            array (
                'id' => 2240,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1664',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 =>
            array (
                'id' => 2241,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1665',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 =>
            array (
                'id' => 2242,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1666',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 =>
            array (
                'id' => 2243,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1667',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 =>
            array (
                'id' => 2244,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1668',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 =>
            array (
                'id' => 2245,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1669',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 =>
            array (
                'id' => 2246,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1670',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 =>
            array (
                'id' => 2247,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1671',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 =>
            array (
                'id' => 2248,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1672',
                'created_at' => '2017-09-25 22:43:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 =>
            array (
                'id' => 2249,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1695',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 =>
            array (
                'id' => 2250,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1676',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 =>
            array (
                'id' => 2251,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1677',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 =>
            array (
                'id' => 2252,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1678',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 =>
            array (
                'id' => 2253,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1679',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 =>
            array (
                'id' => 2254,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1680',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 =>
            array (
                'id' => 2255,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1681',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 =>
            array (
                'id' => 2256,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1682',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 =>
            array (
                'id' => 2257,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1683',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 =>
            array (
                'id' => 2258,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1684',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 =>
            array (
                'id' => 2259,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1685',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 =>
            array (
                'id' => 2260,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1686',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 =>
            array (
                'id' => 2261,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1687',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 =>
            array (
                'id' => 2262,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1688',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 =>
            array (
                'id' => 2263,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1689',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 =>
            array (
                'id' => 2264,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1690',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 =>
            array (
                'id' => 2265,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1696',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 =>
            array (
                'id' => 2266,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1697',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 =>
            array (
                'id' => 2267,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1698',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 =>
            array (
                'id' => 2268,
                // 'id_municipality' => 987,
                'district_id' => 10,
                'identifier' => '1699',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 =>
            array (
                'id' => 2269,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '526',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 =>
            array (
                'id' => 2270,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '527',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 =>
            array (
                'id' => 2271,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '528',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 =>
            array (
                'id' => 2272,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '529',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 =>
            array (
                'id' => 2273,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '530',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 =>
            array (
                'id' => 2274,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '531',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 =>
            array (
                'id' => 2275,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '532',
                'created_at' => '2017-09-25 22:43:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 =>
            array (
                'id' => 2276,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '533',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 =>
            array (
                'id' => 2277,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '534',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 =>
            array (
                'id' => 2278,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '535',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 =>
            array (
                'id' => 2279,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '576',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 =>
            array (
                'id' => 2280,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '536',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 =>
            array (
                'id' => 2281,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '537',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 =>
            array (
                'id' => 2282,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '538',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 =>
            array (
                'id' => 2283,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '539',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 =>
            array (
                'id' => 2284,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '540',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 =>
            array (
                'id' => 2285,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '541',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 =>
            array (
                'id' => 2286,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '542',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 =>
            array (
                'id' => 2287,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '543',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 =>
            array (
                'id' => 2288,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '544',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 =>
            array (
                'id' => 2289,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '545',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 =>
            array (
                'id' => 2290,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '546',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 =>
            array (
                'id' => 2291,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '547',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 =>
            array (
                'id' => 2292,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '548',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 =>
            array (
                'id' => 2293,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '549',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 =>
            array (
                'id' => 2294,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '550',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 =>
            array (
                'id' => 2295,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '551',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 =>
            array (
                'id' => 2296,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '552',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 =>
            array (
                'id' => 2297,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '553',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 =>
            array (
                'id' => 2298,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '554',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 =>
            array (
                'id' => 2299,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '555',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 =>
            array (
                'id' => 2300,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '556',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 =>
            array (
                'id' => 2301,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '571',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 =>
            array (
                'id' => 2302,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '557',
                'created_at' => '2017-09-25 22:43:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 =>
            array (
                'id' => 2303,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '558',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 =>
            array (
                'id' => 2304,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '559',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 =>
            array (
                'id' => 2305,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '560',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 =>
            array (
                'id' => 2306,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '561',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 =>
            array (
                'id' => 2307,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '562',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 =>
            array (
                'id' => 2308,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '563',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 =>
            array (
                'id' => 2309,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '564',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 =>
            array (
                'id' => 2310,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '565',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 =>
            array (
                'id' => 2311,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '566',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 =>
            array (
                'id' => 2312,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '567',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 =>
            array (
                'id' => 2313,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '568',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 =>
            array (
                'id' => 2314,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '569',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 =>
            array (
                'id' => 2315,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '570',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 =>
            array (
                'id' => 2316,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '577',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 =>
            array (
                'id' => 2317,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '578',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 =>
            array (
                'id' => 2318,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '579',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 =>
            array (
                'id' => 2319,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '580',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 =>
            array (
                'id' => 2320,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '581',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 =>
            array (
                'id' => 2321,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '582',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 =>
            array (
                'id' => 2322,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '583',
                'created_at' => '2017-09-25 22:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 =>
            array (
                'id' => 2323,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '584',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 =>
            array (
                'id' => 2324,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '585',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 =>
            array (
                'id' => 2325,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '586',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 =>
            array (
                'id' => 2326,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '587',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 =>
            array (
                'id' => 2327,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '588',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 =>
            array (
                'id' => 2328,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '589',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 =>
            array (
                'id' => 2329,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '590',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 =>
            array (
                'id' => 2330,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '591',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 =>
            array (
                'id' => 2331,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '592',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 =>
            array (
                'id' => 2332,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '593',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 =>
            array (
                'id' => 2333,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '594',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 =>
            array (
                'id' => 2334,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '595',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 =>
            array (
                'id' => 2335,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '596',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 =>
            array (
                'id' => 2336,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '597',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 =>
            array (
                'id' => 2337,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '598',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 =>
            array (
                'id' => 2338,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '599',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 =>
            array (
                'id' => 2339,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '633',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 =>
            array (
                'id' => 2340,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '634',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 =>
            array (
                'id' => 2341,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '600',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 =>
            array (
                'id' => 2342,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '601',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 =>
            array (
                'id' => 2343,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '602',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 =>
            array (
                'id' => 2344,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '603',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 =>
            array (
                'id' => 2345,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '629',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 =>
            array (
                'id' => 2346,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '630',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 =>
            array (
                'id' => 2347,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '631',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 =>
            array (
                'id' => 2348,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '632',
                'created_at' => '2017-09-25 22:43:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 =>
            array (
                'id' => 2349,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '635',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 =>
            array (
                'id' => 2350,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '636',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 =>
            array (
                'id' => 2351,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '637',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 =>
            array (
                'id' => 2352,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '638',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 =>
            array (
                'id' => 2353,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '654',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 =>
            array (
                'id' => 2354,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '655',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 =>
            array (
                'id' => 2355,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '656',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 =>
            array (
                'id' => 2356,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '657',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 =>
            array (
                'id' => 2357,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '658',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 =>
            array (
                'id' => 2358,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '659',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 =>
            array (
                'id' => 2359,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '660',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 =>
            array (
                'id' => 2360,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '661',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 =>
            array (
                'id' => 2361,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '662',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 =>
            array (
                'id' => 2362,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '663',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 =>
            array (
                'id' => 2363,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '664',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 =>
            array (
                'id' => 2364,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '665',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 =>
            array (
                'id' => 2365,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '666',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 =>
            array (
                'id' => 2366,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '667',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 =>
            array (
                'id' => 2367,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '684',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 =>
            array (
                'id' => 2368,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '685',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 =>
            array (
                'id' => 2369,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '686',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 =>
            array (
                'id' => 2370,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '737',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 =>
            array (
                'id' => 2371,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '687',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 =>
            array (
                'id' => 2372,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '695',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 =>
            array (
                'id' => 2373,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '696',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 =>
            array (
                'id' => 2374,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '710',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 =>
            array (
                'id' => 2375,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '711',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 =>
            array (
                'id' => 2376,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '712',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 =>
            array (
                'id' => 2377,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '713',
                'created_at' => '2017-09-25 22:43:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 =>
            array (
                'id' => 2378,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '714',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 =>
            array (
                'id' => 2379,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '715',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 =>
            array (
                'id' => 2380,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '716',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 =>
            array (
                'id' => 2381,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '717',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 =>
            array (
                'id' => 2382,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '718',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 =>
            array (
                'id' => 2383,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '719',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 =>
            array (
                'id' => 2384,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '720',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 =>
            array (
                'id' => 2385,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '721',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 =>
            array (
                'id' => 2386,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '722',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 =>
            array (
                'id' => 2387,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '723',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 =>
            array (
                'id' => 2388,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '724',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 =>
            array (
                'id' => 2389,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '736',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 =>
            array (
                'id' => 2390,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '725',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 =>
            array (
                'id' => 2391,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '726',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 =>
            array (
                'id' => 2392,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '727',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 =>
            array (
                'id' => 2393,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '728',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 =>
            array (
                'id' => 2394,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '729',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 =>
            array (
                'id' => 2395,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '730',
                'created_at' => '2017-09-25 22:43:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 =>
            array (
                'id' => 2396,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '731',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 =>
            array (
                'id' => 2397,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '732',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 =>
            array (
                'id' => 2398,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '733',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 =>
            array (
                'id' => 2399,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '734',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 =>
            array (
                'id' => 2400,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '735',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 =>
            array (
                'id' => 2401,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '738',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 =>
            array (
                'id' => 2402,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '739',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 =>
            array (
                'id' => 2403,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '740',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 =>
            array (
                'id' => 2404,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '744',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 =>
            array (
                'id' => 2405,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '745',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 =>
            array (
                'id' => 2406,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '746',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 =>
            array (
                'id' => 2407,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '747',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 =>
            array (
                'id' => 2408,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '748',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 =>
            array (
                'id' => 2409,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '749',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 =>
            array (
                'id' => 2410,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '801',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 =>
            array (
                'id' => 2411,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '750',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 =>
            array (
                'id' => 2412,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '751',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 =>
            array (
                'id' => 2413,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '752',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 =>
            array (
                'id' => 2414,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '753',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 =>
            array (
                'id' => 2415,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '754',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 =>
            array (
                'id' => 2416,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '755',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 =>
            array (
                'id' => 2417,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '756',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 =>
            array (
                'id' => 2418,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '757',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 =>
            array (
                'id' => 2419,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '758',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 =>
            array (
                'id' => 2420,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '759',
                'created_at' => '2017-09-25 22:43:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 =>
            array (
                'id' => 2421,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '760',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 =>
            array (
                'id' => 2422,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '761',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 =>
            array (
                'id' => 2423,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '762',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 =>
            array (
                'id' => 2424,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '763',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 =>
            array (
                'id' => 2425,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '764',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 =>
            array (
                'id' => 2426,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '765',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 =>
            array (
                'id' => 2427,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '766',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 =>
            array (
                'id' => 2428,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '767',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 =>
            array (
                'id' => 2429,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '768',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 =>
            array (
                'id' => 2430,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '769',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 =>
            array (
                'id' => 2431,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '770',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 =>
            array (
                'id' => 2432,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '771',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 =>
            array (
                'id' => 2433,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '772',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 =>
            array (
                'id' => 2434,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '773',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 =>
            array (
                'id' => 2435,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '774',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 =>
            array (
                'id' => 2436,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '775',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 =>
            array (
                'id' => 2437,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '776',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 =>
            array (
                'id' => 2438,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '777',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 =>
            array (
                'id' => 2439,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '778',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 =>
            array (
                'id' => 2440,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '779',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 =>
            array (
                'id' => 2441,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '780',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 =>
            array (
                'id' => 2442,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2677',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 =>
            array (
                'id' => 2443,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '781',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 =>
            array (
                'id' => 2444,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '782',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 =>
            array (
                'id' => 2445,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '783',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 =>
            array (
                'id' => 2446,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '784',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 =>
            array (
                'id' => 2447,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '785',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 =>
            array (
                'id' => 2448,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '786',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 =>
            array (
                'id' => 2449,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '787',
                'created_at' => '2017-09-25 22:43:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 =>
            array (
                'id' => 2450,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '788',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 =>
            array (
                'id' => 2451,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '789',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 =>
            array (
                'id' => 2452,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '790',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 =>
            array (
                'id' => 2453,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '791',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 =>
            array (
                'id' => 2454,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '792',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 =>
            array (
                'id' => 2455,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '793',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 =>
            array (
                'id' => 2456,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '802',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 =>
            array (
                'id' => 2457,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '182',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 =>
            array (
                'id' => 2458,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '794',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 =>
            array (
                'id' => 2459,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '795',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 =>
            array (
                'id' => 2460,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '796',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 =>
            array (
                'id' => 2461,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '797',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 =>
            array (
                'id' => 2462,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '798',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 =>
            array (
                'id' => 2463,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '799',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 =>
            array (
                'id' => 2464,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '800',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 =>
            array (
                'id' => 2465,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '803',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 =>
            array (
                'id' => 2466,
                // 'id_municipality' => 973,
                'district_id' => 11,
                'identifier' => '804',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 =>
            array (
                'id' => 2467,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '175',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 =>
            array (
                'id' => 2468,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '176',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 =>
            array (
                'id' => 2469,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '177',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 =>
            array (
                'id' => 2470,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '178',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 =>
            array (
                'id' => 2471,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '179',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 =>
            array (
                'id' => 2472,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '180',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 =>
            array (
                'id' => 2473,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '181',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 =>
            array (
                'id' => 2474,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2319',
                'created_at' => '2017-09-25 22:43:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 =>
            array (
                'id' => 2475,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '172',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 =>
            array (
                'id' => 2476,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '173',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 =>
            array (
                'id' => 2477,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '174',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 =>
            array (
                'id' => 2478,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2320',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 =>
            array (
                'id' => 2479,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '183',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 =>
            array (
                'id' => 2480,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '184',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 =>
            array (
                'id' => 2481,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '185',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 =>
            array (
                'id' => 2482,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '186',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 =>
            array (
                'id' => 2483,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '187',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 =>
            array (
                'id' => 2484,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '188',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 =>
            array (
                'id' => 2485,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '189',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 =>
            array (
                'id' => 2486,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2321',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 =>
            array (
                'id' => 2487,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '190',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 =>
            array (
                'id' => 2488,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '191',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 =>
            array (
                'id' => 2489,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '195',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 =>
            array (
                'id' => 2490,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '841',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 =>
            array (
                'id' => 2491,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '192',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 =>
            array (
                'id' => 2492,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '193',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 =>
            array (
                'id' => 2493,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '194',
                'created_at' => '2017-09-25 22:43:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 =>
            array (
                'id' => 2494,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '849',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 =>
            array (
                'id' => 2495,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2322',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 =>
            array (
                'id' => 2496,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '196',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 =>
            array (
                'id' => 2497,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '199',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 =>
            array (
                'id' => 2498,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1703',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 =>
            array (
                'id' => 2499,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '197',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 =>
            array (
                'id' => 2500,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '198',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('sections')->insert(array (
            0 =>
            array (
                'id' => 2501,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2323',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2502,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '200',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 2503,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '201',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 2504,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2324',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 2505,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2325',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 2506,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '202',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 2507,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '205',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 2508,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2326',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 2509,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2327',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 2510,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '203',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 2511,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '842',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 2512,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '204',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 2513,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2678',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 2514,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '206',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 2515,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '208',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 2516,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '207',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 2517,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '843',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 2518,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2328',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 2519,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2329',
                'created_at' => '2017-09-25 22:43:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 2520,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '209',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 2521,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '210',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 2522,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '214',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 2523,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '211',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 2524,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '212',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 2525,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '835',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 2526,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2330',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 2527,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2331',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 2528,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '213',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 2529,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '837',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 2530,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '215',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 2531,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '838',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 2532,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '839',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 2533,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '216',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 2534,
                // 'id_municipality' => 956,
                'district_id' => 12,
                'identifier' => '217',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 2535,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '844',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 2536,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '845',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 2537,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '846',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 2538,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '847',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 2539,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '848',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 2540,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '851',
                'created_at' => '2017-09-25 22:43:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 2541,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '852',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 2542,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2332',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 2543,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '853',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 2544,
                // 'id_municipality' => 982,
                'district_id' => 12,
                'identifier' => '908',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 2545,
                // 'id_municipality' => 982,
                'district_id' => 12,
                'identifier' => '909',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 2546,
                // 'id_municipality' => 982,
                'district_id' => 12,
                'identifier' => '910',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 2547,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2342',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 2548,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1704',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 2549,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1705',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 2550,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1708',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 2551,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1706',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 2552,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1707',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 2553,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2313',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 2554,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1709',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 2555,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1710',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 2556,
                // 'id_municipality' => 989,
                'district_id' => 12,
                'identifier' => '1711',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 2557,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2308',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 2558,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2309',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 2559,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2310',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 2560,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2311',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 2561,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2312',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 2562,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2333',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 2563,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2334',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 2564,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2335',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 2565,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2336',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 2566,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2337',
                'created_at' => '2017-09-25 22:43:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 2567,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2338',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 2568,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2339',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 2569,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2340',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 2570,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2341',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 2571,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2343',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 2572,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2344',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 2573,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2345',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 2574,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2346',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 2575,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2347',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 2576,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2348',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 =>
            array (
                'id' => 2577,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2349',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 =>
            array (
                'id' => 2578,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2350',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 =>
            array (
                'id' => 2579,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2351',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 =>
            array (
                'id' => 2580,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2359',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 =>
            array (
                'id' => 2581,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2369',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 =>
            array (
                'id' => 2582,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2352',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 =>
            array (
                'id' => 2583,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2353',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 =>
            array (
                'id' => 2584,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2354',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 =>
            array (
                'id' => 2585,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2355',
                'created_at' => '2017-09-25 22:43:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 =>
            array (
                'id' => 2586,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2356',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 =>
            array (
                'id' => 2587,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2357',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 =>
            array (
                'id' => 2588,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2358',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 =>
            array (
                'id' => 2589,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2360',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 =>
            array (
                'id' => 2590,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2361',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 =>
            array (
                'id' => 2591,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2362',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 =>
            array (
                'id' => 2592,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2363',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 =>
            array (
                'id' => 2593,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2364',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 =>
            array (
                'id' => 2594,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2365',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 =>
            array (
                'id' => 2595,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2366',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 =>
            array (
                'id' => 2596,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2367',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 =>
            array (
                'id' => 2597,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2368',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 =>
            array (
                'id' => 2598,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2370',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 =>
            array (
                'id' => 2599,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2371',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 =>
            array (
                'id' => 2600,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2372',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 =>
            array (
                'id' => 2601,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2373',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 =>
            array (
                'id' => 2602,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2374',
                'created_at' => '2017-09-25 22:43:32',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 =>
            array (
                'id' => 2603,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2375',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 =>
            array (
                'id' => 2604,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2376',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 =>
            array (
                'id' => 2605,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2377',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 =>
            array (
                'id' => 2606,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2378',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 =>
            array (
                'id' => 2607,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2379',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 =>
            array (
                'id' => 2608,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2389',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 =>
            array (
                'id' => 2609,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2380',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 =>
            array (
                'id' => 2610,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2381',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 =>
            array (
                'id' => 2611,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2382',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 =>
            array (
                'id' => 2612,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2383',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 =>
            array (
                'id' => 2613,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2384',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 =>
            array (
                'id' => 2614,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2385',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 =>
            array (
                'id' => 2615,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2386',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 =>
            array (
                'id' => 2616,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2387',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 =>
            array (
                'id' => 2617,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2388',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 =>
            array (
                'id' => 2618,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2401',
                'created_at' => '2017-09-25 22:43:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 =>
            array (
                'id' => 2619,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2390',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 =>
            array (
                'id' => 2620,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2391',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 =>
            array (
                'id' => 2621,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2392',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 =>
            array (
                'id' => 2622,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2393',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 =>
            array (
                'id' => 2623,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2394',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 =>
            array (
                'id' => 2624,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2395',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 =>
            array (
                'id' => 2625,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2396',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 =>
            array (
                'id' => 2626,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2397',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 =>
            array (
                'id' => 2627,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2398',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 =>
            array (
                'id' => 2628,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2399',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 =>
            array (
                'id' => 2629,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2400',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 =>
            array (
                'id' => 2630,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2403',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 =>
            array (
                'id' => 2631,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2404',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 =>
            array (
                'id' => 2632,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2405',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 =>
            array (
                'id' => 2633,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2406',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 =>
            array (
                'id' => 2634,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2673',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 =>
            array (
                'id' => 2635,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2407',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 =>
            array (
                'id' => 2636,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2661',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 =>
            array (
                'id' => 2637,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2662',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 =>
            array (
                'id' => 2638,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2663',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 =>
            array (
                'id' => 2639,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2674',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 =>
            array (
                'id' => 2640,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2675',
                'created_at' => '2017-09-25 22:43:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 =>
            array (
                'id' => 2641,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2664',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 =>
            array (
                'id' => 2642,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2665',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 =>
            array (
                'id' => 2643,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2666',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 =>
            array (
                'id' => 2644,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2667',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 =>
            array (
                'id' => 2645,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2668',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 =>
            array (
                'id' => 2646,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2669',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 =>
            array (
                'id' => 2647,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2670',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 =>
            array (
                'id' => 2648,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2671',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 =>
            array (
                'id' => 2649,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2672',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 =>
            array (
                'id' => 2650,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2676',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 =>
            array (
                'id' => 2651,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2679',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 =>
            array (
                'id' => 2652,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2680',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 =>
            array (
                'id' => 2653,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2681',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 =>
            array (
                'id' => 2654,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2682',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 =>
            array (
                'id' => 2655,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2683',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 =>
            array (
                'id' => 2656,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2684',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 =>
            array (
                'id' => 2657,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2685',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 =>
            array (
                'id' => 2658,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2686',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 =>
            array (
                'id' => 2659,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2687',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 =>
            array (
                'id' => 2660,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2688',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 =>
            array (
                'id' => 2661,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2689',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 =>
            array (
                'id' => 2662,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2690',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 =>
            array (
                'id' => 2663,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2691',
                'created_at' => '2017-09-25 22:43:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 =>
            array (
                'id' => 2664,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2692',
                'created_at' => '2017-09-25 22:43:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 =>
            array (
                'id' => 2665,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2693',
                'created_at' => '2017-09-25 22:43:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 =>
            array (
                'id' => 2666,
                // 'id_municipality' => 979,
                'district_id' => 12,
                'identifier' => '2694',
                'created_at' => '2017-09-25 22:43:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
