<?php

use Illuminate\Database\Seeder;

class allMunicipalities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('municipalities')->insert(array (
            0=> array (
                'entity_id' => 1,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            1=> array (
                'entity_id' => 2,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            2=> array (
                'entity_id' => 3,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            3=> array (
                'entity_id' => 4,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            4=> array (
                'entity_id' => 5,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            5=> array (
                'entity_id' => 6,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            6=> array (
                'entity_id' => 7,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            7=> array (
                'entity_id' => 8,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            8=> array (
                'entity_id' => 9,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            9=> array (
                'entity_id' => 10,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            10=> array (
                'entity_id' => 11,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            11=> array (
                'entity_id' => 12,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            12=> array (
                'entity_id' => 13,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            13=> array (
                'entity_id' => 14,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            14=> array (
                'entity_id' => 15,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            15=> array (
                'entity_id' => 16,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            16=> array (
                'entity_id' => 17,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            17=> array (
                'entity_id' => 18,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),

            18=> array (
                'entity_id' => 19,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            19=> array (
                'entity_id' => 20,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            20=> array (
                'entity_id' => 21,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            21=> array (
                'entity_id' => 22,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            22=> array (
                'entity_id' => 23,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            23=> array (
                'entity_id' => 24,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            24=> array (
                'entity_id' => 25,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            25=> array (
                'entity_id' => 26,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            26=> array (
                'entity_id' => 27,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            27=> array (
                'entity_id' => 28,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            28=> array (
                'entity_id' => 29,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            29=> array (
                'entity_id' => 30,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            30=> array (
                'entity_id' => 31,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
            31=> array (
                'entity_id' => 32,
                'identifier' => 'none',
                'name' => '[TODOS]',
                'population' => 0,
                'created_at' => '2013-04-04 17:42:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'indigenous' =>  false,
                'cnch' =>  false,
                'marginalization_id' => 0,
            ),
        ));
    }
}
