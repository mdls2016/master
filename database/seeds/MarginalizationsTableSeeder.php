<?php

use Illuminate\Database\Seeder;

class  MarginalizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('marginalizations')->delete();
      \DB::table('marginalizations')->insert(array (
        0 =>
      array (
          'id' => 1,
          'name' => 'Muy bajo',
          'description' => '',
          'created_at' => '2017-09-25 22:41:51',
          'updated_at' => NULL,
          'deleted_at' => NULL,
      ),
      1 =>
      array (
          'id' => 2,
          'name' => 'Bajo',
          'description' => '',
          'created_at' => '2017-09-25 22:41:51',
          'updated_at' => NULL,
          'deleted_at' => NULL,
      ),
      2 =>
      array (
          'id' => 3,
          'name' => 'Medio',
          'description' => '',
          'created_at' => '2017-09-25 22:41:52',
          'updated_at' => NULL,
          'deleted_at' => NULL,
      ),
      3 =>
      array (
          'id' => 4,
          'name' => 'Alto',
          'description' => '',
          'created_at' => '2017-09-25 22:41:52',
          'updated_at' => NULL,
          'deleted_at' => NULL,
      ),
      4 =>
      array (
          'id' => 5,
          'name' => 'Muy alto',
          'description' => '',
          'created_at' => '2017-09-25 22:41:52',
          'updated_at' => NULL,
          'deleted_at' => NULL,
      ),
  ));
    }
}
