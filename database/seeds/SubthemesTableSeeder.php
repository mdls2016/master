<?php

use Illuminate\Database\Seeder;

class SubthemesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('subthemes')->delete();

        \DB::table('subthemes')->insert(array (
            0 =>
            array (
                'id' => 1,
                // 'coh' => true,
                'theme_id' => 1,
                'name' => 'Becas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 3,
                // 'coh' => true,
                'theme_id' => 1,
                'name' => 'Infraestructura ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 8,
                // 'coh' => true,
                'theme_id' => 3,
                'name' => 'Educación y cultura ambiental',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 9,
                // 'coh' => true,
                'theme_id' => 3,
                'name' => 'Energía alterna',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 10,
                // 'coh' => true,
                'theme_id' => 3,
                'name' => 'Limpieza',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 12,
                // 'coh' => true,
                'theme_id' => 3,
                'name' => 'Protección',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 14,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Apoyos económicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 15,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Aseguramiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 16,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 17,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Campañas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 18,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Control de plagas y enfermedades',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 19,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Equipamiento',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 23,
                // 'coh' => true,
                'theme_id' => 7,
                'name' => 'Apoyos económicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 24,
                // 'coh' => true,
                'theme_id' => 7,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 26,
                // 'coh' => true,
                'theme_id' => 7,
                'name' => 'Regularización territorial',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 27,
                // 'coh' => true,
                'theme_id' => 8,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 28,
                // 'coh' => true,
                'theme_id' => 8,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 29,
                // 'coh' => true,
                'theme_id' => 8,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 =>
            array (
                'id' => 30,
                // 'coh' => true,
                'theme_id' => 9,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 =>
            array (
                'id' => 31,
                // 'coh' => true,
                'theme_id' => 9,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 =>
            array (
                'id' => 33,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Apoyos económicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 =>
            array (
                'id' => 34,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 =>
            array (
                'id' => 35,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Bolsa de trabajo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 =>
            array (
                'id' => 39,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Adulto mayor',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 =>
            array (
                'id' => 42,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Albergues',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 =>
            array (
                'id' => 44,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 =>
            array (
                'id' => 45,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Equidad de género',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 =>
            array (
                'id' => 59,
                // 'coh' => true,
                'theme_id' => 1,
                'name' => 'Servicios educativos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 =>
            array (
                'id' => 61,
                // 'coh' => true,
                'theme_id' => 2,
                'name' => 'Donación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 =>
            array (
                'id' => 62,
                // 'coh' => true,
                'theme_id' => 2,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 =>
            array (
                'id' => 63,
                // 'coh' => true,
                'theme_id' => 2,
                'name' => 'Personas con discapacidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 =>
            array (
                'id' => 64,
                // 'coh' => true,
                'theme_id' => 2,
                'name' => 'Salud preventiva',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 =>
            array (
                'id' => 65,
                // 'coh' => true,
                'theme_id' => 11,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 =>
            array (
                'id' => 66,
                // 'coh' => true,
                'theme_id' => 11,
                'name' => 'Material deportivo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 =>
            array (
                'id' => 67,
                // 'coh' => true,
                'theme_id' => 11,
                'name' => 'Promoción',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 =>
            array (
                'id' => 68,
                // 'coh' => true,
                'theme_id' => 15,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 =>
            array (
                'id' => 69,
                // 'coh' => true,
                'theme_id' => 15,
                'name' => 'Infraestructura de transporte',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 =>
            array (
                'id' => 71,
                // 'coh' => true,
                'theme_id' => 2,
                'name' => 'Servicios médicos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 =>
            array (
                'id' => 74,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 =>
            array (
                'id' => 75,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Grupos marginados',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 =>
            array (
                'id' => 76,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Guarderías',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 =>
            array (
                'id' => 77,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 =>
            array (
                'id' => 78,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Jóvenes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 =>
            array (
                'id' => 79,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Madres solteras',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 =>
            array (
                'id' => 80,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Migrantes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 =>
            array (
                'id' => 81,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Niños y niñas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 =>
            array (
                'id' => 82,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Personas con discapacidad',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 =>
            array (
                'id' => 84,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Situaciones emergentes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 =>
            array (
                'id' => 85,
                // 'coh' => true,
                'theme_id' => 12,
                'name' => 'Subsidios',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 =>
            array (
                'id' => 87,
                // 'coh' => true,
                'theme_id' => 3,
                'name' => 'Reforestación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 =>
            array (
                'id' => 89,
                // 'coh' => true,
                'theme_id' => 13,
                'name' => 'Apoyos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 =>
            array (
                'id' => 90,
                // 'coh' => true,
                'theme_id' => 13,
                'name' => 'Casas hogares',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 =>
            array (
                'id' => 91,
                // 'coh' => true,
                'theme_id' => 13,
                'name' => 'Comedores',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 =>
            array (
                'id' => 92,
                // 'coh' => true,
                'theme_id' => 4,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 =>
            array (
                'id' => 93,
                // 'coh' => true,
                'theme_id' => 13,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 =>
            array (
                'id' => 94,
                // 'coh' => true,
                'theme_id' => 13,
                'name' => 'Hospitales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 =>
            array (
                'id' => 98,
                // 'coh' => true,
                'theme_id' => 14,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 =>
            array (
                'id' => 99,
                // 'coh' => true,
                'theme_id' => 6,
                'name' => 'Convocatorias',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 =>
            array (
                'id' => 100,
                // 'coh' => true,
                'theme_id' => 6,
                'name' => 'Eventos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 =>
            array (
                'id' => 101,
                // 'coh' => true,
                'theme_id' => 6,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 =>
            array (
                'id' => 103,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Certificaciones',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 =>
            array (
                'id' => 104,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 =>
            array (
                'id' => 105,
                // 'coh' => true,
                'theme_id' => 7,
                'name' => 'Situaciones emergentes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 =>
            array (
                'id' => 106,
                // 'coh' => true,
                'theme_id' => 17,
                'name' => 'Asesoría y capacitación',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 =>
            array (
                'id' => 107,
                // 'coh' => true,
                'theme_id' => 17,
                'name' => 'Combate a las drogas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 =>
            array (
                'id' => 108,
                // 'coh' => true,
                'theme_id' => 17,
                'name' => 'Prevención',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 =>
            array (
                'id' => 109,
                // 'coh' => true,
                'theme_id' => 17,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 =>
            array (
                'id' => 114,
                // 'coh' => true,
                'theme_id' => 5,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 =>
            array (
                'id' => 115,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 =>
            array (
                'id' => 116,
                // 'coh' => true,
                'theme_id' => 15,
                'name' => 'Infraestructura hidráulica',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 =>
            array (
                'id' => 117,
                // 'coh' => true,
                'theme_id' => 15,
                'name' => 'Infraestructura urbana y rural',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 =>
            array (
                'id' => 118,
                // 'coh' => true,
                'theme_id' => 19,
                'name' => 'Financiamiento de proyectos',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 =>
            array (
                'id' => 119,
                // 'coh' => true,
                'theme_id' => 19,
                'name' => 'Infraestructura',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 =>
            array (
                'id' => 120,
                // 'coh' => true,
                'theme_id' => 19,
                'name' => 'Tecnología de la información',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 =>
            array (
                'id' => 121,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'e-commerce',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 =>
            array (
                'id' => 122,
                // 'coh' => true,
                'theme_id' => 10,
                'name' => 'Publicidad ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
