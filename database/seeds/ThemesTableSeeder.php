<?php

use Illuminate\Database\Seeder;

class ThemesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('themes')->delete();

        \DB::table('themes')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Educación',
                'ionic_icon' => 'school',
                'icon' => 'school',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Salud',
                'ionic_icon' => 'heart',
                'icon' => 'favorite_border',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Ecología',
                'ionic_icon' => 'leaf',
                'icon' => 'spa',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Servicios Públicos',
                'ionic_icon' => 'water',
                'icon' => 'nature_people',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Agropecuario',
                'ionic_icon' => 'flower',
                'icon' => 'local_florist',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Arte y Cultura',
                'ionic_icon' => 'color-palette',
                'icon' => 'color_lens',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Vivienda',
                'ionic_icon' => 'home',
                'icon' => 'domain',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Turismo',
                'ionic_icon' => 'plane',
                'icon' => 'flight_takeoff',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Transporte',
                'ionic_icon' => 'bus',
                'icon' => 'directions_bus',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'Desarrollo Económico',
                'ionic_icon' => 'usd',
                'icon' => 'monetization_on',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Deportes',
                'ionic_icon' => 'bicycle',
                'icon' => 'directions_bike',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'Desarrollo Social',
                'ionic_icon' => 'body',
                'icon' => 'accessibility',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'ONG',
                'ionic_icon' => 'globe',
                'icon' => 'public',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'Capacitación a Servidores Públicos',
                'ionic_icon' => 'people',
                'icon' => 'supervisor_account',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'Obra Pública',
                'ionic_icon' => 'construct',
                'icon' => 'build',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 17,
                'name' => 'Seguridad',
                'ionic_icon' => 'warning',
                'icon' => 'security',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 19,
                'name' => 'Ciencia y Tecnología',
                'ionic_icon' => 'bulb',
                'icon' => 'lightbulb_outline',
                'coh' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
