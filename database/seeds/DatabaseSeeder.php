<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MarginalizationsTableSeeder::class);
        $this->call(EntitiesTableSeeder::class);
        $this->call(MunicipalitiesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(SubthemesTableSeeder::class);
        $this->call(CircumscriptionsTableSeeder::class);
        $this->call(PostalCodesTableSeeder::class);
        $this->call(InstancesTableSeeder::class);
        $this->call(SettlementsTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(allMunicipalities::class);
    }
}
